<?php
    if (is_product()) {
        global $product;
        $fields = get_fields(get_the_ID());
        $brand = wp_get_post_terms(get_the_ID() , 'product_brand', array('orderby'=>'name', 'fields' => 'names'));
        $brand = count($brand) > 0 ? $brand[0] : "";

        $title = "";
        $title .= empty($fields['type']) ? "" : $fields['type'].' ';
        $title .= !empty($fields['title_on_page']) ? $brand.' '.$fields['title_on_page'].' ' : get_the_title().' ';
    } elseif (is_product_category()) {
        $title = single_cat_title('', 0);

    } elseif (is_front_page()) {
        $title = "";
    } elseif (is_shop()) {
        $title = "Каталог";
    } else {
        $title = get_the_title();
    }

    $title .= empty($title) ? "Mad Shop" : " - Mad Shop";
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
<!--    <title>--><?//= the_title() ?><!--</title>-->

    <?php wp_head(); ?>
    <?php
    if (is_checkout() && !empty(is_wc_endpoint_url('order-received'))) {
        ?>
        <style>
            footer {
                margin-top: 20.5%;
            }
        </style>
        <?php
    } ?>
</head>
<body>
<!-- Yandex.Metrika counter --> <script type="text/javascript" async> (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)}; m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)}) (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym"); ym(66892477, "init", { clickmap:true, trackLinks:true, accurateTrackBounce:true, webvisor:true }); </script> <noscript><div><img src="https://mc.yandex.ru/watch/66892477" style="position:absolute; left:-9999px;" alt="" /></div></noscript> <!-- /Yandex.Metrika counter -->