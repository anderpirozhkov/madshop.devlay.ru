<footer>
        <section class="official-representative">
            <?php if(is_front_page()) {?>
                <div class="official-representative__line"></div>
                <div class="official-representative__logo-and-text">
                    <img src="<?= get_template_directory_uri()?>/img/main/yeezy.svg" alt="">
                </div>
                <div class="official-representative__line"></div>
            <?php } ?>
        </section>
    <section class="footer-nav">
        <div class="footer-nav__menu-block">
            <div class="accordion-item">
                <div class="accordion-title">
                    MADshop
                </div>
                <div class="accordion-list">
                    <div class="accordion-list">
                        <div><a href="/o-nas">О нас</a></div>
                        <div><a href="/kontakty">Контакты</a></div>
                        <div><a href="/himchistka">Химчистка обуви</a></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-nav__menu-block">
            <div class="accordion-item">
                <div class="accordion-title">
                    Покупателю
                </div>
                <div class="accordion-list">
                    <div><a href="/dostavka-i-oplata/">Доставка и оплата</a></div>
                    <div><a href="/obmen-i-vozvrat/">Обмен и возврат</a></div>
                    <div><a href="/release/">Релизы</a></div>
                </div>
            </div>
        </div>
        <div class="footer-nav__menu-block">
            <div class="accordion-item">
                <div class="accordion-title">
                    Задайте вопрос
                </div>
                <div class="accordion-list">
                    <div><a class="fmi fmi1" href=" https://wa.me/79221777733?text=Здравствуйте! ">WhatsApp</a></div>
                    <!-- <div><a class="fmi fmi6" href="#">Telegram</a></div> -->
                    <div><a class="fmi fmi2" href="tel:+73432887881">+7 (343) 288-78-81</a></div>
                    <div><a class="fmi fmi3" href="mailto:hi@themadshop.ru">hi@madshop.ru</a></div>
                </div>
            </div>
        </div>
        <div class="footer-nav__menu-block">
            <div class="subscribe">
                <div class="subscribe-title">
                    Узнайте первыми<br>
                    о новинках и сидках
                </div>
                <form class="subscribe-form" action="">
                    <label title="Введите ваш E-mail">
                        <input id="subscriberEmail" placeholder="Введите ваш E-mail" type="text" required>
                    </label>
                    <button class="btn btn-subscribe"><span>Подписаться</span></button>
                    <div class="subscribe-form--overlay">
                        <img src="<?= get_template_directory_uri() ?>/img/loader-search-new.svg" alt="">
                    </div>
                </form>
            </div>
        </div>
    </section>
</footer>
<div class="footer-bottom">
    <div class="footer-bottom__center">
        <div class="footer-bottom-left">
            <div class="copy">© MADshop 2020</div>
            <nav class="footer-bottom-menu">
                <div><a href="/privacy-policy/">Политика конфидинциальности</a></div>
                <div><a href="/sitemap.xml">Карта сайта</a></div>
            </nav>
        </div>
        <div class="footer-bottom-right">
            <div class="pay-system">
                <div>
                    <div>
                        <img src="<?= get_template_directory_uri() ?>/img/sert.svg" alt="">
                    </div>
                </div>
                <div>
                    <div>
                        <img src="<?= get_template_directory_uri() ?>/img/visa.svg" alt="">
                    </div>
                    <div>
                        <img src="<?= get_template_directory_uri() ?>/img/mc.svg" alt="">
                    </div>
                    <div>
                        <img src="<?= get_template_directory_uri() ?>/img/mir.svg" alt="">
                    </div>
                    <div>
                        <img src="<?= get_template_directory_uri() ?>/img/yandex.svg" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    var ajax_url = "<?= admin_url("admin-ajax.php") ?>";
</script>
<?php wp_footer(); ?>

</body>
</html>