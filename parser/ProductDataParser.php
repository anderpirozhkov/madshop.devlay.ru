<?php

namespace parser;

class ProductDataParser
{

    const DATAFILE = __DIR__.'/eshop_pos.json';
    private $parseData;

    public function __construct()
    {
        $this->parseData = json_decode(file_get_contents(self::DATAFILE), true);
    }

    final public function parse(): void
    {
        global $wpdb;
        foreach ($this->parseData as $data) {
            $sku = $this->getSystemSKU($data['articul'], $data['uid']);
            $postId = $wpdb->get_var("select post_id from ms_postmeta where meta_key='_sku' and meta_value='{$sku}'");

            if ($postId === null) {
                continue;
            }
            $images = [];
            if (empty($data['images']) === false && is_serialized($data['images'])) {
                $images = maybe_unserialize($data['images']);
            }

            update_field('type', $data['type'], $postId);
            update_field('title_on_page', $data['model'], $postId);

            /**
             * Thumbnail
             */
            if (empty($data['image']) === false) {
                $this->addPostImage($postId, [$data['image']], 'thumbnail');
            }
            /**
             * Gallery
             */
            if (empty($images) === false) {
                $this->addPostImage($postId, $images, 'gallery');
            }
        }
    }

    private function getSystemSKU(string $sku, string $uid): string
    {
        $re = '/^[a-zA-Z0-9]*/m';
        preg_match($re, $uid, $matches);
        $groupId = $matches[0];

        return trim($sku).'-'.$groupId;
    }

    private function addPostImage(int $postId, array $img, string $type)
    {
        if (empty($img)) {
            return;
        }
        $galleryAttachIds = [];
        $upload_dir = wp_upload_dir();
        foreach ($img as $item) {
            if (file_exists(__DIR__.'/images/orig/'.$item)) {
                copy(__DIR__.'/images/orig/'.$item, $upload_dir['path'].'/'.$item);
            } elseif (file_exists(__DIR__.'/images/big/'.$item)) {
                copy(__DIR__.'/images/big/'.$item, $upload_dir['path'].'/'.$item);
            } elseif (file_exists(__DIR__.'/images/icons/800x600/'.$item)) {
                copy(__DIR__.'/images/icons/800x600/'.$item, $upload_dir['path'].'/'.$item);
            } else {
                continue;
            }
            $filetype = wp_check_filetype(basename($item), null);
            $attachment = array(
                'guid' => $upload_dir['url'].'/'.$item,
                'post_mime_type' => $filetype['type'],
                'post_title' => preg_replace('/\.[^.]+$/', '', basename($item)),
                'post_content' => '',
                'post_status' => 'inherit'
            );
            $attachId = wp_insert_attachment($attachment, $upload_dir['path'].'/'.$item, 0);
            // Подключим нужный файл, если он еще не подключен
            // wp_generate_attachment_metadata() зависит от этого файла.
            require_once(ABSPATH.'wp-admin/includes/image.php');

            // Создадим метаданные для вложения и обновим запись в базе данных.
            $attachData = wp_generate_attachment_metadata($attachId, $item);
            wp_update_attachment_metadata($attachId, $attachData);
            if ($type === 'thumbnail') {
                add_post_meta($postId, '_thumbnail_id', $attachId);
                set_post_thumbnail($postId, $attachId);
            } else {
                $galleryAttachIds[] = $attachId;
            }
        }
        // SELECT articul, group_uid, image, images, type, model, newbie FROM `eshop_pos` where active = 1 and is_primary = 1

        if ($type !== 'thumbnail' && empty($galleryAttachIds) === false) {
            update_post_meta($postId, '_product_image_gallery', implode(',', $galleryAttachIds));
        }
        wp_update_post(['ID' => $postId, 'post_status' => 'publish']);
    }

}