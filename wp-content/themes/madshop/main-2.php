<?php
/**
 * Template Name: Main 2
 */

get_header();
?>
    <div class="main-content">
        <?php
        load('header'); ?>
        <main>
            <section class="categories">
                <a href="/cat/muzhskoe/" class="categories__category categories__category__big-card">
                    <img src="<?= get_template_directory_uri().'/img/main/mens.png' ?>" alt="">
                    <div class="categories__category__content">
                        <span class="categories__category__content__title categories__man__title">Мужское</span>
                        <span class="categories__category__content__link categories__man__subtitle">Смотреть</span>
                    </div>
                </a>
                <a href="/cat/zhenskoe/" class="categories__category categories__category__big-card">
                    <img src="<?= get_template_directory_uri().'/img/main/woomens.png' ?>" alt="">
                    <div class="categories__category__content">
                        <span class="categories__category__content__title">Женское</span>
                        <span class="categories__category__content__link">Смотреть</span>
                    </div>
                </a>
                <a href="/cat/discounts/"
                   class="categories__category categories__category__normal">
                    <img src="<?= get_template_directory_uri().'/img/main/sales.png' ?>" alt="">
                    <div class="categories__category__content">
                        <div class="categories__category__content__tag">
                            <span class="categories__category__content__tag__title">Скидки</span>
                        </div>
                    </div>
                </a>
                <a href="/cat/aksessuary/"
                   class="categories__category categories__category__normal ">
                    <img src="<?= get_template_directory_uri().'/img/main/accessories.png' ?>" alt="">
                    <div class="categories__category__content">
                        <div class="categories__category__content__tag">
                            <span class="categories__category__content__tag__title">Аксессуары</span>
                        </div>
                    </div>
                </a>
                <a href="/himchistka/"
                   class="categories__category categories__category__normal mobile__hide">
                    <img src="<?= get_template_directory_uri().'/img/main/cleaning.png' ?>" alt="">
                    <div class="categories__category__content">
                        <div class="categories__category__content__tag">
                            <span class="categories__category__content__tag__title">Химчистка</span>
                        </div>
                    </div>
                </a>
                <a href="#" class="categories__category categories__category__normal mobile__hide">
                    <img src="<?= get_template_directory_uri().'/img/main/certs.png' ?>" alt="">
                    <div class="categories__category__content">
                        <div class="categories__category__content__tag">
                            <span class="categories__category__content__tag__title">Подарочные сертификаты</span>
                        </div>
                    </div>
                </a>
            </section>
            <section class="new-arrivals">
                <div class="new-arrivals__header">
                    <div class="new-arrivals__header__title">
                        <span>Новые поступления</span>
                    </div>
                    <div class="new-arrivals__header__link">
                        <a href="/cat/new/">Все новинки
                            <svg width='6' height='13' viewBox='0 0 6 13' fill='none'
                                 xmlns='http://www.w3.org/2000/svg'>
                                <path d='M0.5 11.7143L5.5 6.35714L0.5 1' stroke='#888888' stroke-linecap='round'
                                      stroke-linejoin='round'/>
                            </svg>
                        </a>
                    </div>
                </div>
                <div class="new-arrivals__columns">
                    <div class="new-arrivals__columns__column">
                        <div class="new-arrivals__columns__column__item new-arrivals__columns__column__item__top">
                            <div class="new-arrivals__columns__column__item__top__wrap new-arrivals__columns__column__item__top__wrap__top-image">
                                <img src="<?= get_template_directory_uri().'/img/main/colinastrada-logo.png' ?>" alt=""
                                     class="new-arrivals__columns__column__item__top-image">
                            </div>
                            <div class="new-arrivals__columns__column__item__top__wrap new-arrivals__columns__column__item__top__wrap__middle-image">
                                <div class="new-arrivals__columns__column__item__thumbnail" id="twentytwenty_top">
                                    <img src="<?= get_template_directory_uri().'/img/main/colina-strada-white.png' ?>"
                                         alt="">
                                    <img src="<?= get_template_directory_uri().'/img/main/colinastrada-black.png' ?>"
                                         alt="">
                                </div>
                            </div>
                            <div class="new-arrivals__columns__column__item__top__wrap new-arrivals__columns__column__item__top__wrap__text">
                                <a href="/cat/new/"><span class="new-arrivals__columns__column__item__title">Collina Strada x Reebok</span></a>
                                <span class="new-arrivals__columns__column__item__description">Коллаборация Collina Strada X Reebok с принтом асимметричного синего и зеленого цветов. Надпись "Позвони маме" сзади.</span>
                                <a href="/?_wpnonce=982829b534&_wp_http_referer=%2Fproduct%2Fkrossovki-reebok-x-collina-strada-h03156%2F&post_type=product&s=Collina+Strada"
                                   class="new-arrivals__columns__column__item__link">
                                    Смотреть коллекцию
                                    <svg width="21" height="8" viewBox="0 0 21 8" fill="none"
                                         xmlns="http://www.w3.org/2000/svg">
                                        <path d="M20.3536 4.35355C20.5488 4.15829 20.5488 3.84171 20.3536 3.64645L17.1716 0.464466C16.9763 0.269204 16.6597 0.269204 16.4645 0.464466C16.2692 0.659728 16.2692 0.976311 16.4645 1.17157L19.2929 4L16.4645 6.82843C16.2692 7.02369 16.2692 7.34027 16.4645 7.53553C16.6597 7.7308 16.9763 7.7308 17.1716 7.53553L20.3536 4.35355ZM0 4.5H20V3.5H0V4.5Z"
                                              fill="black"/>
                                    </svg>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="new-arrivals__columns__column">
                        <div class="new-arrivals__columns__column__items new-arrivals__columns__column__items__row">
                            <?php
                            $args = array(
                                'category' => array('new'),
                                'orderby' => 'rand',
                                'status' => 'publish'
                            );
                            $products = wc_get_products($args);
                            unset($args);
                            $i = 0;
                            foreach ($products as $product) {
                                if ($i === 4) {
                                    break;
                                }

                                $price = $product->get_price();
                                $brand = get_the_terms($product->get_id(), 'product_brand');
                                $fields = get_fields($product->get_id());
                                $availableVariants = $product->get_available_variations();
                                $availableVariants = getVariationsSort($availableVariants);
                                ?>
                                <a href="<?= get_permalink($product->get_id()) ?>"
                                   class="new-arrivals__columns__column__items__item">
                                <span class="new-arrivals__columns__column__items__item__wrap">
                                    <img src="<?= wp_get_attachment_url($product->get_image_id()); ?>" alt=""
                                         class="new-arrivals__columns__column__items__item__image">
                                    <div class="popular__slider__slide__image__wrap__overlay"></div>

                                </span>
                                    <span class="new-arrivals__columns__column__items__item__brand"><?= $brand[0]->name ?></span>
                                    <span class="new-arrivals__columns__column__items__item__title"><?= $fields['title_on_page'] ?></span>
                                    <span class="new-arrivals__columns__column__items__item__type"><?= $fields['type'] ?></span>
                                    <span class="new-arrivals__columns__column__items__item__price">
                                        <?= number_format($price, 0, ' ', ' ') ?> ₽
                                    </span>
                                    <span class="new-arrivals__columns__column__items__item__sku">
                                        <?= str_ireplace(mb_strrchr($product->get_sku(), '-'), '', $product->get_sku()) ?></span>
                                </a>
                                <?php
                                $i++;
                            } ?>

                        </div>
                    </div>
                </div>
            </section>
            <section class="popular">
                <div class="popular__header">
                    <div class="popular__header__title">
                        <span>Популярные товары</span>
                    </div>
                    <div class="popular__header__link">
                        <a href="/populars">Все товары
                            <svg width='6' height='13' viewBox='0 0 6 13' fill='none'
                                 xmlns='http://www.w3.org/2000/svg'>
                                <path d='M0.5 11.7143L5.5 6.35714L0.5 1' stroke='#888888' stroke-linecap='round'
                                      stroke-linejoin='round'/>
                            </svg>
                        </a>
                    </div>
                </div>
                <div class="popular__slider">
                    <?php
                    $args = array(
                        'numberposts' => 16,
                        'order' => 'desc',
                        'post_type' => 'product',
                        'suppress_filters' => false,
                        'post_status' => 'publish',
                        'orderby' => 'post_views',
                        'meta_query' => [
                            [
                                'key' => 'isSold',
                                'value' => '1',
                                'compare' => '!=',
                            ],
                        ]
                    );
                    $mostViewed = get_posts($args);
                    $_pf = new WC_Product_Factory();
                    ?>
                    <div id="popular__slider" class="popular__slider__popular__swiper-container">
                        <div class="swiper-wrapper">
                            <?php

                            foreach ($mostViewed as $most) {
                                $product = $_pf->get_product($most->ID);
                                $brand = get_the_terms($product->get_id(), 'product_brand');
                                $fields = get_fields($product->get_id());
                                $availableVariants = $product->get_available_variations();
                                $availableVariants = getVariationsSort($availableVariants);
                                $price = $product->get_price();
                                ?>
                                <div class="swiper-slide">
                                    <a href="<?= get_permalink($product->get_id()) ?>" class="popular__slider__slide">
                                        <div class="popular__slider__slide__image__wrap">
                                            <?= wp_get_attachment_image($product->get_image_id(), [250, 250], false, ['class' => 'popular__slider__slide__image']); ?>
                                            <div class="popular__slider__slide__image__wrap__overlay"></div>
                                        </div>
                                        <span class="popular__slider__slide__brand"><?= $brand[0]->name ?></span>
                                        <span class="popular__slider__slide__title"><?= $fields['title_on_page'] ?></span>
                                        <span class="popular__slider__slide__type"><?= $fields['type'] ?></span>
                                        <span class="popular__slider__slide__price">
                                        <?= number_format($price, 0, ' ', ' ') ?> ₽
                                    </span>
                                        <span class="popular__slider__slide__sku"><?= $product->get_sku() ?></span>
                                    </a>
                                </div>
                            <?php
                            } ?>
                        </div>
                        <div class="popular__swiper-container__slider-controls">
                            <div class="popular__swiper-container__swiper-button-prev">
                                <svg xmlns="http://www.w3.org/2000/svg" width="10" height="19" viewBox="0 0 10 19"
                                     fill="none">
                                    <path d="M9 18.3333L1 9.66667L9 1" stroke="#333333" stroke-linecap="round"
                                          stroke-linejoin="round"/>
                                </svg>
                            </div>
                            <div class="popular__swiper-container__swiper-pagination"></div>
                            <div class="popular__swiper-container__swiper-button-next">
                                <svg xmlns="http://www.w3.org/2000/svg" width="10" height="19" viewBox="0 0 10 19"
                                     fill="none">
                                    <path d="M1 18.3333L9 9.66667L1 1" stroke="#333333" stroke-linecap="round"
                                          stroke-linejoin="round"/>
                                </svg>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </main>
    </div>
<?php
get_footer('main');