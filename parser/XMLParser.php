<?php
//<tabs>[a-zA-Zа-яёА-Я \s ' ! – _ % « » ;< & “ >:—="\(\)0-9-Ø.<\/ \t {0,}]{0,}</tabs>
//<description [a-zA-Zа-яА-Я \s < >:="0-9-Ø.<\/ \t{0,}]{0,}<images>

require_once __DIR__."/../wp-load.php";
include_once(ABSPATH.'wp-admin/includes/admin.php');
define('ALLOW_UNFILTERED_UPLOADS', true);

class XMLParser
{
    public $xmlFiles;
    public $acticuls;
    public $sortable;

    public function __construct()
    {
        $handle = opendir(__DIR__.'/xml');
        while (false !== ($file = readdir($handle))) {
            $this->xmlFiles[] = $file;
        }

        if (file_exists(__DIR__.'/eshop_pos.json')) {
            $this->acticuls = json_decode(file_get_contents(__DIR__.'/eshop_pos.json'), true);
            file_put_contents('artic.log', print_r($this->acticuls, true).PHP_EOL);
        }
        if (file_exists(__DIR__.'/sort.json')) {
            $this->sortable = json_decode(file_get_contents(__DIR__.'/sort.json'), true);
        }
    }

    public function parseFiles()
    {
        if (empty($this->xmlFiles)) {
            return [];
        }
        foreach ($this->xmlFiles as $file) {
            if ($file === '.' || $file === '..') {
                continue;
            }

            $xml = simplexml_load_file(__DIR__.'/xml/'.$file, "SimpleXMLElement", LIBXML_NOCDATA);
            $json = json_encode($xml);
            $data = json_decode($json, true);

            foreach ($data['positions']['pos'] as $position) {
                $articul = '';
                $idSize = (isset($position['sizes']['size'][0])) ? $position['sizes']['size'][0]['@attributes']['id-size'] : $position['sizes']['size']['@attributes']['id-size'];
                foreach ($this->acticuls as $acticulBlock) {
                    if ($acticulBlock['id'] !== $idSize) {
                        continue;
                    }

                    $articul = $acticulBlock['articul'];
                    $tabs = $acticulBlock['tabs'];
                    break;
                }
                $num = array_keys(array_column($this->sortable, 'articul'), $articul);
                $productVariantSort = array_intersect_key($this->sortable, array_flip($num));
                $productSort = is_array($productVariantSort) ? array_shift($productVariantSort) : 1;
                $productSort = empty($productSort) ? 1 : $productSort['sort_id'];
                $tabs = preg_replace_callback(
                    '!s:(\d+):"(.*?)";!s',
                    function ($m) {
                        $len = strlen($m[2]);
                        $result = "s:$len:\"{$m[2]}\";";
                        return $result;
                    }, $tabs);
                $tabs = unserialize($tabs);
                $description = "";
                if ($tabs) {
                    foreach ($tabs as $tab) {
                        $description .= $tab['content'];
                    }
                }
                $brand = $this->checkOrCreateBrand($position['brand']);
                $categories = $this->checkOrCreateCategories($position['gender'], $position['category']);
                $images = $this->addImages($position['images']['image']);
                $post = array(
                    'post_content' => $description,
                    'post_status' => "publish",
                    'post_title' => $position['title'],
                    'post_parent' => '',
                    'post_type' => "product",
                    'menu_order' => $productSort,
                );
                $post_id = wp_insert_post($post);
                if (!empty($images)) {
                    foreach ($images as $type => $imageList) {
                        $gallery = "";
                        foreach ($imageList as $imageId) {
                            if ($type === 'thumbnail') {
                                add_post_meta($post_id, '_thumbnail_id', $imageId);
                                continue 2;
                            } else {
                                $gallery .= $imageId.",";
                            }
                        }
                        if (!empty($gallery)) {
                            update_post_meta($post_id, '_product_image_gallery', $gallery);
                        }
                    }
                }
                if (!empty($position['type'])) {
                    update_field('type', $position['type'], $post_id);
                }
                if (!empty($position['title_on_page'])) {
                    update_field('title_on_page', $position['title_on_page'], $post_id);
                }

                update_post_meta($post_id, '_sku', trim($articul));
                wp_set_object_terms($post_id, $brand, 'product_brand');
                wp_set_object_terms($post_id, $categories, 'product_cat');
                wp_set_object_terms($post_id, 'variable', 'product_type', false);

                $sizeTax = wc_attribute_taxonomy_name('size');
                $attributes = array(
                    $sizeTax => array(
                        'name' => $sizeTax,
                        'value' => 1,
                        'is_visible' => '1',
                        'is_variation' => '1',
                        'is_taxonomy' => '1'
                    ),
                );
                update_post_meta($post_id, '_product_attributes', $attributes);

                if (!empty($position['sizes']['size'])) {
                    $variation = array(
                        'post_title' => $position['title_on_page'],
                        'post_content' => '',
                        'post_status' => 'publish',
                        'post_parent' => $post_id,
                        'post_type' => 'product_variation',
                    );
                    $sizeTerms = [];
                    if (isset($position['sizes']['size'][0])) {
                        foreach ($position['sizes']['size'] as $size) {
                            $sizeTerms[] = $size['@attributes']['name_eu'];
                        }
                        wp_set_object_terms($post_id, $sizeTerms, $sizeTax);

                        foreach ($position['sizes']['size'] as $order => $size) {
                            $variation['menu_order'] = $order++;
                            $variation_id = wp_insert_post($variation);
                            update_post_meta($variation_id, '_sku', trim($articul));
                            update_post_meta($variation_id, '_regular_price', $size['@attributes']['price']);
                            if (!empty($size['@attributes']['sales_price'])) {
                                $categories[] = $this->checkOrCreateSalesCategory();
                                wp_set_object_terms($post_id, $categories, 'product_cat');
                                update_post_meta($variation_id, '_sale_price', $size['@attributes']['sales_price']);
                            }

                            update_post_meta($variation_id, '_manage_stock', 1);
                            update_post_meta($variation_id, '_stock', $size['@attributes']['amount_left']);
                            update_post_meta($variation_id, 'attribute_'.$sizeTax, strtolower(str_replace('.', '-', $size['@attributes']['name_eu'])));
                            if (!empty($tabs)) {
                                foreach ($tabs as $tab) {
                                    if (mb_stristr($tab['title'], 'описание')) {
                                        update_post_meta(
                                            $variation_id,
                                            'variable_description',
                                            strip_tags($tab['content'])
                                        );
                                    }
                                }
                            }
                        }
                    } else {
                        $size = $position['sizes']['size'];
                        wp_set_object_terms($post_id, $size['@attributes']['name_eu'], $sizeTax);
                        $variation_id = wp_insert_post($variation);

                        update_post_meta($variation_id, '_sku', trim($articul));
                        update_post_meta($variation_id, '_regular_price', $size['@attributes']['price']);
                        if (!empty($size['@attributes']['sales_price'])) {
                            update_post_meta($variation_id, '_sale_price', $size['@attributes']['sales_price']);
                        }
                        update_post_meta($variation_id, '_manage_stock', 1);
                        update_post_meta($variation_id, '_stock', $size['@attributes']['amount_left']);
                        update_post_meta(
                            $variation_id,
                            'attribute_'.$sizeTax,
                            strtolower(str_replace('.', '-', $size['@attributes']['name_eu']))
                        );
                        if (!empty($tabs)) {
                            foreach ($tabs as $tab) {
                                if (mb_stristr($tab['title'], 'описание')) {
                                    update_post_meta(
                                        $variation_id,
                                        'variable_description',
                                        strip_tags($tab['content'])
                                    );
                                }
                            }
                        }
                    }
                }
                unset ($articul);
            }
        }
    }

    private function checkOrCreateBrand($brand)
    {
        if (empty($brand['@attributes']['mane'])) {
            return 0;
        }
        $brandTax = get_term_by('name', $brand['@attributes']['mane'], 'product_brand', ARRAY_A);
        if (!$brandTax) {
            $brandTax = wp_insert_term($brand['@attributes']['mane'], 'product_brand');
        }

        if (empty($brand['@attributes']['image']) === false) {
            var_dump($brand['@attributes']['image']);
            die();
        }

        return $brandTax['term_taxonomy_id'];
    }

    private function checkOrCreateCategories($gender, $category)
    {
        $categories = [];
        if (!empty($gender)) {
            $cat = get_term_by('name', $gender, 'product_cat', ARRAY_A);
            if (empty($cat)) {
                $cat = wp_insert_term($gender, 'product_cat');
            }
            $categories[] = $cat['term_taxonomy_id'];
        }
        unset($cat);

        $parent = 0;
        if (!empty($category['@attributes']['parent_category'])) {
            $parent = get_term_by('name', $category['@attributes']['parent_category'], 'product_cat', ARRAY_A);
            if (empty($parent)) {
                $parent = wp_insert_term($category['@attributes']['parent_category'], 'product_cat');
                $parent = $parent['term_taxonomy_id'];
            }
            $categories[] = $parent['term_taxonomy_id'];
        }

        if (!empty($category['@attributes']['title'])) {
            $cat = get_term_by('name', $category['@attributes']['title'], 'product_cat', ARRAY_A);
            if (empty($cat)) {
                $cat = wp_insert_term($category['@attributes']['title'], 'product_cat', ['parent' => $parent]);
            }
            $categories[] = $cat['term_taxonomy_id'];
        }

        return $categories;
    }

    private function addImages($images)
    {
        $upload_dir = wp_upload_dir();
        if (empty($images)) {
            return [];
        }
        $returned = [];
        if (isset($images[0])) {
            foreach ($images as $image) {
                if (empty($image['@attributes']['src'])) {
                    continue;
                }
                $src = str_replace('img/', '', $image['@attributes']['src']);
                $filetype = wp_check_filetype(basename($image['@attributes']['src']), null);
                if (file_exists($upload_dir['path'].$src) == false) {
                    continue;
                }
                $attachment = array(
                    'guid' => $upload_dir['url'].$src,
                    'post_mime_type' => $filetype['type'],
                    'post_title' => preg_replace('/\.[^.]+$/', '', basename($image['@attributes']['src'])),
                    'post_content' => '',
                    'post_status' => 'inherit'
                );
                $attach_id = wp_insert_attachment($attachment, $upload_dir['path'].$src, 0);
                // Подключим нужный файл, если он еще не подключен
                // wp_generate_attachment_metadata() зависит от этого файла.
                require_once(ABSPATH.'wp-admin/includes/image.php');

                // Создадим метаданные для вложения и обновим запись в базе данных.
                $attach_data = wp_generate_attachment_metadata($attach_id, $src);
                wp_update_attachment_metadata($attach_id, $attach_data);

                $returned[$image['@attributes']['type']][] = $attach_id;
            }
        } else {
            $src = str_replace('img/', '', $images['@attributes']['src']);
            $filetype = wp_check_filetype(basename($images['@attributes']['src']), null);
            if (file_exists($upload_dir['path'].$src) == false) {
                return [];
            }
            $attachment = array(
                'guid' => $upload_dir['url'].$src,
                'post_mime_type' => $filetype['type'],
                'post_title' => preg_replace('/\.[^.]+$/', '', basename($images['@attributes']['src'])),
                'post_content' => '',
                'post_status' => 'inherit'
            );
            $attach_id = wp_insert_attachment($attachment, $upload_dir['path'].$src, 0);
            // Подключим нужный файл, если он еще не подключен
            // wp_generate_attachment_metadata() зависит от этого файла.
            require_once(ABSPATH.'wp-admin/includes/image.php');

            // Создадим метаданные для вложения и обновим запись в базе данных.
            $attach_data = wp_generate_attachment_metadata($attach_id, $src);
            wp_update_attachment_metadata($attach_id, $attach_data);

            $returned[$images['@attributes']['type']][] = $attach_id;
        }

        return $returned;
    }

    private function checkOrCreateSalesCategory()
    {
        $cat = get_term_by('name', 'Скидки', 'product_cat', ARRAY_A);
        if (empty($cat)) {
            $cat = wp_insert_term('Скидки', 'product_cat');
        }
        return $cat['term_taxonomy_id'];
    }

}