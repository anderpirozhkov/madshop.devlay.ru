<?php
    $menu = wp_get_nav_menu_items('header-menu');
    ddJSON($menu);
?>
<nav class="menu">
    <div><a href="<?= get_term_link(15, 'product_cat') ?>">Новинки</a></div>
    <div>
        <a href="/brands/">Бренды</a>
        <div class="menu-drop-wrap">
            <div class="wrap">
                <div class="menu-drop-container">
                    <div class="menu-drop-l no-grid">
                        <?php
                        $brands = get_terms(
                            'product_brand',
                            array(
                                'orderby' => 'name'
                            )
                        );
                        ?>
                        <nav class="menu-drop-brand">
                            <?php
                            $i = 0;
                            foreach ($brands as $brandItem) {
                                if ($i === 0) {
                                    echo "<div>";
                                }
                                ?>
                                <div>
                                    <a href="<?= get_term_link($brandItem->term_id, 'product_brand') ?>">
                                        <?= $brandItem->name ?>
                                    </a>
                                </div>
                                <?php
                                if ($i === 9) {
                                    echo "</div>";
                                    $i = 0;
                                    continue;
                                }
                                $i++;
                            } ?>
                        </nav>
                        <div class="menu-drop-all">
                            <a href="/cat/muzhskoe/?filters=product_brand[521-491-471-455-522-475-472-512-447-396-517-433-438-479-465-518-516-523]">Все
                                бренды</a>
                        </div>
                    </div>
                    <div class="menu-drop-r">
                        <div class="slide-menu">
                            <span>New Balance</span>
                            <a href="/shop/?filters=product_brand[447]"><img
                                    src="/wp-content/themes/madshop/img/menu-newbalance-327.png" loading="lazy"></a>
                        </div>
                        <div class="slide-menu">
                            <span>Yeezy</span>
                            <a href="/brand/yeezy/"><img
                                    src="/wp-content/themes/madshop/img/menu-yeezy.png" loading="lazy"></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div>
        <a href="<?= get_term_link(397, 'product_cat') ?>">Мужское</a>
        <div class="menu-drop-wrap">
            <div class="wrap">
                <div class="menu-drop-container">
                    <div class="menu-drop-l">
                        <nav class="menu-drop-category">
                            <div>
                                <a href="/cat/muzhskoe/?filters=product_cat[399]">Кроссовки</a>
                            </div>
                            <div>
                                <a href="/cat/muzhskoe/?filters=product_cat[362]">Бомберы</a>
                            </div>
                            <div>
                                <a href="/cat/muzhskoe/?filters=product_cat[388]">Пуховики</a>
                            </div>
                            <div>
                                <a href="/cat/muzhskoe/?filters=product_cat[366]">Жилеты</a>
                            </div>
                            <div>
                                <a href="/cat/muzhskoe/?filters=product_cat[365]">Рубашки</a>
                            </div>
                            <div>
                                <a href="/cat/muzhskoe/?filters=product_cat[355]">Толстовки</a>
                            </div>
                            <div>
                                <a href="/cat/muzhskoe/?filters=product_cat[354]">Футболки</a>
                            </div>
                            <div>
                                <a href="/cat/muzhskoe/?filters=product_cat[229]">Кепки</a>
                            </div>
                            <div>
                                <a href="/cat/muzhskoe/?filters=product_cat[368]">Джинсы</a>
                            </div>
                            <div>
                                <a href="/cat/muzhskoe/?filters=product_cat[241]">Носки</a>
                            </div>

                        </nav>
                        <nav class="menu-drop-brand">
                            <div>
                                <div>
                                    <a href="/cat/muzhskoe/?filters=product_brand[521]">adidas
                                        Originals</a>
                                </div>
                                <div>
                                    <a href="/cat/muzhskoe/?filters=product_brand[491]">ASICS</a>
                                </div>
                                <div>
                                    <a href="/cat/muzhskoe/?filters=product_brand[471]">BBC</a>
                                </div>
                                <div>
                                    <a href="/cat/muzhskoe/?filters=product_brand[455]">Calvin
                                        Clein</a></div>
                                <div>
                                    <a href="/cat/muzhskoe/?filters=product_brand[522]">Carhartt
                                        WIP</a></div>
                                <div>
                                    <a href="/cat/muzhskoe/?filters=product_brand[475]">Edwin</a>
                                </div>
                                <div>
                                    <a href="/cat/muzhskoe/?filters=product_brand[447]">New
                                        Balance</a></div>
                                <div>
                                    <a href="/cat/muzhskoe/?filters=product_brand[396]">Nike</a>
                                    <div>
                                        <a href="/cat/muzhskoe/?filters=product_brand[433]">Puma</a>
                                    </div>
                                    <div>
                                        <a href="/cat/muzhskoe/?filters=product_brand[438]">Reebok</a>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div>
                                    <a href="/cat/muzhskoe/?filters=product_brand[479]">Stance</a>
                                </div>
                                <div>
                                    <a href="/cat/muzhskoe/?filters=product_brand[518]">The
                                        North Face</a></div>
                                <div>
                                    <a href="/cat/muzhskoe/?filters=product_brand[465]">Stussy</a>
                                </div>
                                <div>
                                    <a href="/?s=Yeezy&post_type=product&dgwt_wcas=1">Yeezy</a>
                                </div>
                        </nav>
                        <div class="menu-drop-all">
                            <a href="/cat/muzhskoe/">Все категории</a>
                        </div>
                        <div class="menu-drop-all">
                            <a href="/cat/muzhskoe/?filters=product_brand[521-491-471-455-522-475-472-512-447-396-517-433-438-479-465-518-516-523]">Все
                                бренды</a>
                        </div>
                    </div>
                    <div class="menu-drop-r">
                        <div class="slide-menu">
                            <span>Обувь</span>
                            <a href="/cat/muzhskoe/?filters=product_cat[399]"><img
                                    src="/wp-content/themes/madshop/img/menu-men-obuv.png" loading="lazy"></a>
                        </div>
                        <div class="slide-menu">
                            <span>Одежда</span>
                            <a href=""><img
                                    src="/wp-content/themes/madshop/img/menu-men-odejda.png" loading="lazy"></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div>
        <a href="<?= get_term_link(408, 'product_cat') ?>">Женское</a>
        <div class="menu-drop-wrap">
            <div class="wrap">
                <div class="menu-drop-container">
                    <div class="menu-drop-l">
                        <nav class="menu-drop-category">
                            <div>
                                <a href="/cat/zhenskoe/?filters=product_cat[399]">Кроссовки</a>
                            </div>
                            <div>
                                <a href="/cat/zhenskoe/?filters=product_cat[339]">Кеды</a>
                            </div>
                            <div>
                                <a href="/cat/zhenskoe/?filters=product_cat[358]">Брюки</a>
                            </div>
                            <div>
                                <a href="/cat/zhenskoe/?filters=product_cat[355]">Толстовки</a>
                            </div>
                            <div>
                                <a href="/cat/zhenskoe/?filters=product_cat[241]">Носки</a>
                            </div>
                            <div>
                                <a href="/cat/zhenskoe/?filters=product_cat[389]">Леггинсы</a>
                            </div>
                            <div>
                                <a href="/cat/zhenskoe/?filters=product_cat[500]">Шорты</a>
                            </div>
                            <div>
                                <a href="/cat/zhenskoe/?filters=product_cat[453]">Аксессуары</a>
                            </div>

                        </nav>
                        <nav class="menu-drop-brand">
                            <div>
                                <div>
                                    <a href="/cat/zhenskoe/?filters=product_brand[521]">adidas
                                        Originals</a></div>
                                <div>
                                    <a href="/cat/zhenskoe/?filters=product_brand[447]">New
                                        Balance</a></div>
                                <div>
                                    <a href="/cat/zhenskoe/?filters=product_brand[396]">Nike</a>
                                </div>
                                <div>
                                    <a href="/cat/zhenskoe/?filters=product_brand[433]">Puma</a>
                                </div>
                                <div>
                                    <a href="/cat/zhenskoe/?filters=product_brand[438]">Reebok</a>
                                </div>
                                <div>
                                    <a href="/cat/zhenskoe/?filters=product_brand[479]">Stance</a>
                                </div>
                                <div>
                                    <a href="/?s=Yeezy&post_type=product&dgwt_wcas=1">Yeezy</a>
                                </div>
                            </div>
                        </nav>
                        <div class="menu-drop-all">
                            <a href="/cat/zhenskoe/">Все категории</a>
                        </div>
                        <div class="menu-drop-all">
                            <a href="/cat/zhenskoe/?filters=product_brand[521-482-497-447-396-433-438-479-498]">Все
                                бренды</a>
                        </div>
                    </div>
                    <div class="menu-drop-r">
                        <div class="slide-menu">
                            <span>Обувь</span>
                            <a href="/cat/zhenskoe/?filters=product_cat[339-399]"><img
                                    src="/wp-content/themes/madshop/img/menu-woman-obuv2.png" loading="lazy"></a>
                        </div>
                        <div class="slide-menu">
                            <span>Одежда</span>
                            <a href=""><img
                                    src="/wp-content/themes/madshop/img/menu-woman-odejda.png" loading="lazy"></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div>
        <a href="<?= get_term_link(453, 'product_cat') ?>">Аксессуары</a>
        <div class="menu-drop-wrap">
            <div class="wrap">
                <div class="menu-drop-container">
                    <div class="menu-drop-l">
                        <nav class="menu-drop-category">
                            <div>
                                <a href="/cat/aksessuary/?filters=product_cat[230]">Бейсболки</a>
                            </div>
                            <div>
                                <a href="/cat/aksessuary/?filters=product_cat[229]">Кепки</a>
                            </div>
                            <div>
                                <a href="/cat/aksessuary/?filters=product_cat[241]">Носки</a>
                            </div>
                            <div>
                                <a href="/cat/aksessuary/?filters=product_cat[224-260]">Перчатки</a>
                            </div>
                            <div>
                                <a href="/cat/aksessuary/?filters=product_cat[238]">Сумки
                                    поясные</a>
                            </div>
                            <div>
                                <a href="/cat/aksessuary/?filters=product_cat[456]">Трусы</a>
                            </div>
                            <div>
                                <a href="/cat/aksessuary/?filters=product_cat[218]">Часы</a>
                            </div>
                            <div>
                                <a href="/cat/aksessuary/?filters=product_cat[264-251]">Шапки</a>
                            </div>
                        </nav>
                        <nav class="menu-drop-brand">
                            <div>
                                <div>
                                    <a href="/cat/aksessuary/?filters=product_brand[491]">ASICS</a>
                                </div>
                                <div>
                                    <a href="/cat/aksessuary/?filters=product_brand[471]">BBC</a>
                                </div>
                                <div>
                                    <a href="/cat/aksessuary/?filters=product_brand[455]">Calvin
                                        Clein</a>
                                </div>
                                <div>
                                    <a href="/cat/aksessuary/?filters=product_brand[522]">Carhartt
                                        WIP</a>
                                </div>
                                <div>
                                    <a href="/cat/aksessuary/?filters=product_brand[452]">Casio
                                    </a>
                                </div>
                                <div>
                                    <a href="/cat/aksessuary/?filters=product_brand[460]">Crep
                                        Protect
                                    </a>
                                </div>
                                <div>
                                    <a href="/cat/aksessuary/?filters=product_brand[475]">Edwin</a>
                                </div>
                                <div>
                                    <a href="/cat/aksessuary/?filters=product_brand[475]">Fred
                                        Perry</a>
                                </div>
                                <div>
                                    <a href="/cat/aksessuary/?filters=product_brand[497]">Herschel</a>
                                </div>
                                <div>
                                    <a href="/cat/aksessuary/?filters=product_brand[447]">New
                                        Balance</a>
                                </div>
                            </div>
                            <div>
                                <div>
                                    <a href="/cat/aksessuary/?filters=product_brand[433]">Puma</a>
                                </div>
                                <div>
                                    <a href="/cat/aksessuary/?filters=product_brand[438]">Reebok</a>
                                </div>
                                <div>
                                    <a href="/cat/aksessuary/?filters=product_brand[476]">Solemate</a>
                                </div>
                                <div>
                                    <a href="/cat/aksessuary/?filters=product_brand[479]">Stance</a>
                                </div>
                                <div>
                                    <a href="/cat/aksessuary/?filters=product_brand[518]">The
                                        North Face</a>
                                </div>
                                <div>
                                    <a href="/cat/aksessuary/?filters=product_brand[465]">Stussy</a>
                                </div>
                                <div>
                                    <a href="/cat/aksessuary/?filters=product_brand[498]">Vans</a>
                                </div>
                            </div>
                        </nav>
                        <div class="menu-drop-all">
                            <a href="/cat/aksessuary/">Все категории</a>
                        </div>
                        <div class="menu-drop-all">
                            <a href="/cat/aksessuary/?filters=product_brand[491-482-471-455-522-452-460-475-472-497-447-433-438-476-479-465-498]">Все
                                бренды</a>
                        </div>
                    </div>
                    <div class="menu-drop-r">
                        <div class="slide-menu">
                            <span>Часы Casio</span>
                            <a href="/brand/casio/"><img
                                    src="/wp-content/themes/madshop/img/menu-casio-aksessuary.png" loading="lazy"></a>
                        </div>
                        <div class="slide-menu">
                            <span>CREP PROTECT</span>
                            <a href="/cat/aksessuary/?filters=product_brand[460]"><img
                                    src="/wp-content/themes/madshop/img/menu-crep-protect.png" loading="lazy"></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div><a href="<?= get_term_link(520, 'product_cat') ?>">Скидки</a></div>
</nav>
