<?php
/**
 * Related Products
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/related.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see         https://docs.woocommerce.com/document/template-structure/
 * @package     WooCommerce\Templates
 * @version     3.9.0
 */

if (!defined('ABSPATH')) {
    exit;
}
global $product;

$brand = get_the_terms($product->get_id(), 'product_brand');
$type = get_field('type', $product->get_id());
if (empty($brand) === false) {
    $brandProducts = [];
    $args = array(
        'post__not_in' => [$product->get_id()],
        'orderby' => 'rand',
        'post_type' => 'product',
        'tax_query' => array(
            array(
                'taxonomy' => 'product_brand',
                'field' => 'term_id',
                'terms' => $brand[0]->term_id
            )
        ),
        'meta_query' => [
            'relation' => 'AND',
            [
                'key' => 'type',
                'value' => $type
            ],
            [
                'key' => 'isSold',
                'value' => '1',
                'compare' => '!=',
            ],
        ]
    );
    $query = new WP_Query($args);
    if (empty($query->posts) === false) {
        foreach ($query->posts as $post) {
            $brandProducts[] = wc_get_product($post->ID);
        }
    }
}

if (!empty($brandProducts)) {
    ?>
    <div class="might-like decor">
        <div class="wrap">
            <div class="might-like-list">
                <?php
                $i = 0;
                foreach ($brandProducts as $brandProduct) {
                    if ($i >= 4) {
                        break;
                    }
                    $post_object = get_post($brandProduct->get_id());
                    setup_postdata($GLOBALS['post'] =& $post_object);
                    wc_get_template_part('content', 'product');
                    $i++;
                } ?>
            </div>
        </div>
    </div>
    <?php
}
if ($related_products) { ?>
    <div class="might-like">
        <div class="wrap">
            <div class="might-like-title">
                Возможно вам понравится
            </div>
            <div class="might-like-list">
                <?php
                foreach ($related_products as $related_product) {
                    $post_object = get_post($related_product->get_id());
                    $isSold = get_field('isSold', $related_product->get_id(), true);
                    if ($isSold) { continue; }
                    setup_postdata($GLOBALS['post'] =& $post_object);
                    wc_get_template_part('content', 'product');
                } ?>
            </div>
        </div>
    </div>
    <?php
} ?>