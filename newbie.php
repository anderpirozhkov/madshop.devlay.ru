<?php

require_once "wp-load.php";

global $wpdb;
//const NEWBIE_ID = 15;
//const NEWBIE_FILE = ABSPATH.'parser/newbie.json';
//$newbieData = json_decode(file_get_contents(NEWBIE_FILE), true);
//
//if (empty($newbieData) === false) {
//    foreach ($newbieData as $newbieItem) {
//        $re = '/^[a-zA-Z0-9]*/m';
//        preg_match($re, $newbieItem['group_uid'], $matches);
//        $groupId = $matches[0];
//        $sysArticul = trim($newbieItem['articul']).'-'.$groupId;
//
//        $post = $wpdb->get_row(
//            "SELECT post_id FROM ms_postmeta WHERE meta_key = '_sku' AND meta_value = '".$sysArticul."'"
//        );
//        if (empty($post)) {
//            continue;
//        }
//
//        $productTerms = wp_get_post_terms($post->post_id, 'product_cat', ['fields' => 'ids']);
//        if (is_array($productTerms)) {
//            array_push($productTerms, NEWBIE_ID);
//        } else {
//            $productTerms[] = NEWBIE_ID;
//        }
//        wp_set_post_terms($post->post_id, $productTerms, 'product_cat');
//    }
//}

const INFO_FILE = 'parser/eshop_pos.json';
$drafts = get_posts(['post_type' => 'product', 'post_status' => 'draft', 'numberposts' => '-1']);
$info = json_decode(file_get_contents(INFO_FILE), true);
if (empty($drafts)) {
    exit;
}

foreach ($drafts as $draft) {
    $sku = get_post_meta($draft->ID, '_sku', true);
    $sku = substr($sku, 0, strlen($sku)-9);
    foreach ($info as $item) {
        if ($item['articul'] !== (string)$sku) {
            continue;
        }
        $images = [];
        if (empty($item['images']) === false && is_serialized($item['images'])) {
            $images = maybe_unserialize($item['images']);
        }

        update_field('type', $item['type'], $draft->ID);
        update_field('title_on_page', $item['model'], $draft->ID);
//        var_dump($item['articul'], $images);die();
        /**
         * Thumbnail
         */
        if (empty($item['image']) === false) {
            addPostImage($draft->ID, [$item['image']], 'thumbnail');
        }
        /**
         * Gallery
         */
        if (empty($images) === false) {
            addPostImage($draft->ID, $images, 'gallery');
        }
    }
}



function addPostImage(int $postId, array $img, string $type)
{
    if (empty($img)) {
        return;
    }
    $galleryAttachIds = [];
    $upload_dir = wp_upload_dir();
    foreach ($img as $item) {
        if (file_exists('parser/images/orig/'.$item)) {
            copy('parser/images/orig/'.$item, $upload_dir['path'].'/'.$item);
        } elseif (file_exists('parser/images/big/'.$item)) {
            copy('parser/images/big/'.$item, $upload_dir['path'].'/'.$item);
        } elseif (file_exists('parser/images/icons/800x600/'.$item)) {
            copy('parser/images/icons/800x600/'.$item, $upload_dir['path'].'/'.$item);
        } else {
            continue;
        }
        $filetype = wp_check_filetype(basename($item), null);
        $attachment = array(
            'guid' => $upload_dir['url'].'/'.$item,
            'post_mime_type' => $filetype['type'],
            'post_title' => preg_replace('/\.[^.]+$/', '', basename($item)),
            'post_content' => '',
            'post_status' => 'inherit'
        );
        $attachId = wp_insert_attachment($attachment, $upload_dir['path'].'/'.$item, 0);
        // Подключим нужный файл, если он еще не подключен
        // wp_generate_attachment_metadata() зависит от этого файла.
        require_once(ABSPATH.'wp-admin/includes/image.php');

        // Создадим метаданные для вложения и обновим запись в базе данных.
        $attachData = wp_generate_attachment_metadata($attachId, $item);
        wp_update_attachment_metadata($attachId, $attachData);
        if ($type === 'thumbnail') {
            add_post_meta($postId, '_thumbnail_id', $attachId);
            set_post_thumbnail($postId, $attachId);
        } else {
            $galleryAttachIds[] = $attachId;
        }
    }
    // SELECT articul, group_uid, image, images, type, model, newbie FROM `eshop_pos` where active = 1 and is_primary = 1

    if ($type !== 'thumbnail' && empty($galleryAttachIds) === false) {
        update_post_meta($postId, '_product_image_gallery', implode(',', $galleryAttachIds));
    }
//    wp_update_post(['ID' => $postId, 'post_status' => 'publish']);
}