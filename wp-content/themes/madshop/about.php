<?php
/**
 * Template Name: About
 */

get_header();

load('header');
$breadcrumbs = array(
    'delimiter' => '',
    'wrap_before' => '<nav class="bread-crumbs">',
    'wrap_after' => '</nav>',
    'before' => '<div>',
    'after' => '</div>',
    'home' => _x('MADshop', 'breadcrumb', 'woocommerce'),
);
?>

<style>
	.about-page {
    background: #fff
	}
	.about-page h1 {
	font-size:18px;
    text-align:center;
	padding-top:55px;
	padding-bottom:10px;
	width:100%;
	}
	.about-page ul, .about-page ol {
		padding:0;
		margin:0;
	}

	.wrap.about-content {
    max-width: 740px;
	font-size: 13px;
    line-height: 21px;
	}

	.miniGallery {
    display: flex;
    justify-content: center;
	}
	.miniGallery img {
    padding: 25px 0;
	width: 120%;
	}
	.aboutGallery {
		padding-top:25px;
		padding-bottom:100px;
	}
	.aboutGallery img {
		width:100%;
	}

	@media(max-width:768px){
			.about-page h1{
				padding-top:35px;
			}
			.miniGallery img {
			padding: 25px 0 15px 0;
			width: 150%;
			height:100%;
			}
			.wrap.about-content {
			padding: 0 5px;
			}
		}
</style>

    <div class="l_content about-page" id="content">
        <div class="wrap about-content">
            <div class="lc_main">
                <div class="lcm_content">
                    <div class="content_wrap">
                        <div class="section section_text">
                            <div class="uss_section_content">
                                <div class="content-o-nas">
										<h1>О нас</h1>
										<p>Проект Mad Shop задумывался как место притяжения для тех, кто не смыслит свою
                                        жизнь без качественных и удобных кроссовок. Поэтому мы трансформировали
                                        привычный формат магазина, создав здесь Мекку для сникерхэдов.</p>
										<p>400 пар первоклассных кроссовок и кед, самые впечатляющие коллаборации, уличная
                                        одежда, яркие аксессуары и ХИМЧИСТКА, аналогов которой нет в городе. Здесь фанат
                                        сникеров найдет все и даже больше.</p>
										<p>Менее чем за год работы Mad Shop получил официальные статусы:</p>
										<ul>
											<ol>— Yeezy от adidas</ol>
											<ol>— Fashion специалист от adidas, Reebok</ol>
											<ol>— Select от Puma</ol>
										</ul>
										<div class="miniGallery">
										<img src="<?= get_template_directory_uri() ?>/img/about/about.jpg" alt="">
										</div>
									<p>MAD – гостеприимный парень. Поэтому мы обучили нашу команду работать точно, как
                                        швейцарские часы. Ведь наши стены принимали таких величин, как: Mike Tyson,
                                        Руслана Проводникова, Константина Цзю, Антона Шипулина, Дмитрия Нагиева, Михаила
                                        Пореченкова, Yanix и многих других.</p>
                                    <p>На полках Mad Shop представлены все мировые бренды: Nike, New Balance, Jordan,
                                        adidas, Asics Tiger, Diadora, Puma, Reebok, Vans, Yeezy.</p>
                                    <p>Устраивая презентации, мы создаем атмосферу одержимости, собирая сотни людей. На
                                        последний весенний релиз пришло более 200-а любителей кроссовок. И это не просто
                                        представление новой пары. Это праздник, перформанс о котором будут говорить еще
                                        долго.&nbsp;</p>
                                    <p>Mad Shop – открытая компания. Концепция продвижения в социальных сетях
                                        подразумевает максимальный контакт 24/7. Идея заботы о потребителе и его
                                        желаниях красной линией проходит через наш сервис. Оповещение о новых
                                        поступлениях, рассылка материалов, waiting list, помощь любого рода
                                        транслируется не только в стенах оффлайновой точки, но и в виртуальном
                                        общении.</p>
									<div class="aboutGallery">
										<img src="<?= get_template_directory_uri() ?>/img/about/tsn.png" />
										<img src="<?= get_template_directory_uri() ?>/img/about/galustan.JPG" />
										<img src="<?= get_template_directory_uri() ?>/img/about/jons1.jpg" />
										<img src="<?= get_template_directory_uri() ?>/img/about/star11.JPG" />
										<img src="<?= get_template_directory_uri() ?>/img/about/star5.jpg" />
										<img src="<?= get_template_directory_uri() ?>/img/about/star6.jpg" />
										<img src="<?= get_template_directory_uri() ?>/img/about/star7.jpg" />
										<img src="<?= get_template_directory_uri() ?>/img/about/star8.JPG" />
										<img src="<?= get_template_directory_uri() ?>/img/about/star9.JPG" />
									</div>
                                </div>
                            </div>
                         </div>
                        </div>
                        <div class="cleaner"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php
get_footer('main'); ?>