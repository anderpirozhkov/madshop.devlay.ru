<?php
/**
 * Thankyou page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/thankyou.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.7.0
 */

defined( 'ABSPATH' ) || exit;
?>

<div class="woocommerce-order">
    <table style="max-width:600px; width:100%;border-spacing: 0;background:#ffffff; margin: 20px auto; padding: 10px 0 10px 0">
        <tbody>
        <tr>
            <td>
                <center>
                    <br>
                    <table style="border-spacing: 0;">
                        <tbody>
                        <tr>
                            <td style="border-spacing: 0;max-width:600px;font-family: 'Montserrat',sans-serif; display: flex; flex-direction: column; justify-content: center;align-items: center;">
                                <table style="margin-bottom:30px">
                                    <tbody>
                                    <tr>
                                        <td style="font-family: 'Montserrat',sans-serif;
                                       font-style: normal;
                                       font-weight: normal;
                                       font-size: 24px; text-align: center;
                                       line-height: 29px; max-width: 280px;">
                                            Заказ №<?= $order->get_order_number() ?>
                                            успешно оформлен
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                                <table style="margin-bottom: 6px;">
                                    <tbody>
                                    <tr>
                                        <td style="font-family: 'Montserrat',sans-serif;
                                       font-style: normal;
                                       font-weight: normal;
                                       font-size: 14px;
                                       line-height: 21px; text-align: center;
										max-width: 560px;
                                       padding: 0 20px;">
                                            Мы свяжемся с вами после сборки Вашего заказа. Сборка заказов происходит
                                            ежедневно с 10:00 до 18:00 по Москвоскому времени.
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </center>
            </td>
        </tr>
        </tbody>
    </table>
