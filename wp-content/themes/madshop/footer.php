<footer class="footer">
    <div class="wrap">
        <div class="footer-container">
            <div class="footer-top">
                <div class="footer-top-left">
                    <nav class="footer-menu js-footer-acc">
                        <div class="footer-menu-block b1">
                            <div class="accordion-item">
                                <div class="accordion-title">
                                    Задайте вопрос
                                </div>
                                <?php /*
                                    wp_reset_postdata();
                                    $waLink = "https://wa.me/+79634477971";
                                    if (is_product()) {
                                        $product = wc_get_product(get_the_id());
                                        $fields = get_fields($product->get_parent_id() ?: $product->get_id());
                                        $waLink .= "/?text=Здравствуйте, меня заинтересовал продукт {$fields['type']}  {$fields['title_on_page']}, артикул: ".$product->get_sku();
                                    }
                                    */
                                ?>
                                <div class="accordion-list">
                                    <?php /*<div><a class="fmi fmi1" href="<?= $waLink ?>">WhatsApp</a></div>*/?>
                                    <div><a class="fmi fmi2" href="tel:+73432887881">+7 (343) 288-78-81</a></div>
                                    <div><a class="fmi fmi3" href="mailto:hi@themadshop.ru">hi@themadshop.ru</a></div>
                                </div>
                            </div>
                        </div>
                        <div class="footer-menu-block b2">
                            <div class="accordion-item">
                                <div class="accordion-title">
                                    Покупателям
                                </div>
                                <div class="accordion-list">
                                    <div><a href="/obmen-i-vozvrat">Возврат</a></div>
                                    <div><a href="/dostavka-i-oplata">Доставка</a></div>
<!--                                    <div><a href="">Подарочные сертификаты</a></div>-->
                                </div>
                            </div>
                        </div>
                        <div class="footer-menu-block b3">
                            <div class="accordion-item">
                                <div class="accordion-title">
                                    Об MADshop
                                </div>
                                <div class="accordion-list">
                                    <div><a href="/o-nas">О нас</a></div>
                                    <div><a href="/kontakty">Контакты</a></div>
                                    <div><a href="/himchistka">Химчистка обуви</a></div>
                                </div>
                            </div>
                        </div>
                        <div class="footer-menu-block b4">
                            <div class="accordion-item">
                                <div class="accordion-title">
                                    Контакты
                                </div>
                                <div class="accordion-list">
                                    <div><a href="">+7 (343) 288-78-81</a></div>
                                </div>
                            </div>
                        </div>
                        <div class="footer-menu-block b5">
                            <div class="accordion-item">
                                <div class="accordion-title">
                                    Адрес
                                </div>
                                <div class="accordion-list">
                                    <div>
                                        620000, г. Екатеринбург<br>
                                        ул. Радищева, 25
                                    </div>
									<div>Ежедневно 11:00 — 21:00</div>
                                </div>
                            </div>
                        </div>
                    </nav>
                </div>
                <div class="footer-top-right">
                    <div class="social">
                        <div class="social-title">
                            Мы в соцсетях
                        </div>
                        <div class="social-list">
                            <div><a target="_blank" title="Instagram" href="https://www.instagram.com/the__madshop/"><img src="<?= get_template_directory_uri() ?>/img/in.svg" alt=""></a></div>
<!--                            <div><a title="Vkontakte" href=""><img src="--><?//= get_template_directory_uri() ?><!--/img/vk.svg" alt=""></a></div>-->
<!--                            <div><a title="Facebook" href=""><img src="--><?//= get_template_directory_uri() ?><!--/img/fb.svg" alt=""></a></div>-->
                        </div>
                    </div>
                    <div class="subscribe">
                        <div class="subscribe-title">
                            Узнайте первыми<br>
                            о новинках и сидках
                        </div>
                        <form class="subscribe-form" action="">
                            <label title="Введите ваш E-mail">
                                <input id="subscriberEmail" placeholder="Введите ваш E-mail" type="text" required>
                            </label>
                            <button class="btn btn-subscribe"><span>Подписаться</span></button>
                            <div class="subscribe-form--overlay">
                                <img src="<?= get_template_directory_uri() ?>/img/loader-search-new.svg" alt="">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="footer-bottom">
                <div class="footer-bottom-left">
                    <div class="copy">© MADshop 2021</div>
                    <!-- <nav class="footer-bottom-menu">
                        <div><a href="">Карта сайта</a></div>
                        <div><a href="">Гарантии и сертификаты</a></div>
                        <div><a href="">Конфидинциальность</a></div>
                        <div><a href="">Соглашение</a></div>
                    </nav> -->
                </div>
                <div class="footer-bottom-right">
                    <div class="pay-system">
                        <div>
                            <div>
                                <img src="<?= get_template_directory_uri() ?>/img/sert.svg" alt="">
                            </div>
                        </div>
                        <div>
                            <div>
                                <img src="<?= get_template_directory_uri() ?>/img/visa.svg" alt="">
                            </div>
                            <div>
                                <img src="<?= get_template_directory_uri() ?>/img/mc.svg" alt="">
                            </div>
                            <div>
                                <img src="<?= get_template_directory_uri() ?>/img/mir.svg" alt="">
                            </div>
                            <div>
                                <img src="<?= get_template_directory_uri() ?>/img/yandex.svg" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
</div>
<script>
    var ajax_url = "<?= admin_url("admin-ajax.php") ?>";
</script>
<?php wp_footer() ?>

<script>

    var aboutSwiper = new Swiper('.about-swiper-container', {
        autoplay: {
            delay: 2000,
        }
    });
</script>
</body>
</html>