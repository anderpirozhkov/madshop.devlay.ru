<?php
/**
 * Single Product Price
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/price.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
global $product;
$price = explode(' ', strip_tags($product->get_price_html()));

?>
<div>
    <?php if(count($price) > 1) { ?>
        <div class="cart-item-price-old"><?= $price['0'] ?></div>
        <div class="cart-item-price"><?= $price['1'] ?></div>
    <?php } else { ?>
        <div class="cart-item-price-old cart-item-price-old_empty"></div>
        <div class="cart-item-price"><?= $price['0'] ?></div>
    <?php } ?>
</div>
<?php
/**
 * Open tag in wp-content/template/madshop/woocommerce/title.php 24 line
 * <div class="cart-item-title-wrap">
 */
?>
</div>