<?php
/**
 * Template Name: Home Page
 */

get_header() ?>
    <div class="main-content">
<?php load('header') ?>
    <div class="main">
        <div class="media-menu">
            <ul>
                <li><a href="<?= get_term_link(397, 'product_cat') ?>">Мужское</a></li>
                <li><a href="<?= get_term_link(408, 'product_cat') ?>">Женское</a></li>
                <li><a href="<?= get_term_link(520, 'product_cat') ?>">Скидки</a></li>
            </ul>
        </div>
        <div class="main__content main-slider swiper-container">
            <div class="swiper-pagination"></div>
            <div class="swiper-wrapper ">
				<div class="swiper-slide">
					<a href="/product/krossovki-zhenskie-new-balance-327/">
					<img style="max-width:1200px" src="<?= get_template_directory_uri()?>/img/main-slider/new-balance-327.jpg" class="img-slider" />
					</a>
                </div>
                <div class="swiper-slide">
					<a href="/product/krossovki-adidas-originals-yeezy-boost-350-ash-stone/">
					<img style="max-width:1200px" src="<?= get_template_directory_uri()?>/img/main-slider/yeezy-ash.jpg" class="img-slider" />
					</a>
                </div>
                <div class="swiper-slide">
					<a href="/cat/new/?filters=product_brand[396]">
					<img style="max-width:1200px" src="<?= get_template_directory_uri()?>/img/main-slider/nike-air-1-new.jpg" class="img-slider" />
					</a>
                </div>
            </div>
        </div>
    </div>
    <div class="wrap">
        <div class="collections">
            <div class="collections__content">
                <div class="man">
                    <a href="<?= get_term_link(397, 'product_cat') ?>" class="collections__header">
                        Мужское
						<span>Смотреть</span>
                    </a>
                    <a href="<?= get_term_link(397, 'product_cat') ?>">
                        <img class="collections__img" src="<?= get_template_directory_uri() ?>/img/mad-men.jpg">
                    </a>
                </div>
                <div class="woman">
                    <a href="<?= get_term_link(408, 'product_cat') ?>" class="collections__header">
                        Женское
						<span>Смотреть</span>
                    </a>
                    <a href="<?= get_term_link(408, 'product_cat') ?>">
                        <img class="collections__img"
                             src="<?= get_template_directory_uri() ?>/img/mad-woman.jpg">
                    </a>
                </div>
            </div>
        </div>
        <?php
        $section = get_field('new_products', get_the_ID());
        if (empty($section['products']) === false) {
            ?>
            <div class="latest">
                <div class="latest__header">
                    <a href="/cat/new/"><?= $section['title'] ?></a>
                </div>
                <div class="latest__header latest__header-media">
                    <a href="/cat/new/"><?= $section['title'] ?></a>
                </div>
                <div class="latest__text">
                    <?= $section['subtitle'] ?>
                </div>
                <?php if(count($section['products']) > 4) { ?>
                    <!-- <img src="<?= get_template_directory_uri() ?>/img/arrow-prev.svg" alt="" class="swiper-button-next">
                    <img src="<?= get_template_directory_uri() ?>/img/arrow-prev.svg" alt="" class="swiper-button-prev"> -->
                <?php } ?>
                <div class="latest__slider">
                    <div class="swiper-container latest-slider">
                        <div class="swiper-wrapper">
                            <?php foreach($section['products'] as $product) {
                                $product = (new WC_Product_Factory())->get_product($product->ID);
                                $productId = $product->get_id();
                                $brand = get_the_terms($productId, 'product_brand');
                                $fields = get_fields($productId);
                                $availableVariants = $product->get_available_variations();
                                $availableVariants = getVariationsSort($availableVariants);
                                $cart = WC()->cart->get_cart();
                                $inCart = false;
                                ?>
                                <div class="product swiper-slide">
                                    <div class="product-content">
                                        <div class="product-img">
                                            <img src="<?= wp_get_attachment_url( $product->get_image_id() ); ?>" />
                                            <div class="img-overlay"></div>
                                        </div>
                                        <?php
                                        if (!empty($brand)) { ?>
                                            <div class="product-brand"><?= $brand[0]->name ?></div>
                                            <?php
                                        } ?>

                                        <div class="product-title">
                                            <div class="link-product">
                                                <a href="<?= get_permalink($productId) ?>">
                                                    <?php
                                                    if (!empty($fields['title_on_page'])) {
                                                        echo $fields['title_on_page'];
                                                    } else {
                                                        if (!empty($brand)) {
                                                            echo str_ireplace($brand[0]->name, '', $product->get_title());
                                                        } else {
                                                            echo $product->get_title();
                                                        }
                                                    } ?>
                                                </a>
                                            </div>
                                            <div class="product-type">
                                                <?php
                                                if (!empty($fields['type'])) {
                                                    echo $fields['type'];
                                                } ?>
                                            </div>
                                        </div>
                                        <?php
                                        if (empty($product->get_children())) { ?>
                                            <div class="product-price">
                                                <div class="coming-soon">
                                                    Подробности скоро!
                                                </div>
                                            </div>
                                            <?php
                                        } else { ?>
                                            <div class="product-price">
                                                <?= $product->get_price_html() ?>
                                            </div>
                                            <?php
                                            if (empty($availableVariants) === false) { ?>
                                                <div class="product-size">
                                                    <div class="product-size-title">
                                                        EU размеры в наличии
                                                    </div>
                                                    <div class="product-size-list">
                                                        <?php
                                                        foreach ($availableVariants as $variant) {
                                                            if (empty($variant['term']->name)) {
                                                                continue;
                                                            }
                                                            $addClass = "";
                                                            if (!empty($cart)) {
                                                                foreach ($cart as $itemCart) {
                                                                    if ($variant['variation_id'] != $itemCart['variation_id']) {
                                                                        continue;
                                                                    }
                                                                    $addClass .= "add-to-cart-btn--active";
                                                                    $inCart = true;
                                                                }
                                                            }
                                                            ?>
                                                            <a id="add-to-cart" data-product-id="<?= $productId ?>"
                                                               data-variant-id="<?= $variant['variation_id'] ?>"
                                                               class="product-size-item add-to-cart-btn <?= $addClass ?>">
                                                                <?= $variant['term']->name ?>
                                                            </a>
                                                            <?php
                                                        } ?>
                                                    </div>
                                                    <a class="go-checkout--catalog" href="/checkout" <?= $inCart ? "style=\"display: block;\"" : ""?>>Перейти в корзину</a>
                                                </div>
                                                <?php
                                            }
                                        } ?>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
        <div class="founded-row">
            <div class="founded">
                <div class="dry-cleaning" style="height: 440px">
                    <img src="<?= get_template_directory_uri() ?>/img/founded-bg.png" alt="">
                    <img src="<?= get_template_directory_uri() ?>/img/main-slider/dadadadavarik2.jpg" alt="">
                </div>
            </div>
            <div class="founded">
                <img src="<?= get_template_directory_uri() ?>/img/founded-bg.png" alt="img" class="founded-bg">
                <div class="founded__bg">
                    <div class="founded__header">
                        Founded in <br> August
                    </div>
                    <div class="founded__date">
                        2018
                    </div>
                </div>
            </div>
            <div class="founded">
                <img src="<?= get_template_directory_uri() ?>/img/founded-bg.png" alt="img" class="founded-bg">
                <div class="founded__bg">
                    <div class="founded__header">
                        Founded in <br> August
                    </div>
                    <div class="founded__date">
                        2018
                    </div>
                </div>
            </div>

        </div>

        <div class="about">
            <div class="about__content">
                <div class="about__first-col about-col">
                    <div class="about__header">
                        Химчистка
                    </div>
                    <div class="about__text">
                        <span>Восстановление и очистка кроссовок на профессиональном оборудовании и лучшими материалами от 800 руб</span>
                    </div>
                </div>
                <div class="about__second-col about-col">
                    <div class="about__header">
                        О нас
                    </div>
                    <div class="about__text">Проект Mad Shop задумывался как место притяжения для тех, кто не смыслит
                        свою жизнь без качественных и удобных кроссовок.
                    </div>
                </div>
                <div class="about__third-col about-col">
                    <div class="about__header">
                        Подарочные сертификаты
                    </div>
                    <div class="about__text">
                        Фирменные подарочные сертификаты, с которыми каждый сможет выбрать подарок на свой вкус.
                    </div>
                </div>
            </div>
        </div>
        <div class="brends">
            <!-- <div class="brends-content">
				<a href="/brand/adidas-2/"><img src="<?= get_template_directory_uri() ?>/img/brend1.png" alt="" class="adidas"></a>
                <a href="/brand/nike/"><img src="<?= get_template_directory_uri() ?>/img/brend2.png" alt="" class="nike"></a>
                <a href="/brand/puma/"><img src="<?= get_template_directory_uri() ?>/img/brend3.png" alt=""></a>
                <a href="/brand/reebok/"><img src="<?= get_template_directory_uri() ?>/img/brend4.png" alt=""></a>
                <a href="/brand/new-balance/"><img src="<?= get_template_directory_uri() ?>/img/brend5.png" alt=""></a>
                <a href="/brand/carhartt/"><img src="<?= get_template_directory_uri() ?>/img/brend6.png" alt=""></a>
                <a href="/brand/stussy/"><img src="<?= get_template_directory_uri() ?>/img/brend7.png" alt=""></a>
            </div> -->
            <div class="brends-media swiper-container">
                <div class="swiper-wrapper">
                    <div class="swiper-slide">
                        <a href="/brand/adidas-2/"><img src="<?= get_template_directory_uri() ?>/img/brend1.png" alt="" class="adidas"></a>
                    </div>
                    <div class="swiper-slide">
                        <a href="/brand/nike/"><img class="brends" src="<?= get_template_directory_uri() ?>/img/brend2.png" alt=""
                             class="nike"></a>
                    </div>
                    <div class="swiper-slide">
                        <a href="/brand/puma/"><img src="<?= get_template_directory_uri() ?>/img/brend3.png" alt=""></a>
                    </div>
                    <div class="swiper-slide">
                        <a href="/brand/reebok/"><img src="<?= get_template_directory_uri() ?>/img/brend4.png" alt=""></a>
                    </div>
                    <div class="swiper-slide">
                        <a href="/brand/new-balance/"><img src="<?= get_template_directory_uri() ?>/img/brend5.png" alt=""></a>
                    </div>
                    <div class="swiper-slide">
                        <a href="/brand/carhartt/"><img src="<?= get_template_directory_uri() ?>/img/brend6.png" alt=""></a>
                    </div>
                    <div class="swiper-slide">
                        <a href="/brand/stussy/"><img src="<?= get_template_directory_uri() ?>/img/brend7.png" alt=""></a>
                    </div>
                </div>
            </div>
        </div>
			<div class="yeezy">
				<a href="/brand/yeezy/"><img class="brend-yeezy" src="<?= get_template_directory_uri() ?>/img/yeezy-logo.svg" alt=""></a>
			</div>
		</div>
<?php
get_footer(); ?>