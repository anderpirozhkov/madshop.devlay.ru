<?php
/**
 * Variable product add to cart
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/add-to-cart/variable.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.5.5
 */

defined('ABSPATH') || exit;

global $product;
$variations = [];
$taxonomy = 'pa_size';

$variations = getVariationsSort($available_variations);
$brand = wp_get_post_terms($product->get_id(), 'product_brand');
$fields = get_fields($product->get_id());
$cartItems = WC()->cart->get_cart();
$productInCart = false;
foreach ($cartItems as $cartItem) {
    $productInCart = $cartItem['product_id'] == $product->get_id() ?: false;
}

do_action('woocommerce_before_add_to_cart_form'); ?>
<?php
if (empty($variations) === false) { ?>
    <div class="size-select js-tabs" id="tabs">
        <div class="size-select-top">
            <div class="size-select-title">
                <span class="js-size">Размеры в наличии</span>
            </div>
        </div>
        <div class="js-tabs-content-list">
            <div class="js-tabs__content">
                <div class="size-select-list">
                    <?php
                    foreach ($variations as $variation) {
                        if (empty($variation['term']->name)) {
                            continue;
                        }
                        ?>
                        <div class="size-select-item"
                             data-variant-id="<?= $variation['variation_id'] ?>"><?= str_ireplace('-', '.', $variation['term']->name) ?></div>
                        <?php
                    } ?>
                </div>
            </div>
        </div>
    </div>
    <div class="add-product-notify">Пожалуйста, выберите размер</div>
    <div class="cart-item-btns">
        <a href="/checkout" class="btn go-checkout" <?php
        if ($productInCart) { ?> style="display: block;" <?php
        } else { ?> style="display: none;" <?php
        } ?>>Перейти в корзину</a>
        <a href="" class="btn btn-primary animated-btn add-to-cart-btn btn-product-cart"
           data-product-id="<?= $product->get_id() ?>" <?php
        if ($productInCart) { ?> style="display: none;" <?php
        } else { ?> style="display: block;" <?php
        } ?> >
            Добавить в корзину
        </a>
        <a href="/" data-product-id="<?= $product->get_id() ?>" class="btn btn-secondary add-to-cart-checkout-btn">Купить</a>
    </div>

<?php
} else { ?>
    <div class="cart-item-btns">
        <?php
        if (empty($fields['isSold']) === false) { ?>
            <a class="btn animated-btn go-checkout">Нет в наличии</a>
        <?php
        } elseif (empty($fields['isSold']) && empty($fields['to_plan_start_sales']) === false) { ?>
            <a class="btn animated-btn go-checkout">
                Старт продаж <?= $fields['start_sales'] ?>
            </a>
        <?php
        } else { ?>
            <a class="btn animated-btn go-checkout">Подробности скоро</a>
        <?php
        } ?>
    </div>
<?php
} ?>

<?php
$onlyOnline = get_field('only_online_payment', $product->get_id());
if ($onlyOnline) {
    ?>
    <div class="only-online-payment">
        Только онлайн оплата
    </div>

<?php
} ?>
<div class="cart-item-params accordion js-cart-item-acc">
    <?php
    if (empty($product->post->post_content) === false) { ?>
        <div class="cart-item-param accordion-item _open-alweys">
            <div class="accordion-title">
                <div>Описание</div>
            </div>
            <div class="accordion-list">
                <?= $product->post->post_content ?>
            </div>
        </div>
        <?php
    } ?>
    <div class="cart-item-param row">
        <div class="cart-item-param-title">
            <div>Артикул</div>
        </div>
        <div class="cart-item-param-content">
            <?= str_ireplace(mb_strrchr($product->get_sku(), '-'), '', $product->get_sku()) ?>
        </div>
    </div>
    <?php
        $brandId = !empty($brand[0]) ? $brand[0]->term_id : null;
        $categories = wp_get_post_terms($product->get_id(), 'product_cat', ['fields' => 'ids']);
        $tableData = null;
        $args = [
            'post_type' => 'tables_sizes',
            'meta_query' => [
                [
                    'key' => 'brand',
                    'compare' => '=',
                    'value' => $brandId,
                ]
            ]
        ];
        $tables = get_posts($args);
        if (empty($tables) === false) {
            foreach ($tables as $tableVariant) {
                $tvFields = get_fields($tableVariant->ID);
                $diffCategories = array_diff($categories, $tvFields['category']);
                if(count($diffCategories) === count($categories)) {
                    continue;
                }

                if(empty($tvFields['sex']) === false) {
                    if (in_array($tvFields['sex'], $categories)) {
                        $tableData = $tvFields['table'];
                    }
                } else {
                    $tableData = $tvFields['table'];
                }

            }
        }
        if(empty($tableData) === false) {
    ?>
    <div class="cart-item-param accordion-item <?= empty($product->post->post_content) ? "_open" : "" ?>">
        <div class="accordion-title sub first">
            <div>Таблица размеров</div>
        </div>
        <div class="accordion-list table_size">
            <?php
                load('tableSizes', $tableData)
            ?>
        </div>
    </div>
    <?php } ?>
    <div class="cart-item-param accordion-item <?= empty($tableData) ? "_open" : "" ?>">
        <div class="accordion-title sub">
            <div>Оплата и доставка</div>
        </div>
        <div class="accordion-list">
            <p class="title_tab"><b>Доставка по России</b></p>
            <p>- Курьерская служба СДЭК (Срок доставки 2-10 дней)</p>
            <p>- Почта России (Срок доставки 2-10 дней)</p>
            <p>-&nbsp;При заказе от 10 000 рублей доставка по России <strong>бесплатно.</strong></p>
            <p class="title_tab"><b>Доставка по Екатеринбургу</b></p>
            <p>- Курьером по Екатеринбургу (бесплатно, срок доставки до 2-х дней).</p>
            <p class="title_tab"><b>Оплата</b></p>
            <p>- Оплатить заказ можно наличными/картой при получении, или электронными средствами на сайте.</p>
            <p>- К оплате на сайте принимаются Qiwi, Яндекс.Деньги, карты любых банков РФ и многое другое.</p>
            <p>- Вы можете произвести оплату банковскими картами платежных систем: Visa, Mastercard, МИР.</p></div>
    </div>
    <div class="cart-item-param accordion-item">
        <div class="accordion-title sub">
            <div>Отказ и возврат</div>
        </div>
        <div class="accordion-list">
            <div class="title">Возврат:</div>
            <p>Возврат или обмен товара возможен в течение 14 календарных дней,
                не считая дня совершения покупки в нашем магазине на ул. Радищева.</p>
            <p>Не подошедший товар необходимо отправить нам обратно через Почту России, либо курьерской службой по
                адресу:</p>
            <p>— Куда: 620014, г. Екатеринбург ул. Радищева 25.</p>
            <p>— Кому: ИП Мазурков А.Д.</p>
            <p>К возврату принимаются изделия в товарном виде: без следов эксплуатации, с заводскими бирками и в
                оригинальной упаковке.</p>
        </div>
    </div>
</div>
<?php if(empty($brand) === false) {?>
<div class="still-for-brand">
    <a href="<?= get_term_link($brand[0]->term_id) ?>">Еще от <?= $brand[0]->name ?></a>
</div>
<?php } ?>