<?php

define('VERSION_STYLE', '1.0.20');
define('VERSION_SCRIPTS', '1.0.20');
add_theme_support( 'title-tag' );
add_filter('woocommerce_enqueue_styles', '__return_empty_array');
add_action('wp_enqueue_scripts', 'theme_add_scripts');
function theme_add_scripts()
{
    // подключаем файл стилей темы
    wp_enqueue_style('libs', get_template_directory_uri().'/css/libs.min.css', false);
    wp_enqueue_style('menu', get_template_directory_uri().'/css/menu-style.css', false, VERSION_STYLE);
    wp_enqueue_style('main', get_template_directory_uri().'/css/main.css', false, VERSION_STYLE);
    if (is_front_page() || is_page_template('main-2.php')) {
        wp_enqueue_style('main-v2', get_template_directory_uri().'/css/main-v2.css', false, VERSION_STYLE);
    }
    if (is_shop() === false && is_page_template('popular.php') === false) {
        wp_enqueue_style('swiper', get_template_directory_uri().'/css/swiper-bundle.css', false);
        wp_enqueue_style('swiper-min', get_template_directory_uri().'/css/swiper-bundle.min.css', false);
        wp_enqueue_style('twentytwenty', get_template_directory_uri().'/css/twentytwenty.css', false);
    }
    // подключаем js файл темы
    if (is_shop() === false && is_page_template('popular.php') === false) {
        wp_enqueue_script('swiper', get_template_directory_uri().'/js/swiper.js', array('jquery'), VERSION_SCRIPTS, true);
        wp_enqueue_script('swiper-min', get_template_directory_uri().'/js/swiper.min.js', array('jquery'), VERSION_SCRIPTS, true);
        wp_enqueue_script('twentytwenty', get_template_directory_uri().'/js/jquery.twentytwenty.js', array('jquery'), VERSION_SCRIPTS, true);
    }
    wp_enqueue_script('libs', get_template_directory_uri().'/js/libs.min.js', array('jquery'), VERSION_SCRIPTS, true);
    wp_enqueue_script('event-move', get_template_directory_uri().'/js/jquery.event.move.js', array('jquery'),VERSION_SCRIPTS,true);
    wp_enqueue_script('index', get_template_directory_uri().'/js/index.js', array('jquery'), VERSION_SCRIPTS, true);
    wp_enqueue_script('add-to-cart', get_template_directory_uri().'/js/add-to-cart.js', array('jquery'),VERSION_SCRIPTS,true);
    wp_enqueue_script('filter', get_template_directory_uri().'/js/filter.js', array('jquery'), VERSION_SCRIPTS, true);

}
function load(string $component, $data = null)
{
    if (file_exists(get_template_directory()."/components/{$component}/{$component}.php")) {
        require(get_template_directory()."/components/{$component}/{$component}.php");
    }
}
add_theme_support('woocommerce');
function remove_hooks_loop()
{
    remove_action('woocommerce_before_shop_loop', 'woocommerce_result_count', 20);
    remove_action('woocommerce_before_shop_loop', 'woocommerce_output_all_notices', 30);
}
add_action('woocommerce_before_shop_loop', 'remove_hooks_loop', 1);
function remove_functions()
{
    remove_action('woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10);
    remove_action('woocommerce_after_single_product_summary', 'woocommerce_upsell_display', 15);
}
add_action('woocommerce_after_single_product_summary', 'remove_functions', 1);
function remove_breadcrumbs()
{
    remove_action('woocommerce_before_main_content', 'woocommerce_breadcrumb');
}
add_action('woocommerce_before_main_content', 'remove_breadcrumbs', 1);
function updateQuantityAjax()
{
    $result = WC()->cart->set_quantity($_POST['key'], $_POST['quantity']);
    die($result);
}
add_action('wp_ajax_update_quantity', 'updateQuantityAjax');
add_action('wp_ajax_nopriv_update_quantity', 'updateQuantityAjax');
function sv_remove_cart_notice_on_checkout()
{
    if (function_exists('wc_cart_notices')) {
        remove_action('woocommerce_before_checkout_form', array(wc_cart_notices(), 'add_cart_notice'));
    }
}
add_action('init', 'sv_remove_cart_notice_on_checkout');
add_theme_support('widgets');
if (function_exists('register_sidebar')) {
    register_sidebar(
        array(
            'name' => 'Фильтр продуктов ', //название виджета в админ-панели
            'id' => 'filter-products', //идентификатор виджета
            'description' => '', //описание виджета в админ-панели
            'before_widget' => '<div class="cart-filter">', //открывающий тег виджета с динамичным идентификатором
            'after_widget' => '<div class="clear"></div></div>', //закрывающий тег виджета с очищающим блоком
            'before_title' => '<span class="widget-title">', //открывающий тег заголовка виджета
            'after_title' => '</span>',//закрывающий тег заголовка виджета
        )
    );
}
function ddJSON($data)
{
    echo "<script>console.log(".json_encode($data, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_FORCE_OBJECT).")</script>";
}
function apply_coupon()
{
    WC()->cart->remove_coupons();
    $couponCode = $_POST['code'];
    $coupon = new WC_Coupon($couponCode);
    if ($coupon->is_valid()) {
        $ret = WC()->cart->add_discount($couponCode);
        if ($coupon->get_discount_type() == 'percent') {
            $coupon_amount = $coupon->get_amount();
            $coupon_amount_label = '%';
        }
        if ($coupon->is_type('fixed_cart')) {
            $coupon_amount = $coupon->get_amount();
            $coupon_amount_label = get_woocommerce_currency_symbol(
                get_woocommerce_currency()
            );
        }
        wp_send_json(
            ['success' => $ret, 'amount' => $coupon_amount],
            200,
            JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_LINE_TERMINATORS
        );
        wp_die();
    }

    print_r(['success' => 0]);
    wp_die();
}
add_action('wp_ajax_apply_coupon', 'apply_coupon');
add_action('wp_ajax_nopriv_apply_coupon', 'apply_coupon');
function remove_coupon()
{
    WC()->cart->remove_coupons();
    WC()->cart->calculate_totals();
    print_r(1);
    die();
}
add_action('wp_ajax_remove_coupon', 'remove_coupon');
add_action('wp_ajax_nopriv_remove_coupon', 'remove_coupon');
function get_cart_data()
{
    $couponsTotalDiscount = WC()->cart->get_coupon_discount_totals();
    $cartTotal = WC()->cart->get_cart_total();
    print_r(json_encode(['discount' => $couponsTotalDiscount, 'total_cart' => $cartTotal]));
    die();
}
add_action('wp_ajax_get_cart_data', 'get_cart_data');
add_action('wp_ajax_nopriv_get_cart_data', 'get_cart_data');
function addToCart()
{
    $data = $_POST['data'];
    if (empty($data['product_id']) && !empty($data['variation_id'])) {
        $data['product_id'] = wp_get_post_parent_id($data['variation_id']);
    }
    $product_id = apply_filters('woocommerce_add_to_cart_product_id', absint($data['product_id']));
    $quantity = empty($data['quantity']) ? 1 : wc_stock_amount($data['quantity']);
    $variation_id = absint($data['variation_id']);
    $passed_validation = apply_filters('woocommerce_add_to_cart_validation', true, $product_id, $quantity);
    $product_status = get_post_status($product_id);
    if ($passed_validation && WC()->cart->add_to_cart(
            $product_id,
            $quantity,
            $variation_id
        ) && 'publish' === $product_status) {
        do_action('woocommerce_ajax_added_to_cart', $product_id);

        if ('yes' === get_option('woocommerce_cart_redirect_after_add')) {
            wc_add_to_cart_message(array($product_id => $quantity), true);
        }

        $response = array(
            'error' => false,
            'cartCountItem' => WC()->cart->get_cart_contents_count(),
            'cartItems' => getBasketHover(),
        );
    } else {
        $response = array(
            'error' => true,
            'product_url' => apply_filters(
                'woocommerce_cart_redirect_after_error',
                get_permalink($product_id),
                $product_id
            )
        );
    }

    echo wp_send_json($response);
    wp_die();
}
add_action('wp_ajax_add_to_cart', 'addToCart');
add_action('wp_ajax_nopriv_add_to_cart', 'addToCart');
function getBasketHover()
{
    $cartData = WC()->cart->get_cart_contents();
    $headBasketHover = "<div class=\"basket-hover\" id=\"basket-hover\">
                            <div class=\"basket\"><div class=\"basket-close\">x</div>";
    $footBasketHover = "</div>
                            <div class=\"summa\">
                              <span class=\"summa__text\">
                                 Сумма заказа
                              </span>
                                <span class=\"summa_number\">
                                 ".WC()->cart->get_cart_total()."
                              </span>
                            </div>
                            <a href=\"/checkout\" class=\"basket-hover__btn\">
                                Оформить заказ
                            </a>
                        </div>";
    $contentBasketHover = "";
    $cartItems = [];
    $_pf = new WC_Product_Factory();
    foreach ($cartData as $cartItem) {
        if (empty($cartItems[$cartItem['product_id']])) {
            $fields = get_fields($cartItem['product_id']);
            $brand = wp_get_post_terms(
                $cartItem['product_id'],
                'product_brand',
                array('orderby' => 'name', 'fields' => 'names')
            );
            $brand = count($brand) > 0 ? $brand[0] : "";
            $cartItems[$cartItem['product_id']] = [
                'type' => $fields['type'] ?: '',
                'link' => get_permalink($cartItem['product_id']),
                'image' => wp_get_attachment_url($cartItem['data']->get_image_id()),
                'titleOnPage' => $fields['title_on_page'] ?: '',
                'quantity' => 1,
                'price' => explode(' ', strip_tags($cartItem['data']->get_price_html())),
            ];
            $cartItems[$cartItem['product_id']]['sizes'][] = str_replace(
                '-',
                '.',
                $cartItem['variation']['attribute_pa_size']
            );
        } else {
            $cartItems[$cartItem['product_id']]['sizes'][] = str_replace(
                '-',
                '.',
                $cartItem['variation']['attribute_pa_size']
            );
            $cartItems[$cartItem['product_id']]['quantity']++;
        }
    }
    foreach ($cartItems as $productId => $cartItem) {
        $product = $_pf->get_product($productId);
        $isSimple = $product->is_type('simple');
        if (count($cartItem['price']) > 1) {
            $basketCost = "<div class=\"basket__cost_stock\">".$cartItem['price'][0]."</div>
            <div class=\"basket__cost_number\">
                ".$cartItem['price'][1]."
            </div>";
        } else {
            $basketCost = "<div class=\"basket__cost_number\">".$cartItem['price'][0]."</div>";
        }
        if ($isSimple === false) {
            $sizes = "<div class=\"basket__about_size\">
                        <div class=\"basket__about_size-header\">
                            Размер
                        </div>
                        <div class=\"basket__about_size-number\">".implode(", ", $cartItem['sizes'])."
                        </div>
                    </div>";
        } else {
            $sizes = "";
        }
        $contentBasketHover .= "<div class=\"basket__item\">
			<div class=\"basket-hover-img\">
            <img class=\"item-fba\" src=\"".$cartItem['image']."\" alt=\"\">
			</div>
            <div class=\"basket__about\">
                <div class=\"basket__about_category\">".$cartItem['type']."</div>
                <div class=\"basket__about_name\">".$brand.' '.$cartItem['titleOnPage']."</div>
                <div class=\"basket__about_feature\">".$sizes."
                    
                </div>
            </div>
            <div class=\"basket__cost\">".$basketCost."
                <a data-product-id=\"".$productId."\" class=\"basket__cost_delete\">удалить</a>
            </div>
        </div>";
    }
    return trim($headBasketHover).trim($contentBasketHover).trim($footBasketHover);
}
function removeItemCart()
{
    $productId = $_POST['data']['product_id'];
    $variantId = $_POST['data']['variation_id'] ?: null;
    $cart = WC()->cart;
    $cartItems = $cart->get_cart();
    if (empty($cartItems)) {
        $response = array(
            'error' => false,
            'cartCountItem' => 0,
            'cartItems' => null,
        );
    } else {
        foreach ($cartItems as $item) {
            if ($variantId) {
                if ($item['variation_id'] != $variantId) {
                    continue;
                }
            } else {
                if ($item['product_id'] != $productId) {
                    continue;
                }
            }
            $cart->remove_cart_item($item['key']);
        }

        $response = array(
            'error' => false,
            'cartCountItem' => $cart->get_cart_contents_count(),
            'cartItems' => getBasketHover(),
        );
    }

    echo wp_send_json($response);
    wp_die();
}
add_action('wp_ajax_remove_item_cart', 'removeItemCart');
add_action('wp_ajax_nopriv_remove_item_cart', 'removeItemCart');
function addSubscriber()
{
    $email = $_POST['email'];

    if ($email === null) {
        wp_send_json(['success' => 0, 'message' => 'Нужен ваш email']);
        wp_die();
    }

    $post = get_page_by_title($email, ARRAY_A, 'subscribers');

    if (empty($post) === false) {
        wp_send_json(['success' => 0, 'message' => 'Вы уже подписаны. Спасибо.']);
        wp_die();
    }

    wp_insert_post(
        [
            'post_status' => 'draft',
            'post_type' => 'subscribers',
            'ping_status' => get_option('default_ping_status'),
            'post_title' => $email
        ]
    );

    wp_send_json(['success' => 1, 'message' => 'Спасибо. Мы вас уведомим о новинках']);
    wp_die();
}
add_action('wp_ajax_add_subscriber', 'addSubscriber');
add_action('wp_ajax_nopriv_add_subscriber', 'addSubscriber');

add_filter('woocommerce_available_payment_gateways', 'delete_gateway_category');
function delete_gateway_category($availableGateways)
{
    global $woocommerce;

    foreach ($woocommerce->cart->cart_contents as $key => $item) {
        $onlyOnlinePayment = get_field('only_online_payment', $item['product_id']);
        if ($onlyOnlinePayment) {
            foreach ($availableGateways as $type => $availableGateway) {
                if ($type !== 'yookassa_epl') {
                    unset($availableGateways[$type]);
                }
            }
        }
    }

    return $availableGateways;
}
$cf = function () { return 100; };
add_filter('woocommerce_background_image_regeneration', '__return_false');
add_filter('jpeg_quality', $cf);
function getVariationsSort($availableVariations)
{
    $taxonomy = 'pa_size';
    foreach ($availableVariations as $variant) {
        $meta = get_post_meta($variant['variation_id'], 'attribute_'.$taxonomy, true);
        $term = get_term_by('slug', $meta, $taxonomy);
        $order = null;
        if (empty($term) === false) {
            $order = get_field('order', 'pa_size_'.$term->term_id);
        }
        $variant['meta'] = $meta;
        $variant['term'] = $term;
        if ($order) {
            $variations[$order] = $variant;
        } else {
            $variations[] = $variant;
        }
    }

    if (is_array($variations)) {
        ksort($variations);
    }

    return $variations;
}
function attributesSort($attributes)
{
    $sizesСlothes = [
        's',
        'm',
        'l',
        'xl',
        'xxl',
        '2xl',
        'xxxl'
    ];
    $returned = [];
    foreach ($attributes['pa_size'] as $attribute) {
        if (in_array(strtolower($attribute), $sizesСlothes) === false) {
            continue;
        }
        $term = get_term_by('slug', $attribute, 'pa_size');
        $order = get_field('order', 'pa_size_'.$term->term_id);
        $returned['pa_size'][$order] = $attribute;
    }
    if (empty($returned)) {
        return $attributes;
    }
    ksort($returned['pa_size']);

    return $returned;
}
remove_action('woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30);
add_action('woocommerce_before_shop_loop', 'catalog_sort', 20);
function catalog_sort($isMobile = false)
{
    if (!wc_get_loop_prop('is_paginated') || !woocommerce_products_will_display()) {
        return;
    }
    $show_default_orderby = 'menu_order' === apply_filters(
            'woocommerce_default_catalog_orderby',
            get_option(
                'woocommerce_default_catalog_orderby',
                'menu_order'
            )
        );
    $catalog_orderby_options = apply_filters(
        'woocommerce_catalog_orderby',
        array(
            'date' => __('Сначала новые', 'woocommerce'),
            'price' => __('Дешевле выше', 'woocommerce'),
            'price-desc' => __('Дороже выше', 'woocommerce'),
        )
    );

    $default_orderby = wc_get_loop_prop('is_search') ? 'relevance' : apply_filters(
        'woocommerce_default_catalog_orderby',
        get_option('woocommerce_default_catalog_orderby', '')
    );
    // phpcs:disable WordPress.Security.NonceVerification.Recommended
    $orderby = isset($_GET['orderby']) ? wc_clean(wp_unslash($_GET['orderby'])) : $default_orderby;
    // phpcs:enable WordPress.Security.NonceVerification.Recommended

    if (wc_get_loop_prop('is_search')) {
        $catalog_orderby_options = array_merge(
            array('relevance' => __('Relevance', 'woocommerce')),
            $catalog_orderby_options
        );

        unset($catalog_orderby_options['menu_order']);
    }

    if (!$show_default_orderby) {
        unset($catalog_orderby_options['menu_order']);
    }

    if (!wc_review_ratings_enabled()) {
        unset($catalog_orderby_options['rating']);
    }

    if (!array_key_exists($orderby, $catalog_orderby_options)) {
        $orderby = current(array_keys($catalog_orderby_options));
    }

    wc_get_template(
        'loop/orderby.php',
        array(
            'catalog_orderby_options' => $catalog_orderby_options,
            'orderby' => $orderby,
            'show_default_orderby' => $show_default_orderby,
            'isMobile' => $isMobile
        )
    );
}
function append_slug($data) {
    global $post_ID;
    $title = str_ireplace('/', '', $data['post_title']);
    $title = str_ireplace('\\', '', $title);
    if (empty($data['post_name'])) {
        $data['post_name'] = sanitize_title($title, $post_ID);
    }

    return $data;
}
add_filter('wp_insert_post_data', 'append_slug', 10);

function getAjaxProducts()
{
    $page = $_POST['pagePath'];

    $postProducts = array(
        'limit' => 60,
        'posts_per_page' => 60,
        'paged' => 1,
        'order' => 'desc',
        'post_type' => 'product',
        'orderby' => 'post_views',
        'paginate' => true,
    );
    $wpQuery = new WP_Query($postProducts);
    $posts = [];
    if ($wpQuery->have_posts()) {
        foreach($wpQuery->get_posts() as $post) {
            $product = wc_get_product($post->ID);
            setup_postdata($GLOBALS['post'] =& $post);
            $products[] = include 'woocommerce/content-product.php';
        }
        wp_reset_postdata();
    } else {
        do_action('woocommerce_no_products_found');
    }
    print_r($posts, true); die();

}
add_action('wp_ajax_get_products', 'getAjaxProducts');
add_action('wp_ajax_nopriv_get_products', 'getAjaxProducts');