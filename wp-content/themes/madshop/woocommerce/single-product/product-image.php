<?php
/**
 * Single Product Image
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/product-image.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.5.1
 */

defined( 'ABSPATH' ) || exit;

// Note: `wc_get_gallery_image_html` was added in WC 3.3.2 and did not exist prior. This check protects against theme overrides being used on older versions of WC.
if ( ! function_exists( 'wc_get_gallery_image_html' ) ) {
	return;
}

global $product;

$gallery = $product->get_gallery_image_ids();
array_unshift($gallery, $product->get_image_id());
?>
    <div class="cart-item-slider-wrap">
        <div class="cart-item-slider-container">
            <div class="cart-item-slider">
                <?php foreach($gallery as $order => $image) {?>
                <div class="slide-img-bg">
                    <div data-slide-id="<?= $order ?>" class="slide-img">
						<img src="<?= wp_get_attachment_image_url($image, [600, 600]) ?>" alt="">
						<div class="img-overlay-slider"></div>
                    </div>
                </div>
                <?php } ?>
            </div>
        </div>
        <div class="cart-item-slider-preview">
            <?php foreach($gallery as $order => $image) {?>
                        <div data-slide-prev-id="<?= $order ?>" class="slide-img _active">
                            <img src="<?= wp_get_attachment_image_url($image, [450, 450]) ?>" alt="">
							<div class="slider-preview-overlay"></div>
                        </div>
            <?php } ?>
        </div>
        <div class="original">
            <div class="original-icon">
                <div class="icon icon-original"></div>
            </div>
            <div class="original-drop">
                <div class="original-drop-title">
                    <div>100% — Только</div>
                    оригинальные бренды
                </div>
                <div class="original-drop-link">
                    <a href="">Гарантия подлинности</a>
                </div>
            </div>
        </div>
    </div>