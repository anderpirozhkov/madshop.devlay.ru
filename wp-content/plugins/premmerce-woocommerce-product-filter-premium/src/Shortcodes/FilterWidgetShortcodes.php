<?php namespace Premmerce\Filter\Shortcodes;

use Premmerce\Filter\Filter\Container;
use Premmerce\Filter\Widget\FilterWidget;
use Premmerce\SDK\V2\FileManager\FileManager;
use Premmerce\Filter\Widget\ActiveFilterWidget;

class FilterWidgetShortcodes
{
    /**
     * @var FileManager
     */
    private $fileManager;

    /**
     * Shortcode constructor
     */
    public function __construct(FileManager $fileManager)
    {
        $this->fileManager = $fileManager;

        //register shortcodes
        add_shortcode('premmerce_filter', [$this, 'premmerceShortcodeFilter']);
        add_shortcode('premmerce_active_filters', [$this, 'premmerceShortcodeActiveFilters']);
    }


    /**
     * Shortcode data and render template
     */
    public function premmerceShortcodeFilter($atts) {
        //check if it is woocomerce pages and we can show this shortcode
        if (apply_filters('premmerce_product_filter_active', false)) {
            //take data from FilterWidget class
            $data = FilterWidget::getFilterWidgetContent();

            //render filter
            return $this->fileManager->renderTemplate('frontend/filter.php', $data);
        }
    }

    /**
     * Shortcode data and render template
     */
    public function premmerceShortcodeActiveFilters($atts) {
        //check if it is woocomerce pages and we can show this shortcode
        if (apply_filters('premmerce_product_filter_active', false)) {
            //take data from ActiveFilterWidget class
            $data = (new ActiveFilterWidget)->getActiveFilterWidgetContent();

            //render filter
            return $this->fileManager->renderTemplate('frontend/active_filters.php', $data);
        }
    }
}
