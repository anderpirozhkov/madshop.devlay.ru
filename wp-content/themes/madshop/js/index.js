// мобильное меню
let html = document.querySelector('html')
let menuBtn = document.querySelector('.menu-btn')
let menuArr = document.querySelector('.m-menu-primary')
let menuBtnClose = document.querySelector('.m-menu-close')
let menuMobile = document.querySelector('.m-menu-wrap')
let menuSubMobile = document.querySelectorAll('.m-menu-sub')
let menuSubBtnClose = document.querySelectorAll('.m-menu-sub-close')

if (menuBtn !== null) {
    menuBtn.addEventListener('click', function (e) {
        let item = e.target.closest('.menu-btn')
        if (!item) return;
        menuMobile.classList.add('_open')
        html.classList.add('_blocked')
    })
}

if (menuBtnClose !== null) {
    menuBtnClose.addEventListener('click', function (e) {
        let item = e.target.closest('.m-menu-close')
        if (!item) return;
        menuMobile.classList.remove('_open')
        html.classList.remove('_blocked')
    })
}

if (menuMobile !== null) {
    menuMobile.addEventListener('click', function (e) {
        let item = e.target
        if (item.classList.contains('m-menu-wrap')) {
            menuMobile.classList.remove('_open')
            html.classList.remove('_blocked')
        }
    })
}

if (menuArr !== null) {
    menuArr.addEventListener('click', function (e) {
        let item = e.target.closest('.arr')
        if (!item) return;
        if (item.parentNode.lastElementChild.classList.contains('m-menu-sub')) {
            item.parentNode.lastElementChild.classList.add('_open')
        }
    })
}

if (menuSubMobile !== null) {
    for (let msbClose of menuSubMobile) {
        msbClose.addEventListener('click', function (e) {
            let item = e.target
            if (item.classList.contains('m-menu-sub')) {
                msbClose.classList.remove('_open')
            }
        })
    }
}

if (menuSubBtnClose !== null) {
    for (let msbClose of menuSubBtnClose) {
        msbClose.addEventListener('click', function (e) {
            let item = e.target.closest('.m-menu-sub-close')
            if (!item) return;
            item.closest('.m-menu-sub').classList.remove('_open')
        })
    }
}


// слайдер на карточке товара

let cis = document.querySelector('.cart-item-slider')

if (cis !== null) {
    let slider
    const enableSlider = function () {
        slider = tns({
            container: '.cart-item-slider',
            items: 1,
            slideBy: 'page'
        })
    }

    const breakpoint = window.matchMedia('(min-width:768px)');

    const breakpointChecker = function () {
        breakpoint.matches === true
            ? slider !== undefined && slider.destroy()
            : enableSlider()
    }

    breakpoint.addEventListener('change', breakpointChecker)
    breakpointChecker()
}


// табы размеров на странице товара

let tabsB = document.getElementById('tabs')
if (tabsB !== null) {
    var tabs = new Tabs({
        elem: "tabs"
    });
}


var animatedBtn = document.querySelector(".animated-btn");

var animateButton = function () {

    //reset animation
    animatedBtn.classList.remove('animate');

    animatedBtn.classList.add('animate');
    setTimeout(function () {
        animatedBtn.classList.remove('animate');
    }, 700);
};


// выбор размера на странице товара и изменение заголовка

let sizeItemAll = document.querySelectorAll('.size-select-item')
let sizes = document.querySelector('.js-tabs-content-list')
let sizeCount = document.querySelector('.js-size')

if (sizes !== null) {
    sizes.addEventListener('click', function (e) {
        let item = e.target.closest('.size-select-item')
        animatedBtn.classList.remove('btn-product-cart')
        if (!item) return;
        if (!item.classList.contains('_disable') && !item.classList.contains('_active')) {
            sizeCount.innerHTML = 'Размер ' + e.target.innerHTML
            for (let sizeItem of sizeItemAll) {
                sizeItem.classList.remove('_active')
                animateButton()
            }
            item.classList.add('_active')
        } else {
            item.classList.remove('_active')
            animatedBtn.classList.add('btn-product-cart');
            sizeCount.innerHTML = 'Выберите размер'
        }
    })
}

let cartDesctopSlider = document.querySelector('.cart-item-slider')
if (cartDesctopSlider !== null) {
    var cartDesctopSlideHeight = cartDesctopSlider.firstElementChild.clientHeight
}

let sliderPrev = document.querySelector('.cart-item-slider-preview')
let sliderPrevAll = document.querySelectorAll('.cart-item-slider-preview .slide-img')

if (sliderPrev !== null) {
    sliderPrev.addEventListener('click', function (e) {
        let item = e.target.closest('.slide-img')
        if (!item) return;

        for (let prevItem of sliderPrevAll) {
            prevItem.classList.remove('_active')
        }
        item.classList.add('_active')

        let prevId = item.dataset.slidePrevId
        cartDesctopSlider.scrollTop = prevId * cartDesctopSlideHeight
    })
}


// кастомный селект в сортировке

const sortSelect = document.querySelector('.js-choice');
if (sortSelect !== null) {
    const choices = new Choices(sortSelect, {
        searchEnabled: false
    });
}


// кастомный селект в сортировке
if (typeof (hideChoiceM) === 'undefined' || !hideChoiceM) {
    const sortSelectMobile = document.querySelector('.js-choice-m');
    if (sortSelectMobile !== null) {
        const choicesMobile = new Choices(sortSelectMobile, {
            searchEnabled: false
        })
    }
}

// аккордионы

class accController {
    constructor(element) {
        this.element = element;
        this.accItems = this.element.querySelectorAll('.accordion-item');
        this.init();
    }

    init() {
        let elem = this.element
        let accItems = this.accItems
        for (let accItem of accItems) {
            let accTitle = accItem.querySelector('.accordion-title');
            if (accTitle) {
                accTitle.addEventListener('click', function (e) {
                    if (!elem.classList.contains('type2')) {
                        for (let other of accItems) {
                            other.classList.remove('_open')
                        }
                        accItem.classList.add('_open')
                    } else {
                        if (accItem.classList.contains('_open')) {
                            accItem.classList.remove('_open')
                        } else {
                            accItem.classList.add('_open')
                        }
                    }
                })
            }
        }
    }
}

let cartFilterAcc = document.querySelector('.js-cart-acc');
if (cartFilterAcc !== null) {
    new accController(cartFilterAcc);
}

let cartFilterAccMobile = document.querySelector('.js-cart-acc-m');
if (cartFilterAccMobile !== null) {
    new accController(cartFilterAccMobile);
}

let footerAcc = document.querySelector('.js-footer-acc');
if (footerAcc !== null) {
    new accController(footerAcc);
}

let cartItemAcc = document.querySelector('.js-cart-item-acc');
if (cartItemAcc !== null) {
    new accController(cartItemAcc);
}


// мобильный фильтр

let openMobileFilter = document.querySelector('.js-mobile-filter-open');
let closeMobileFilter = document.querySelector('.js-mobile-filter-close');
let mobileFilter = document.querySelector('.m-filter-wrap');

// if (openMobileFilter !== null) {
//    openMobileFilter.addEventListener('click', function (e) {
//       // mobileFilter.classList.add('_open')
//       // html.classList.add('_blocked')
//    })
// }

if (closeMobileFilter !== null) {
    closeMobileFilter.addEventListener('click', function (e) {
        mobileFilter.classList.remove('_open')
        html.classList.remove('_blocked')
    })
}

const searchBtn = document.getElementById("icon-search")
const searchInputField = document.getElementsByClassName("yith-s");
const searchInput = document.getElementById("search-box")
const basketBtn = document.getElementById("icon-basket")
const basketBox = document.getElementById("basket-hover")
const imgSlider = document.getElementsByClassName("latest-slider")
document.addEventListener('DOMContentLoaded', function () {
    searchBtn.addEventListener('click', function () {
        searchBtn.classList.toggle("icon-search--hide");
        searchInput.classList.toggle("search-active")
        searchInputField[0].focus();
    });

    imgSlider.onmouseenter = function () {
        this.className.toggle("basket-active")
    };
});

if (typeof (Swiper) !== "undefined" ) {
    var swiper = new Swiper(".latest-products-swiper", {
        slidesPerView: 4,
        spaceBetween: 20,
        loop: true,
        loopFillGroupWithBlank: true,
        pagination: {
            clickable: true
        }
    });
    new Swiper('.latest-slider', {
        // Optional parameters
        direction: 'horizontal',
        loop: true,
        slidesPerView: 4,
        spaceBetween: 15,
        // Navigation arrows
        breakpoints: {
            900: {
                slidesPerView: 4,
            },
            700: {
                slidesPerView: 3,
            },
            320: {
                slidesPerView: 2,
            },
        },
    })
    new Swiper('.main-slider', {
        // Optional parameters
        direction: 'horizontal',
        loop: true,
        slidesPerView: 1,
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
        },
    })
    new Swiper('.img-slider', {
        // Optional parameters
        direction: 'horizontal',
        grabCursor: true,
        loop: true,
        slidesPerView: 1,
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
        },
        nested: true,
    })
    new Swiper('.brends-media', {
        direction: 'horizontal',
        grabCursor: true,
        loop: true,
        slidesPerView: 3,
    })
}

jQuery(function ($) {
    $(window).load(function () {
        if (typeof ($('#twentytwenty_top').twentytwenty) !== "undefined") {
            $('#twentytwenty_top').twentytwenty(
                {
                    move_with_handle_only: true,
                    before_label: '',
                    after_label: '',
                    no_overlay: true,
                    move_slider_on_hover: true,
                    click_to_move: true
                }
            );
        }
        if (typeof ($('#twentytwenty_top').twentytwenty) !== "undefined") {
            $(".dry-cleaning").twentytwenty({
                default_offset_pct: 0.5,
                orientation: 'horizontal'
            });
        }

        if ($('.filter__inner').length > 0) {
            $($('.filter__inner')[0]).parent('.filter__item').find('.filter__title').addClass('active');
            $($('.filter__inner')[0]).addClass('active');
        }
    });
    $(document).ready(function () {
        if (typeof(Swiper) !== "undefined") {
            new Swiper('#popular__slider', {
                slidesPerView: 4,
                spaceBetween: 5,
                slidesPerGroup: 4,
                loop: true,
                loopFillGroupWithBlank: true,
                pagination: {
                    el: '.popular__swiper-container__swiper-pagination',
                    clickable: true,
                },
                navigation: {
                    nextEl: '.popular__swiper-container__swiper-button-next',
                    prevEl: '.popular__swiper-container__swiper-button-prev',
                },
                breakpoints: {
                    280: {
                        slidesPerView: 2,
                        slidesPerGroup: 2,
                        spaceBetween: 0
                    },
                    320: {
                        slidesPerView: 2,
                        slidesPerGroup: 2,
                        spaceBetween: 0
                    },
                    768: {
                        slidesPerView: 4,
                        slidesPerGroup: 4,
                        spaceBetween: 5
                    }
                }
            });
        }
        addClassSortOption();
        $('input[type="search"]').attr('autofocus', true);
        $(document).on('click', '.page-numbers', function () {
            $("html, body").animate({scrollTop: 0}, "slow");
            return false;
        });
        $(document).on('click', '.apply_coupon', function (e) {
            e.preventDefault();
            let code = $('.basket__price_promo').val();
            $.ajax({
                type: "POST",
                async: false,
                url: ajax_url,
                data: {action: 'apply_coupon', code: code},
                success: applied_coupon,
            });
        })
        $(document).on('click', '.remove_coupon', function (e) {
            e.preventDefault();
            let code = $('.basket__price_promo').val();
            $.post(ajax_url, {action: 'remove_coupon', code: code}, removed_coupon, 'JSON');
        });
        $(document).on('click', '.basket-close', function (e) {
            $('#basket-hover').removeClass('basket-active');
        })
        $(document).on('click', '#icon-basket', function (e) {
            if (window.innerWidth > 425) {
                location.href = "/checkout";
            } else {
                if ($('#basket-hover').is(":visible")) {
                    $('#basket-hover').removeClass('basket-active');
                } else {
                    $('#basket-hover').addClass('basket-active');
                }
            }
        });
        function addClassSortOption() {
            $('select[name="orderby"] option').addClass('option-sort');
        }
        function applied_coupon(response) {
            if (response.success == 1) {
                if ($('.applied-coupon--filed').is(":visible")) {
                    $('.applied-coupon--failed').hide()
                }
                $('.applied-coupon--success').show();
                $('.remove-coupon--success').hide();
                $('input.basket__price_promo').attr('disabled', 'disabled');
                $('input.basket__price_promo').val(response.amount + " ₽");
                let button = $('.apply_coupon');
                button.addClass('remove_coupon');
                button.removeClass('apply_coupon');
                button.html("<img src=\"/wp-content/themes/madshop/img/close.svg\" >")
            } else {
                $('.applied-coupon--success').hide();
                $('.remove-coupon--success').hide();
                $('.applied-coupon--failed').show();
            }
        }
        function removed_coupon(data) {
            let button = $('.remove_coupon');
            $('.applied-coupon--success').hide();
            $('.applied-coupon--failed').hide();
            $('.remove-coupon--success').show();
            $('input.basket__price_promo').val('');
            $('input.basket__price_promo').removeAttr('disabled');
            button.addClass('apply_coupon');
            button.removeClass('remove_coupon');
            button.html('OK')

            $.post(ajax_url, {action: 'get_cart_data'}, function (response) {
                $('.basket__price_total div:last-child').html(response.total_cart);
            }, 'JSON');
        }
        $(document).on('keypress', function (e) {
            if ($(".basket__price_promo").is(":focus") && e.which == 13) {
                $('.apply_coupon').trigger("click");
            }
        });
        $(document).on('submit', '.subscribe-form', function (e) {
            e.preventDefault();
            $('.subscribe-form--overlay').show();
            let email = $("#subscriberEmail").val();
            $.post(ajax_url, {action: 'add_subscriber', email: email}, function (response) {
                $('.btn-subscribe').html(response.message);
                $('.subscribe-form--overlay').hide();

            }, 'JSON');
        })

        $("#menu-header-menu-1 > li > a").on('click', function(e) {
            let subMenu = $(this).parent('li.menu-item').children('ul.sub-menu');
            if ($(subMenu).length > 0) {
                e.preventDefault();
                if ($(subMenu[0]).is(':visible') === false) {
                    $(subMenu[0]).css('display', 'flex');
                }
            }
        });

        $('.revert-back').click(function(e) {
            e.preventDefault();
            let subMenu = $(e.target).closest('.revert-back').parent('ul.sub-menu');
            if ($(subMenu[0]).is(':visible')) {
                $(subMenu[0]).css('display', 'none')
            }

            return false;
        })
    });
});
