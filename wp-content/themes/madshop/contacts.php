<?php
/**
 * Template Name: Contacts
 */
get_header();

load('header');
$breadcrumbs = array(
    'delimiter' => '',
    'wrap_before' => '<nav class="bread-crumbs">',
    'wrap_after' => '</nav>',
    'before' => '<div>',
    'after' => '</div>',
    'home' => _x('MADshop', 'breadcrumb', 'woocommerce'),
);

?>
<style>
	.about-page {
    background: #fff;
	padding-bottom: 50px;
	}
	.wrap.about-content {
    max-width: 740px;
	font-size: 13px;
    line-height: 21px;
	}
</style>

    <div class="l_content about-page" id="content">
        <div class="wrap about-content">
        <div class="lc_main">
            <div class="lcm_content">
                <div class="bread-crumbs-wrap">
                    <?php
                    woocommerce_breadcrumb($breadcrumbs); ?>
                </div>
                <div class="content_wrap">
                    <!-- <div class="offer-block">
                    <img src="/img/originals-offer.svg">
                    <span class="originals-offer">Подлинный товар</span>
                    </div> -->
                    <div class="section section_text"><div class="uss_section_content"><div class="contacts-new">
                                <p>620000, г. Екатеринбург, ул. Радищева, 25 (Торговая галерея).</p>
                                <p>Ежедневно 11:00 - 21:00.</p>
                                <p><a href="tel:+7 343 288 78-81">+7 343 288 78 81</a></p>
                                <p><iframe src="https://yandex.ru/map-widget/v1/?um=constructor%3Aa0bb81c3508a744fec8094cd1fc8d04ecca2ae4eee70fd1d092307eeefdb37e1&amp;source=constructor" width="100%" height="400" frameborder="0"></iframe></p>
                                <p></p>
                                <p></p>
                                <p><span style="color: #999999;">ИП Мазурков Андрей Дмитриевич</span></p>
                                <p><span style="color: #999999;">ИНН:&nbsp;663003454531</span></p>
                                <p><span style="color: #999999;">ОГРН:&nbsp;318665800046731</span></p>
                            </div></div></div><div class="cleaner"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php get_footer('main'); ?>
