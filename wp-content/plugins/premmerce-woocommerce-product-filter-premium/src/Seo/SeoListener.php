<?php namespace Premmerce\Filter\Seo;

use Premmerce\Filter\FilterPlugin;
use Premmerce\Filter\Filter\Filter;
use Premmerce\Filter\Filter\Container;
use Premmerce\Filter\Filter\Items\Types\TaxonomyFilter;
use Premmerce\Filter\Filter\Items\Types\FilterInterface;
use Premmerce\Filter\Seo\Sitemap\SeoFilterSitemapProvider;

class SeoListener
{

    /**
     * @var SeoModel
     */
    private $seoModel;

    /**
     * @var array
     */
    private $rule;

    /**
     * @var array
     */
    private $settings;

    /**
     * @var array
     */
    private $formatted = [];
    /**
     * @var array
     */
    private $formattedAttributesReplacements = [];

    /**
     * SeoListener constructor.
     */
    public function __construct()
    {
        add_action('woocommerce_product_query', [$this, 'findRule']);

        add_action('premmerce_filter_rule_found', [$this, 'registerActionsForRule']);

        add_action('premmerce_filter_rule_found', [$this, 'removeSeoActionsForRule']);

        add_filter('wp_robots', [$this, 'addMetaDescourageSearch'], 10, 1);
        add_filter('wpseo_robots_array', [$this, 'addMetaDescourageSearch'], 10, 1);

        add_action('robots_txt', [$this, 'discourageSearchRobotsTxt'], -1);

        add_action('premmerce_filter_rule_not_found', [$this, 'ruleNotFound']);

        add_filter('wpseo_sitemaps_providers', [$this, 'addSitemap']);

        $clearSitemapActions = [
            'save_post_product',
            'premmerce_filter_seo_rule_created',
            'premmerce_filter_seo_rule_updated',
            'premmerce_filter_seo_bulk_rules_removed',
            'premmerce_filter_seo_bulk_rules_updated',
            'update_option_premmerce_filter_seo_settings',
        ];

        foreach ($clearSitemapActions as $action) {
            add_action($action, [$this, 'clearSiteMap']);
        }

        $this->settings = get_option('premmerce_filter_seo_settings', []);

        $this->settings = array_merge(
            [
                "use_default_settings" => null,
                "h1" => null,
                "title" => null,
                "meta_description" => null,
                "description" => null
            ],
            $this->settings
        );

        $this->seoModel = new SeoModel();
    }

    /**
     * Add rules sitemap to YOAST SEO
     *
     * @param $array
     *
     * @return array
     */
    public function addSitemap($array)
    {
        $array[] = new SeoFilterSitemapProvider($this->seoModel);

        return $array;
    }

    /**
     * Clear rules sitemap
     */
    public function clearSiteMap()
    {
        if (class_exists('WPSEO_Sitemaps_Cache')) {
            \WPSEO_Sitemaps_Cache::clear(['filter_seo_rule']);
        }
    }

    /**
     * Get Rule data by path
     */
    public function ruleByPath($path)
    {
        $rule = $this->seoModel
                ->where(['path' => trim($path, '/'), 'enabled' => 1])
                ->returnType(SeoModel::TYPE_ROW)
                ->limit(1)
                ->get();
        return $rule;
    }

    /**
     * Find Rule by path
     */
    public function findRule()
    {
        if (is_product_category() && is_filtered()) {
            $path = parse_url($_SERVER['REQUEST_URI'])['path'];

            $rule = $this->ruleByPath($path);

            if (!$rule && !empty($this->settings['use_default_settings'])) {
                $rule = $this->settings;
            }

            if (is_array($rule)) {
                do_action('premmerce_filter_rule_found', ['rule' => $rule]);
            } else {
                $parentRule = [];
                //if it is pagination page - take info from parent page (main page)
                if (is_paged()) {
                    // get the position where '/page.. ' text start.
                    $pos = strpos($path, '/page');
                    // remove string from the specific postion
                    $parentPath = substr($path, 0, $pos);
                    //get rule from parent page
                    $parentRule = $this->ruleByPath($parentPath);
                }
                do_action('premmerce_filter_rule_not_found', ['path' => $path, 'rule' => $parentRule]);
            }
        }
    }


    /**
     * Register actions for found rule
     *
     * @param array $args
     */
    public function registerActionsForRule($args)
    {
        $this->rule = $args['rule'];

        add_filter('pre_get_document_title', [$this, 'documentTitle'], 30);

        add_filter('woocommerce_page_title', [$this, 'pageTitle']);

        add_action('wp_head', [$this, 'addMetaDescription'], 1);

        add_filter('wpseo_metadesc', [$this, 'addMetaDescriptionYoast']);

        remove_action('woocommerce_archive_description', 'woocommerce_taxonomy_archive_description', 10);
        add_action('woocommerce_archive_description', [$this, 'addDescription'], 20);
    }

    /**
     * Replace term description
     */
    public function addDescription()
    {
        $description = $this->parseVariables($this->rule['description']);

        if ($description) {
            printf('<div class="term-description">%s</div>', wc_format_content($description));
        }
    }

    /**
     * Add meta description
     */
    public function addMetaDescription()
    {
        if (!defined('WPSEO_VERSION')) {
            $metaDescription = $this->escape($this->parseVariables($this->rule['meta_description']));

            printf("<meta name='description' content='%s'>", $metaDescription);
        }
    }

    public function addMetaDescriptionYoast($description)
    {
        return $this->escape($this->parseVariables($this->rule['meta_description']));
    }

    /**
     * Add meta noindex, nofollow if checked `discourage_search` and not using Yoast Seo
     *
     * @param array $robots
     */
    public function addMetaDescourageSearch($robots)
    {
        if (is_filtered()) {
            //Discourage search engines from indexing pages created by the filter, except for pages with SEO rules.
            $isDiscourageSearchForAll = $this->isDiscourageSearchForAll();

            //get Descourage Search meta from current Seo Rule
            $metaDescourageSearch = $this->rule ? $this->escape($this->parseVariables($this->rule['discourage_search'])) : null;

            //add noindex/nofollow when checked Discourage search engines in Seo Rule
            //or when it isn't Seo Rule but is checked Discourage search engines from indexing pages created by the filter (on Permalinks Tab)
            if ((int) $metaDescourageSearch === 1 || (empty($this->rule) && $isDiscourageSearchForAll)) {
                //add nofollow and noindex
                $robots['nofollow'] = (!defined('WPSEO_VERSION')) ? true : 'nofollow';
                $robots['noindex']  = (!defined('WPSEO_VERSION')) ? true : 'noindex';

                //remove index and follow
                $robots['index']  = (!defined('WPSEO_VERSION')) ? false : '';
                $robots['follow']  = (!defined('WPSEO_VERSION')) ? false : '';
            }
        }

        return $robots;
    }

    /**
     * Discourage search engines from indexing pages created by the filter,
     * except for pages with SEO rules.
     */
    public function isDiscourageSearchForAll()
    {
        $permalinkSettings = get_option(FilterPlugin::OPTION_PERMALINKS_SETTINGS, []);
        $isDiscourageSearchForAll = (isset($permalinkSettings['discourage_search_all']) && $permalinkSettings['discourage_search_all'] === 'on') ? true : false;

        return $isDiscourageSearchForAll;
    }

    /**
     * Replace document title
     * @return string
     */
    public function documentTitle()
    {
        return $this->escape($this->parseVariables($this->rule['title']));
    }

    /**
     * Replace H1
     * @return string
     */
    public function pageTitle()
    {
        return $this->parseVariables($this->rule['h1']);
    }


    /**
     * Add canonical if rule not found
     */
    public function ruleNotFound($args)
    {
        if (!defined('WPSEO_VERSION')) {
            //in pagination pages added the same meta like in main page
            if (!empty($args['rule'])) {
                $this->registerActionsForRule($args);
            }
            //add rel canonical for this pages
            add_action('wp_head', [$this, 'addCanonical']);
        }
    }

    /**
     * Ad canonical with term url
     */
    public function addCanonical()
    {
        $term = get_queried_object();

        if ($term instanceof \WP_Term) {
            $canonical = get_term_link($term);
            if (is_string($canonical)) {
                printf('<link rel="canonical" href="%s" />', esc_url($canonical, null, null));
            }
        }
    }

    /**
     * Remove canonical if rule found
     */
    public function removeSeoActionsForRule()
    {
        if (defined('WPSEO_VERSION')) {
            remove_action('wpseo_head', [\WPSEO_Frontend::get_instance(), 'canonical'], 20);
            remove_action('wpseo_head', [\WPSEO_Frontend::get_instance(), 'metadesc'], 6);
        }
    }

    public static function findAllDiscourageRules()
    {
        global $wpdb;

        $prefix = $wpdb->get_blog_prefix();
        $query = "SELECT ID, `path` FROM {$prefix}premmerce_filter_seo WHERE discourage_search = 1";

        $discourageSearchRules = $wpdb->get_results($query);

        return $discourageSearchRules;
    }

    public function discourageSearchRobotsTxt($output)
    {
        $findAllDiscourageRules = self::findAllDiscourageRules();

        $array = [];

        foreach ($findAllDiscourageRules as $find) {
            $path = $find->path;
            $array[] = 'Disallow: /' . $path;
        }

        $output .= implode("\r\n", $array);

        return $output;
    }

    /**
     * Parse variables in SEO text
     *
     * @param $text
     *
     * @return string
     */
    public function parseVariables($text)
    {
        if (empty($this->formatted)) {
            $items = Container::getInstance()->getItemsManager()->getItems();
            $attributeItemsForSeo = array_filter($items, [$this, 'canBeUsedForSeoAttributeText']);
            $this->formatted['attributes'] = $this->formatAttributes($attributeItemsForSeo);
            $this->formattedAttributesReplacements = $this->formatIndividualAttributes($attributeItemsForSeo);

            $this->formatted['brands'] = $this->formatBrands($items);
            $this->formatted['prices'] = Container::getInstance()->getPriceQuery()->getPrices();
        }


        global $wp_query;

        $replacements = [
            '{name}' => get_queried_object()->name,
            '{count}' => $wp_query->found_posts,
            '{description}' => get_queried_object()->description,
            '{attributes}' => $this->formatted['attributes'],
            '{brands}' => $this->formatted['brands'],
            '{min_price}' => $this->formatted['prices']['min_selected'],
            '{max_price}' => $this->formatted['prices']['max_selected'],
        ];
        $replacements = array_merge($replacements, $this->formattedAttributesReplacements);
        $result = strtr($text, $replacements);

        return trim($result);
    }

    /**
     * @param FilterInterface[] $filters
     * @return array
     */
    public function formatIndividualAttributes(array $filters)
    {
        $replacements = [];

        foreach ($filters as $filter) {
            $attributeNameVariable = '{attribute_name_' . $filter->getSlug() . '}';
            $replacements[$attributeNameVariable] = $filter->getLabel();

            $activeItems = array_column($filter->getActiveItems(), 'title', 'id');

            $attributeValueVariable = '{attribute_value_' . $filter->getSlug() . '}';
            $replacements[$attributeValueVariable] = implode(', ', array_values($activeItems));

            $terms = array_column($filter->getItems(), 'term_id', 'name');
            foreach ($activeItems as $id => $title) {
                $tid = isset($terms[$title]) ? $terms[$title] : $id;
                $replacements['{attribute_value_' . $filter->getSlug() . '_' . $tid . '}'] = $title;
            }
        }

        return $replacements;
    }

    /**
     * @param FilterInterface[] $items
     *
     * @return string
     */
    protected function formatBrands($items)
    {
        foreach ($items as $item) {
            if ($item->getId() === 'product_brand') {
                $item->init();
                $active = $item->getActiveItems();
                $names = array_column($active, 'title');

                if (count($names)) {
                    return implode(', ', $names);
                }
            }
        }
    }

    /**
     * Format attributes for seo text variable
     *
     * @param $attributes
     *
     * @return string
     */
    protected function formatAttributes($attributes)
    {
        $attributeStrings = [];
        /** @var FilterInterface $attribute */
        foreach ($attributes as $attribute) {
            $attribute->init();
            $items = $attribute->getActiveItems();
            $names = array_column($items, 'title');
            if (count($names)) {
                $attributeStrings[] = $attribute->getLabel() . ' (' . implode(', ', $names) . ')';
            }
        }

        return implode(', ', $attributeStrings);
    }

    /**
     * @param FilterInterface $attribute
     *
     * @return bool
     */
    protected function canBeUsedForSeoAttributeText(FilterInterface $attribute)
    {
        return $attribute->isActive()
            && $attribute->getType() !== 'slider'
            && (taxonomy_is_product_attribute($attribute->getId()) || $attribute->getSlug() === 'product_brand');
    }

    /**
     * Escape text for tag attribute
     *
     * @param $text
     *
     * @return string
     */
    private function escape($text)
    {
        return esc_attr(wp_strip_all_tags(stripslashes($text)));
    }
}
