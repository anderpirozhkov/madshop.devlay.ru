<?php
/**
 * Template Name: Brands
 */


$alphabet = [
    'A' => [],
    'B' => [],
    'C' => [],
    'D' => [],
    'E' => [],
    'F' => [],
    'G' => [],
    'H' => [],
    'I' => [],
    'J' => [],
    'K' => [],
    'L' => [],
    'M' => [],
    'N' => [],
    'O' => [],
    'P' => [],
    'Q' => [],
    'R' => [],
    'S' => [],
    'T' => [],
    'U' => [],
    'V' => [],
    'W' => [],
    'X' => [],
    'Y' => [],
    'Z' => []
];
$brands = get_terms( 'product_brand');
$breadcrumbs = array(
    'delimiter' => '',
    'wrap_before' => '<nav class="bread-crumbs">',
    'wrap_after' => '</nav>',
    'before' => '<div>',
    'after' => '</div>',
    'home' => _x('MADshop', 'breadcrumb', 'woocommerce'),
);
$top = [];
if (empty($brands) === false) {
    foreach ($brands as $brand) {
        $alphabet[strtoupper($brand->name[0])][] = $brand;
        $brandFiled = get_field('is_top', 'product_brand_'.$brand->term_id);

        if($brandFiled) {
            $top[] = $brand;
        }
    }
}

$brands = array_filter($alphabet);
get_header();
load('header');
?>
    <div class="wrap">
        <div class="bread-crumbs-wrap">
            <?php
            woocommerce_breadcrumb($breadcrumbs);
            do_action( 'woocommerce_before_shop_loop' ); ?>
        </div>
    </div>
    <main>
        <div class="brands">
            <div class="brands__brand-list">
                <?php if(empty($top) === false) {?>
                <div class="brands__brand-list__item brands__brand-list__item__top">
                    <div class="brands__brand-list__item__title">Топ</div>
                    <div class="brands__brand-list__item__items">
                        <?php
                            foreach ($top as $brand) {
                                $thumbId = get_woocommerce_term_meta( $brand->term_id, 'thumbnail_id', true );
                        ?>
                            <a href="<?= get_term_link($brand->term_id, 'product_brand') ?>" class="brands__brand-list__item__items__brand">
                                <img src="<?= wp_get_attachment_url($thumbId) ?>" alt="">
                            </a>
                        <?php } ?>
                    </div>
                </div>
                <?php } if(empty($brands) === false) {
                    foreach ($brands as $letter => $items) { ?>
                        <div class="brands__brand-list__item">
                            <div class="brands__brand-list__item__title"><?= strtoupper($letter) ?></div>
                            <div class="brands__brand-list__item__items">
                                <?php foreach ($items as $item) {?>
                                    <a href="<?= get_term_link($item->term_id, 'product_brand') ?>" class="brands__brand-list__item__items__brand">
                                        <?= $item->name?>
                                    </a>
                                <?php } ?>
                            </div>
                        </div>
                <?php } } ?>
            </div>
        </div>
    </main>


<?php
get_footer();
