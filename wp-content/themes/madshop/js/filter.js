jQuery(function ($) {
    $(document).ready(function () {
        $(document).on('click', '.accordion-item .bapf_head', function(){
            let parent = $(this).parents('div.accordion-item');
            if (parent.hasClass('accordion-item--closed')) {
                parent.removeClass('accordion-item--closed').addClass('accordion-item--opened');
            } else {
                parent.removeClass('accordion-item--opened').addClass('accordion-item--closed');
            }
        })
    });
});
