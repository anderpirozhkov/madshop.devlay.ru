<?php /** @var int $validCredentials */ ?>
<form id="yoomoney-form-1" class="yoomoney-form">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-6 padding-bottom">
                <div class="form-group">
                    <label for="yookassa_shop_id">shopId</label>
                    <input type="text" id="yookassa_shop_id" name="yookassa_shop_id"
                           value="<?php echo get_option('yookassa_shop_id'); ?>" class="form-control"
                           placeholder="<?= __('Заполните поле', 'yookassa'); ?>" />
                    <p class="help-block help-block-error"></p>
                </div>
                <div class="form-group">
                    <label for="yookassa_shop_password"><?= __('Секретный ключ', 'yookassa') ?></label>
                    <input type="text" id="yookassa_shop_password" name="yookassa_shop_password"
                           value="<?php echo get_option('yookassa_shop_password'); ?>" class="form-control"
                           placeholder="<?= __('Заполните поле', 'yookassa'); ?>" />
                    <p class="help-block help-block-error"></p>
                </div>
                <?php if ($validCredentials === 1) : ?>
                <div class="form-group">
                    <p class="help-block help-block-error">
                        <?= __('Проверьте shopId и Секретный ключ — где-то есть ошибка. А лучше скопируйте их прямо из <a href="https://yookassa.ru/my" target="_blank">личного кабинета ЮKassa</a>', 'yookassa'); ?>
                    </p>
                </div>
                <?php endif; ?>
            </div>
            <div class="col-md-5 col-md-offset-1 help-side">
                <p class="title"><b><?= __('shopId и секретный ключ', 'yookassa'); ?></b></p>
                <ul>
                    <li><?= __("<b>shopId</b> можно скопировать в <a target='_blank' href='https://yookassa.ru/my/shop-settings'>личном кабинете ЮKassa</a>.", 'yookassa'); ?></li>
                    <li><?= __('<b>Секретный ключ</b> нужно выпустить и сохранить после подключения ЮKassa. Если ключ потерялся, в личном кабинете можно его перевыпустить.', 'yookassa'); ?></li>
                </ul>
                <p><br></p>
            </div>

        </div>

        <div class="row">
            <div class="col-md-8 col-lg-9">
                <div class="row form-footer">
                    <div class="col-md-12">
                        <button class="btn btn-light btn-back" data-tab="section1"><?= __('Назад', 'yookassa'); ?></button>
                        <button class="btn btn-primary btn-forward" data-tab="section2"><?= __('Сохранить и продолжить', 'yookassa'); ?></button>
                    </div>
                </div>
            </div>
        </div>

    </div>

</form>