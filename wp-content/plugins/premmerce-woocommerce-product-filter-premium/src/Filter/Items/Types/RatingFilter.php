<?php namespace Premmerce\Filter\Filter\Items\Types;

use WC_Query;
use WP_Tax_Query;
use WP_Meta_Query;
use Premmerce\Filter\Filter\Query\QueryHelper;
use Premmerce\Filter\Filter\Items\Types\BaseFilter;

class RatingFilter extends BaseFilter
{
    protected $prefix = 'rating_';

    protected $key = '_wc_average_rating';

    /**
     * @var
     */
    protected $config;

    private $items = [];

    /**
     * @var QueryHelper
     */
    private $queryHelper;

    public function __construct($config, QueryHelper $queryHelper)
    {
        $this->config = $config;
        $this->hideEmpty = !empty($config['hide_empty']);

        if (in_array($this->getType(), ['radio', 'select'])) {
            $this->single = true;
        }

        $this->queryHelper = $queryHelper;
    }

    /**
     * Unique item identifier
     *
     * @return string
     */
    public function getId()
    {
        return 'rating';
    }

    /**
     * @return string
     */
    public function getLabel()
    {
        return 'Rating';
    }

    /**
     * @return string
     */
    public function getSlug()
    {
        return 'filter';
    }

    public function getItems()
    {
        return $this->items;
    }

    /**
     * @return array
     */
    public function getActiveItems()
    {
        $items = $this->getItems();

        $active = [];
        foreach ($items as $item) {
            if ($item->checked) {
                $active[] = ['title' => $item->name, 'link' => $item->link,];
            }
        }

        return $active;
    }

    /**
     * @return array
     */
    public function getActiveProducts()
    {
        return [];
    }

    /**
     * @return boolean
     */
    public function isVisible()
    {
        return count($this->getItems());
    }

    /**
     * checkbox|radio|select|label|color
     * @return string
     */
    public function getType()
    {
        return isset($this->config['type']) ? $this->config['type'] : '';
    }

    /**
     * @return void
     */
    public function init()
    {
        $active = $this->getSelectedValues();

        $items = [];

        for ($rating = 5; $rating >= 1; $rating--) {
            $count = $this->getFilteredProductCount($rating);

            if (empty($count)) {
                continue;
            }

            if (!isset($items[$rating]) || !is_object($items[$rating])) {
                $items[$rating] = new \stdClass();
            }

            $items[$rating]->count   = $count;
            $items[$rating]->term_id = $rating;
            $items[$rating]->name    = $rating;
            $items[$rating]->slug    = $rating;
            $items[$rating]->checked = in_array($rating, $active);

            $items[$rating]->link    = $this->getValueLink($rating);
        }

        $this->items = $items;
    }

    /**
     * Count products after other filters have occurred by adjusting the main query.
     *
     * @param  int $rating Rating.
     * @return int
     */
    protected function getFilteredProductCount($rating)
    {
        //get rating count from transient
        $getCountTransient = get_transient("premmerce_filtered_product_count_{$rating}");

        $taxQuery        = WC_Query::get_main_tax_query();
        $metaQuery       = WC_Query::get_main_meta_query();

        $postInIds = QueryHelper::getPostInProducts();

        $transientQuery = $this->generateQueryTransientID($taxQuery, $metaQuery, $postInIds, 'rating_filter');

        $filteredProductCount = $getCountTransient[$transientQuery] ?? null;

        if (!is_numeric($filteredProductCount)) {
            global $wpdb;

            // Set new rating filter.
            $productVisibilityTerms = wc_get_product_visibility_term_ids();
            $taxQuery[]             = array(
                'taxonomy'      => 'product_visibility',
                'field'         => 'term_taxonomy_id',
                'terms'         => $productVisibilityTerms[ 'rated-' . $rating ],
                'operator'      => 'IN',
                'rating_filter' => true,
            );

            $metaQuery     = new WP_Meta_Query($metaQuery);
            $taxQuery      = new WP_Tax_Query($taxQuery);

            $metaQuerySql = $metaQuery->get_sql('post', $wpdb->posts, 'ID');
            $taxQuerySql  = $taxQuery->get_sql($wpdb->posts, 'ID');

            $sql  = "SELECT COUNT( DISTINCT {$wpdb->posts}.ID ) FROM {$wpdb->posts} ";
            $sql .= $taxQuerySql['join'] . $metaQuerySql['join'];
            $sql .= $this->queryHelper->getPostWhereQuery();
            $sql .= $taxQuerySql['where'] . $metaQuerySql['where'];

            $search = WC_Query::get_main_search_query_sql();
            if ($search) {
                $sql .= ' AND ' . $search;
            }

            $filteredProductCount = absint($wpdb->get_var($sql)); // WPCS: unprepared SQL ok.

            //save old data and new
            $getCountTransient[$transientQuery] = $filteredProductCount;
            //save in transient at 12 hours
            set_transient("premmerce_filtered_product_count_{$rating}", $getCountTransient, DAY_IN_SECONDS);
        }

        return $filteredProductCount;
    }
}
