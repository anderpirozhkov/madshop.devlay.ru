<?php
if (empty($data)) {
    return;
}
?>
<table style="width: 320px;" border="0" cellspacing="0" cellpadding="0">
    <colgroup><col span="5" width="64" /> </colgroup>
    <tbody>
    <?php if(empty($data['header']) === false) {?>
    <tr height="20">
        <?php foreach ($data['header'] as $headerItem) {?>
            <td class="xl65" width="64" height="20"><?= $headerItem['c'] ?></td>
        <?php } ?>
    </tr>
    <?php } if (empty($data['body']) === false) {
        foreach ($data['body'] as $row) { ?>
        <tr height="20">
            <?php foreach ($row as $column) {?>
                <td class="xl65" height="20"><?= $column['c']?></td>
            <?php } ?>
        </tr>
    <?php }
    }?>
    </tbody>
</table>