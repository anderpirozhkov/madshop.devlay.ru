<?php
/**
 * Template Name: Exchange
 */
get_header();

load('header');
$breadcrumbs = array(
    'delimiter' => '',
    'wrap_before' => '<nav class="bread-crumbs">',
    'wrap_after' => '</nav>',
    'before' => '<div>',
    'after' => '</div>',
    'home' => _x('MADshop', 'breadcrumb', 'woocommerce'),
);

?>
<style>
	.about-page {
    background: #fff;
	padding-bottom: 100px;
	}
	.wrap.about-content {
    max-width: 740px;
	font-size: 13px;
    line-height: 21px;
	}
</style>

    <div class="l_content about-page" id="content">
        <div class="wrap about-content">
            <div class="lc_main">
                <div class="lcm_content">
                    <div class="bread-crumbs-wrap">
                        <?php
                        woocommerce_breadcrumb($breadcrumbs); ?>
                    </div>
                    <div class="content_wrap">
                        <!-- <div class="offer-block">
                        <img src="/img/originals-offer.svg">
                        <span class="originals-offer">Подлинный товар</span>
                        </div> -->
                        <div class="section section_text"><div class="uss_section_content"><div class="content-o-nas">
                                    <p><strong>1. Что делать, если пришел товар с&nbsp;браком/дефектом?<br></strong>Мы&nbsp;стараемся оперативно решить возникшую проблему. Пожалуйста, напишите нам письмо на madsportbrands@gmail.com&nbsp;с&nbsp;темой «Дефект/брак товара по&nbsp;заказу №», описав проблему, а&nbsp;также приложите несколько фотографий места дефекта. Укажите ваш телефон, он&nbsp;может понадобиться нашим специалистам для оперативной связи.<br>Нам потребуется некоторое время для изучения претензии, но&nbsp;мы&nbsp;сделаем всё возможное, чтобы решить ваш вопрос как можно быстрее.</p>
                                    <p><strong>2. Когда будет собран и&nbsp;отправлен мой заказ?<br></strong>Срок обработки и&nbsp;отправки заказа, как правило, составляет от&nbsp;одного до&nbsp;двух дней<strong>*</strong>, не&nbsp;считая дня поступления заказа.<br>Так&nbsp;же мы&nbsp;обращаем внимание на&nbsp;ваши пожелания к&nbsp;заказу, поэтому если необходима срочная доставка, просто сообщите об&nbsp;этом в&nbsp;комментариях к&nbsp;заказу, по&nbsp;возможности мы&nbsp;постараемся выполнить просьбу.<br>* — В&nbsp;период распродаж или дополнительных скидок срок обработки может быть незначительно увеличен.</p>
                                    <p><strong>3. Как отследить свой заказ?<br></strong>Все почтовые курьерские службы, которыми мы&nbsp;отправляем заказы, предоставляют возможность отслеживать посылку в&nbsp;пути.<br>После фактической отправки заказа мы&nbsp;отправим вам уведомления в&nbsp;виде&nbsp;письма на&nbsp;электронную почту с&nbsp;указанием трекинг-номера.</p>
                                </div></div></div><div class="cleaner"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php get_footer('main'); ?>