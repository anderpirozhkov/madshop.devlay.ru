<?php
/**
 * The template for displaying product content within loops
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.6.0
 */

defined('ABSPATH') || exit;

global $product;

// Ensure visibility.
if (empty($product) || !$product->is_visible()) {
    return;
}
$variantIds = $product->get_children();
$productId = $product->get_id();
$isSimple = true;

if ($product->is_type('simple') === false) {
    $availableVariants = $product->get_available_variations();
    $availableVariants = getVariationsSort($availableVariants);
    $isSimple = false;
}

$brand = get_the_terms($productId, 'product_brand');
$fields = get_fields($productId);
$cart = WC()->cart->get_cart();
$inCart = false;
$addOpacity = false;
if($product->get_type('variable') === '' && (empty($fields['isSold']) === false ||
    (empty($fields['isSold']) && empty($fields['to_plan_start_sales']) === false) ||
    empty($availableVariants))) {
  $addOpacity = true;
}
?>

<div class="product">
    <div class="product-content">
        <div class="product-img <?= $addOpacity ? "opacity" : "" ?>">
            <?= wp_get_attachment_image($product->get_image_id(), [300, 300], false); ?>
            <div class="img-overlay"></div>
        </div>
        <?php
        if (!empty($brand)) { ?>
            <div class="product-brand <?= $addOpacity ? "opacity" : "" ?>"><?= $brand[0]->name ?></div>
        <?php
        } ?>

        <div class="product-title <?= $addOpacity ? "opacity" : "" ?>">
            <div class="link-product">
                <a href="<?= get_permalink($productId) ?>">
                    <?php
                    if (!empty($fields['title_on_page'])) {
                        if (!empty($brand)) {
                            echo str_ireplace($brand[0]->name, '', $fields['title_on_page']);
                        } else {
                            echo $fields['title_on_page'];
                        }
                    } else {
                        if (!empty($brand)) {
                            echo str_ireplace($brand[0]->name, '', $product->get_title());
                        } else {
                            echo $product->get_title();
                        }
                    } ?>
                </a>
            </div>
            <div class="product-type">
                <?php
                if (!empty($fields['type'])) {
                    echo $fields['type'];
                } ?>
            </div>
        </div>
        <?php if ($isSimple) {?>
            <div class="product-price">
                <?= $product->get_price_html() ?>
            </div>
            <div class="product-size">
                <div class="product-size-list">
                    <?php
                        $addClass = "";
                        if (!empty($cart)) {
                            foreach ($cart as $itemCart) {
                                if ($product->get_id() != $itemCart['product_id']) {
                                    continue;
                                }
                                $addClass .= "add-to-cart-btn--active";
                                $inCart = true;
                            }
                        }
                        ?>
                        <a id="add-to-cart" data-product-id="<?= $productId ?>" data-is-simple="1"
                           class="product-size-item add-to-cart-btn product-size-item--simple <?= $addClass ?>">
                            В корзину
                        </a>
                </div>
                <a class="go-checkout--catalog" href="/checkout" <?= $inCart ? "style=\"display: block;\"" : ""?>>Перейти в корзину</a>
            </div>
        <?php } else {
            if (empty($fields['isSold']) === false) { ?>
                <div class="product-price">
                    <div class="coming-soon">
                        Нет в наличии
                    </div>
                </div>
            <?php } elseif (empty($fields['isSold']) && empty($fields['to_plan_start_sales']) === false) { ?>
                <div class="product-price">
                    <div class="coming-soon">
                        Старт продаж <?= $fields['start_sales'] ?>
                    </div>
                </div>
            <?php } elseif (empty($availableVariants)) { ?>
                <div class="product-price">
                    <div class="coming-soon">
                        Подробности скоро!
                    </div>
                </div>
            <?php
            } else { ?>
                <div class="product-price">
                    <?= $product->get_price_html() ?>
                </div>
                <?php
                if (empty($availableVariants) === false) { ?>
                    <div class="product-size">
                        <div class="product-size-title">
                            Размеры в наличии
                        </div>
                        <div class="product-size-list">
                            <?php
                            foreach ($availableVariants as $variant) {
                                if (empty($variant['term']->name)) {
                                    continue;
                                }
                                $addClass = "";
                                if (!empty($cart)) {
                                    foreach ($cart as $itemCart) {
                                        if ($variant['variation_id'] != $itemCart['variation_id']) {
                                            continue;
                                        }
                                        $addClass .= "add-to-cart-btn--active";
                                        $inCart = true;
                                    }
                                }
                                ?>
                                <a id="add-to-cart" data-product-id="<?= $productId ?>"
                                   data-variant-id="<?= $variant['variation_id'] ?>"
                                   class="product-size-item add-to-cart-btn <?= $addClass ?>">
                                    <?= $variant['term']->name ?>
                                </a>
                            <?php
                            } ?>
                        </div>
                            <a class="go-checkout--catalog" href="/checkout" <?= $inCart ? "style=\"display: block;\"" : ""?>>Перейти в корзину</a>
                    </div>
                <?php
                }
            }
        } ?>
    </div>
</div>
