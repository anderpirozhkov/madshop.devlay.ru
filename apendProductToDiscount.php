<?php
require_once "wp-load.php";

global $wpdb;

const DISCOUNT_CAT_ID = 520;
const TAXONOMY = "product_cat";

$sales =  $wpdb->get_results("select post_id from ms_postmeta where meta_key = \"_sale_price\" and meta_value != \"\"");
foreach ($sales as $sale) {
    $parentId = $wpdb->get_var("select post_parent from ms_posts where ID = ".$sale->post_id);
    wp_set_post_terms($parentId, ['term_id' => DISCOUNT_CAT_ID], TAXONOMY, true);
}