<?php
/**
 * Template Name: Cleaning
 */
get_header();

load('header');
$breadcrumbs = array(
    'delimiter' => '',
    'wrap_before' => '<nav class="bread-crumbs">',
    'wrap_after' => '</nav>',
    'before' => '<div>',
    'after' => '</div>',
    'home' => _x('MADshop', 'breadcrumb', 'woocommerce'),
);

?>
<style>
    #404 {
        margin: 0 auto;
        display: block;
    }
</style>
<main style="text-align: center;">
    <span id="404">404</span>
</main>
