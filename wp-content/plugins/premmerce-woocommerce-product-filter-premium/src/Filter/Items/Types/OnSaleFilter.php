<?php namespace Premmerce\Filter\Filter\Items\Types;

use Premmerce\Filter\Filter\Items\Types\BaseFilter;
use Premmerce\Filter\Filter\Query\QueryHelper;
use WP_Query;
use WC_Query;
use WP_Tax_Query;
use WP_Meta_Query;

class OnSaleFilter extends BaseFilter
{
    protected $prefix = 'meta_';

    protected $key = '_sale_price';

    /**
     * @var
     */
    protected $config;

    private $items = [];

    /**
     * @var QueryHelper
     */
    private $queryHelper;

    public function __construct($config, QueryHelper $queryHelper)
    {
        $this->config = $config;
        $this->hideEmpty = !empty($config['hide_empty']);

        if (in_array($this->getType(), ['radio', 'select'])) {
            $this->single = true;
        }

        add_filter('woocommerce_product_query', [$this, 'extendQuery']);
        $this->queryHelper = $queryHelper;
    }

    public function extendQuery($query)
    {
        $values = $this->getSelectedValues();

        if (! empty($values)) {
            //get all product's ids which are `On sale` (grouped too)
            $query->set('post__in', self::groupedOnSale());
        }

        return $query;
    }

    /**
     * Unique item identifier
     *
     * @return string
     */
    public function getId()
    {
        return '_on_sale';
    }

    /**
     * @return string
     */
    public function getLabel()
    {
        return 'On Sale';
    }

    /**
     * @return string
     */
    public function getSlug()
    {
        return 'on_sale';
    }

    public function getItems()
    {
        return $this->items;
    }

    /**
     * @return array
     */
    public function getActiveItems()
    {
        $items = $this->getItems();

        $active = [];
        foreach ($items as $item) {
            if ($item->checked) {
                $active[] = ['title' => $item->name, 'link' => $item->link,];
            }
        }

        return $active;
    }

    /**
     * @return array
     */
    public function getActiveProducts()
    {
        return [];
    }

    /**
     * @return boolean
     */
    public function isVisible()
    {
        return count($this->getItems());
    }

    /**
     * checkbox|radio|select|label|color
     * @return string
     */
    public function getType()
    {
        return isset($this->config['type']) ? $this->config['type'] : '';
    }

    /**
     * @return void
     */
    public function init()
    {
        $active = $this->getSelectedValues();

        $items = $this->loadItems();

        $zeroCount = false;

        foreach ($items as $item) {
            $item->slug    = $this->getSlug();
            $item->checked = in_array($item->slug, $active);
            $item->link    = $this->getValueLink($item->slug);

            //check if count return 0.
            //it's need for hiding count filter
            if (0 == $item->count) {
                $zeroCount = true;
            }
        }

        if ($zeroCount) {
            $items = [];
        }

        $this->items = $items;
    }

    //get ids Grouped products which have On Sale products inside (onsale children products)
    public static function groupedOnSale()
    {
        //all products with status On Sale
        $allProductsOnSale = wc_get_product_ids_on_sale();

        //get all Groped Products from transient
        $allGroupedProductsIds = get_transient('premmerce_get_grouped_products');

        //if we don't have transient `premmerce_get_grouped_products` - take it from WP_Query and save
        if (false === $allGroupedProductsIds) {
            //get all grouped products
            $args = array(
                'post_type'       => 'product',
                'posts_per_page'  => -1,
                'fields'          => 'ids',
                'tax_query'       => array(
                    array(
                        'taxonomy' => 'product_type',
                        'field'    => 'slug',
                        'terms'    => 'grouped',
                    )
                )
            );
            $allGroupedProductsQuery = new WP_Query($args);
            $allGroupedProductsIds   = $allGroupedProductsQuery->posts;

            //save in transient at 24 hours
            set_transient('premmerce_get_grouped_products', $allGroupedProductsIds, DAY_IN_SECONDS);
        }


        $groupedOnSale = [];

        foreach ($allGroupedProductsIds as $pr) {
            $childrenProducts = get_post_meta($pr, '_children', true);

            //check if childrenProducts are on sale
            if (array_intersect($childrenProducts, $allProductsOnSale)) {
                $groupedOnSale[] = $pr;
            }
        }

        $onSale = array_unique(array_merge($groupedOnSale, $allProductsOnSale));

        return $onSale;
    }

    private function countOnSaleProductsInQuery()
    {
        global $wpdb;

        //get all product's ids which are `On sale`
        $onSaleIds = implode(',', self::groupedOnSale());

        $taxQuery  = WC_Query::get_main_tax_query();
        $metaQuery = WC_Query::get_main_meta_query();

        $taxQuery  = new WP_Tax_Query($taxQuery);
        $metaQuery = new WP_Meta_Query($metaQuery);

        $metaQuerySql = $metaQuery->get_sql('post', $wpdb->posts, 'ID');
        $taxQuerySql  = $taxQuery->get_sql($wpdb->posts, 'ID');

        $sql  = "SELECT COUNT( DISTINCT {$wpdb->posts}.ID ) FROM {$wpdb->posts} ";
        $sql .= $taxQuerySql['join'] . $metaQuerySql['join'];
        $sql .= " WHERE {$wpdb->posts}.ID IN ({$onSaleIds}) AND {$wpdb->posts}.post_type = 'product' AND {$wpdb->posts}.post_status = 'publish' ";
        $sql .= $taxQuerySql['where'] . $metaQuerySql['where'];

        $search = WC_Query::get_main_search_query_sql();
        if ($search) {
            $sql .= ' AND ' . $search;
        }

        $onsaleCount = absint($wpdb->get_var($sql)) ?: 0;

        return $onsaleCount;
    }


    private function loadItems()
    {
        $items[0] = new \stdClass();

        $items[0]->count   = $this->countOnSaleProductsInQuery();
        $items[0]->term_id = 'onsale';
        $items[0]->slug    = 'onsale';
        $items[0]->name    = __('On sale', 'premmerce-filter');

        return $items;
    }

    /**
     * Remove transient for OnSale Filter
    */
    public static function removeTransients()
    {
        delete_transient('premmerce_get_grouped_products');
    }
}
