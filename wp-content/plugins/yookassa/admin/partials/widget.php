<script src="https://yookassa.ru/checkout-ui/v2.js"></script>
<script>
    const checkout = new window.YooMoneyCheckoutWidget({
        confirmation_token: 'confirmation-token',
        return_url: 'https://merchant.site',
    });

    checkout.render('yookassa-widget-ui');
</script>

<div id="yookassa-widget-ui"></div>