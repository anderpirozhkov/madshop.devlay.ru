<?php if ( ! defined( 'WPINC' ) ) die; ?>

<?php if(empty($brands) === false ){ ?>
<div class="prm-brands-list">
    <?php foreach ($brands as $brand){ ?>
        <div class="prm-brands-list__col">
            <a class="prm-brands-list__item" href="<?= get_term_link($brand->slug, 'product_brand'); ?>">
                <div class="prm-brands-list__title"><?= $brand->name; ?></div>
            </a>
        </div>
    <?php } ?>
</div>
<?php } else { ?>
    <p class="woocommerce-info"><?php _e('Брендов нет', 'premmerce-brands'); ?></p>
<?php }?>