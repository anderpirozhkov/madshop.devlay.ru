<?php

/**
 * Template Name: Shop
 */
get_header();
?>
<div class="content">

    <?php
    load('header'); ?>

    <div id="maincontent"></div>
    <?php
    the_content(); ?>
</div>
<?php
get_footer('main'); ?>
