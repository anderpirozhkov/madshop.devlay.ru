<?php
define('DONOTCACHEPAGE', true);
if (!defined('ABSPATH')) {
    exit;
}
$checkout = WC()->checkout();
$cartData = WC()->cart->get_cart_contents();
$checkoutField = $checkout->get_checkout_fields();
$_pf = new WC_Product_Factory();
$cartItems = [];
foreach ($cartData as $cartItem) {
    if (empty($cartItems[$cartItem['product_id']])) {
        $fields = get_fields($cartItem['product_id']);
        $brand = get_the_terms($cartItem['product_id'], 'product_brand');
        $cartItems[$cartItem['product_id']] = [
            'type' => $fields['type'] ?: '',
            'link' => get_permalink($cartItem['product_id']),
            'image' => wp_get_attachment_url($cartItem['data']->get_image_id()),
            'titleOnPage' => $fields['title_on_page'] ?: '',
            'quantity' => 1,
            'price' => explode(' ', strip_tags($cartItem['data']->get_price_html())),
        ];
        $cartItems[$cartItem['product_id']]['sizes'][] = str_replace('-', '.', $cartItem['variation']['attribute_pa_size']);
    } else {
        $cartItems[$cartItem['product_id']]['sizes'][] = str_replace('-', '.', $cartItem['variation']['attribute_pa_size']);
        $cartItems[$cartItem['product_id']]['quantity']++;
    }
}
?>
<div class="main-content">
    <div class="basket">
        <div class="wrap">
            <div class="bread-crumbs-wrap">
                <nav class="bread-crumbs">
                    <div><a href="/">MADshop</a></div>
                    <div><span>Корзина</span></div>
                </nav>
            </div>
            <div class="basket__content">
                <div class="basket__first-col">
                    <?php
                    foreach ($cartItems as $productId => $cartItem) {
                        $product = $_pf->get_product($productId);
                        $isSimple = $product->is_type('simple');
                        sort($cartItem['sizes']);
                        ?>
                        <div class="basket__item">
                            <a href="<?= $cartItem['link'] ?>" class="basket-item">
                                <img width="150px" height="150px" class="item-preview"
                                     src="<?= $cartItem['image'] ?>" alt="" class="basket-img">
								<div class="img-overlay-slider-2"></div>
							</a>
                            <div class="basket__about">
                                <div class="basket__about_category"><?= $cartItem['type'] ?></div>
                                <div class="basket__about_name">
                                    <a href="<?= $cartItem['link'] ?>">
                                        <?php
                                        if (!empty($cartItem['titleOnPage'])) {
                                            echo $brand[0]->name.' '.$cartItem['titleOnPage'];
                                        } else {
                                            echo $product->get_title();
                                        } ?>
                                    </a>
                                </div>
                                <div class="basket__about_feature">
                                    <?php if($isSimple === false) {?>
                                    <div class="basket__about_size">
                                        <div class="basket__about_size-header">
                                            Размер
                                        </div>
                                        <div class="basket__about_size-number">
                                            <?= implode(", ", $cartItem['sizes']) ?>
                                        </div>
                                    </div>
                                    <?php } ?>
                                    <div class="basket__about_amount">
                                        <div class="basket__about_amount-header">
                                            Количество
                                        </div>
                                        <div class="basket__about_amount-change">
                                            <span class="basket__about_amount-number"><?= $cartItem['quantity'] ?></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="basket__cost">
                                <?php
                                if (count($cartItem['price']) > 1) {
                                    ?>
                                    <div class="basket__cost_stock">
                                        <?= $cartItem['price'][0] ?>
                                    </div>
                                    <div class="basket__cost_number">
                                        <?= $cartItem['price'][1] ?>
                                    </div>
                                    <?php
                                } else { ?>
                                    <div class="basket__cost_number">
                                        <?= $cartItem['price'][0] ?>
                                    </div>
                                    <?php
                                } ?>
                                <a class="basket__cost_delete reload" data-product-id="<?= $productId ?>">
                                    удалить
                                </a>
                            </div>
                            <button class="basket__item_media-btn">
                                <img src="<?= get_template_directory_uri() ?>/img/cross.svg" alt="">
                            </button>
                        </div>
                        <div class="basket-dr"></div>
                        <?php
                    } ?>
                    <div class="basket__price">
                        <?php
                            $coupons = WC()->cart->get_applied_coupons();
                            $coupon = $coupons[0] ?: "";

                            $coupon_amount = 0;
                            $coupon_amount_label = '₽';
                            foreach (WC()->cart->get_coupons() as $code => $coupon) {
                                if ($coupon->get_discount_type() == 'percent') {
                                    $coupon_amount = $coupon->get_amount();
                                    $coupon_amount_label = '%';
                                }

                                $code = $coupon->code;
                                if ($coupon->is_type('fixed_cart')) {
                                    $coupon_amount = $coupon->get_amount();
                                    $coupon_amount_label = get_woocommerce_currency_symbol(
                                        get_woocommerce_currency()
                                    );
                                }
                            }

                        ?>
                        <div class="coupon_status">
                            <span class="applied-coupon applied-coupon--success" <?php  if($coupon) {?> style="display: block" <?php } ?>>Купон применен</span>
                            <span class="applied-coupon remove-coupon--success">Купон удален</span>
                            <span class="applied-coupon applied-coupon--failed">Купон не применен</span>
                        </div>
                        <div class="basket__price_row">
                            <div class="basket__price_row2 basket__price_promocode">
                                <?php if (empty($coupons)) { ?>
                                    <input type="text" class="basket__price_promo" placeholder="Промокод">
                                    <button class="basket__price_btn apply_coupon">ОК</button>
                                <?php } else { ?>
                                    <input type="text" class="basket__price_promo" placeholder="Промокод" value="<?= $coupon_amount ? $coupon_amount.' '.$coupon_amount_label : "" ?>" disabled>
                                    <button class="basket__price_btn remove_coupon">
                                        <img src="<?= get_template_directory_uri() ?>/img/close.svg" alt="">
                                    </button>
                                <?php } ?>
                            </div>
                            <div class="basket__price_col">

                                <div class="basket__price_sum">
                                    <span>Сумма</span>
                                    <span><?php
                                        wc_cart_totals_subtotal_html() ?></span>
                                </div>
                                <div class="basket__price_discount">
                                    <span>Доставка</span>
                                    <span>от 150 ₽</span>
                                </div>
                            </div>

                        </div>
                        <div class="basket__price_total">
                            <div>Итого</div>
                            <div><?= WC()->cart->get_cart_total() ?></div>

                        </div>
                    </div>
                </div>
                <div class="basket__second-col">
                    <div class="purchase">
                        <div class="purchase__decor">
                            <img src="<?= get_template_directory_uri() ?>/img/decor.png" alt="">
                        </div>
                        <div class="purchase__body--overlay"> </div>
                        <div class="purchase__body">
                            <div class="purchase__header">
                                Оформление заказа
                            </div>
                            <form name="checkout" method="post" class="checkout woocommerce-checkout"
                                  action="<?= esc_url(wc_get_checkout_url()); ?>" enctype="multipart/form-data">

                                <?php
                                if ($checkout->get_checkout_fields()) { ?>

                                    <input type="text" class="input-text col-input" name="billing_first_name"
                                           id="billing_first_name" placeholder="ФИО"
                                           value="<?= $checkout->get_value('billing_first_name') ?>"
                                           autocomplete="given-name" required>
                                    <div class="basket__row">
                                        <input type="tel" class="input-text row-input tel" name="billing_phone"
                                               id="billing_phone" placeholder="Телефон"
                                               value="<?= $checkout->get_value('billing_phone') ?>" autocomplete="tel" required>
                                        <input type="email" class="input-text row-input ml" name="billing_email"
                                               id="billing_email" placeholder="Email"
                                               value="<?= $checkout->get_value('billing_email') ?>" autocomplete="tel" required>
                                    </div>

                                    <div class="purchase__radio">
                                        <?php
                                        if (WC()->cart->needs_shipping() && WC()->cart->show_shipping()) { ?>
                                            <?php wc_cart_totals_shipping_html(); ?>
                                        <?php } ?>
                                    </div>
                                    <div class="local_pickup_message">
                                        <span>г. Екатеринбург, ул. Радищева, д. 25 <br/> Ежедневно 11:00 — 21:00</span>
                                        <span class="border">+7 (343) 288-78-81</span>
                                    </div>
                                    <input type="text" class="input-text col-input" name="billing_city"
                                           id="billing_city" placeholder="Город"
                                           value="<?= $checkout->get_value('billing_city') ?>"
                                           autocomplete="address-level2" required>
                                    <input type="text" class="input-text col-input" name="billing_address_1"
                                           id="billing_address_1" placeholder="Адрес"
                                           value="<?= $checkout->get_value('billing_address_1') ?>"
                                           autocomplete="address-line1" required>
                                <?php } ?>
                                <?php do_action('woocommerce_checkout_order_review'); ?>
                                <?php do_action('woocommerce_checkout_after_order_review'); ?>
                            </form>

                            <?php
                            do_action('woocommerce_after_checkout_form', $checkout); ?>
                            <form class="checkout_coupon woocommerce-form-coupon" method="post" style="display:none">
                                <input type="text" name="coupon_code" class="input-text" id="coupon_code" value=""/>
                                <button type="submit" class="button" name="apply_coupon"></button>
                                <div class="clear"></div>
                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php
    get_footer('main');
    ?>
