<?php
/**
 * Email Order Items
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/email-order-items.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates\Emails
 * @version 3.7.0
 */

defined('ABSPATH') || exit;

foreach ($items as $item_id => $item) {
    $quantity = $item->get_quantity();
    $product = $item->get_product();
    $sku = '';
    $purchase_note = '';
    $image = '';
    $isSimple = $product->is_type('simple');
    if (!apply_filters('woocommerce_order_item_visible', true, $item)) {
        continue;
    }
    if($isSimple === false) {
        $productId = $product->get_children() ? $product->get_id() : $product->get_parent_id();
    } else {
        $productId = $product->get_id();
    }

    $sku = $product->get_sku();
    $purchase_note = $product->get_purchase_note();
    $image = wp_get_attachment_image_url($product->get_image_id(), [150, 150]);
    $fields = get_fields($productId);
    $brand = wp_get_post_terms($productId, 'product_brand', array('orderby' => 'name', 'fields' => 'names'));
    $brand = count($brand) > 0 ? $brand[0] : "";
    ?>
    <div class="sct id-41052 sk pr-16 pl-16 "
         style="background:#ffffff;background-color:#ffffff;margin:0px auto;border-radius:0px 0px 0px 0px;max-width:440px;">
        <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation"
               style="background:#ffffff;background-color:#ffffff;width:100%;border-radius:0px 0px 0px 0px;">
            <tbody>
            <tr>
                <td style="border:none;direction:ltr;font-size:0px;padding:10px 15px 10px 15px;text-align:left;border-bottom:0.5px solid #eee;">
                    <!--[if mso | IE]>
                    <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td class="middle-outlook cl-outlook id-41053-outlook -outlook"
                                style="vertical-align:middle;width:60px;"><![endif]-->
                    <div class="mj-column-px-60 mj-outlook-group-fix middle cl id-41053 "
                         style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:middle;width:65px;margin-right:7px;">
                        <table border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%">
                            <tbody>
                            <tr>
                                <td style="background-color:transparent;border:none;border-radius:0px 0px 0px 0px;vertical-align:middle;padding:0px 0px 0px 0px;">
                                    <table border="0" cellpadding="0" cellspacing="0" role="presentation" style=""
                                           width="100%">
                                        <tbody>
                                        <tr>
                                            <td align="left" class="img id-41058 last"
                                                style="font-size:0px;padding:0px;padding-bottom:0px;word-break:break-word;">
                                                <table border="0" cellpadding="0" cellspacing="0" role="presentation"
                                                       style="border-collapse:collapse;border-spacing:0px;">
                                                    <tbody>
                                                    <tr>
                                                        <td style="width:60px;">
                                                            <img alt="" height="auto" src="<?= $image ?>"
                                                                 style="border:0;border-radius:0px 0px 0px 0px;display:block;outline:none;text-decoration:none;height:auto;width:100%;font-size:13px;"
                                                                 width="60"/>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <!--[if mso | IE]></td><td class="middle-outlook cl-outlook id-41055-outlook -outlook" style="vertical-align:top;width:410px;"><![endif]-->
                    <div class="mj-column-px-410 mj-outlook-group-fix middle cl id-41055 "
                         style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:50%;">
                        <table border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%">
                            <tbody>
                            <tr>
                                <td style="background-color:transparent;border:none;border-radius:0px 0px 0px 0px;vertical-align:top;padding:0px 0px 0px 0px;">
                                    <table border="0" cellpadding="0" cellspacing="0" role="presentation" style=""
                                           width="100%">
                                        <tbody>
                                        <tr>
                                            <td align="left" class="txt id-41056 hm-1 middle"
                                                style="font-size:0px;padding:0px 55px 0px 0px;padding-bottom:0px;word-break:break-word;">
                                                <div style="font-family:Helvetica nue, sans-serif;font-size:10px;line-height:12px;text-align:left;color:#000000;">
                                                    <span style="mso-line-height-rule:exactly;font-size:10px;font-family:Helvetica nue, sans-serif;text-transform:none;font-style:none;font-weight:400;color:#999999;line-height:12px;letter-spacing:0px;text-decoration:initial;"><?= $fields['type'] ?></span>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" class="txt id-41057 middle"
                                                style="font-size:0px;padding:0px 55px 0px 0px;padding-bottom:0px;word-break:break-word;">
                                                <div style="font-family:Helvetica nue, sans-serif;font-size:12px;line-height:18px;text-align:left;color:#000000;">
                                                    <span style="mso-line-height-rule:exactly;font-size:12px;font-family:Helvetica nue, sans-serif;text-transform:none;font-style:none;font-weight:400;color:#000000;line-height:18px;letter-spacing:0px;text-decoration:initial;"><?= !empty($fields['title_on_page']) ? $brand.' '.$fields['title_on_page'] : $product->get_title() ?></span></div>
                                            </td>
                                        </tr>
                                        <?php if($isSimple === false) {?>
                                        <tr>
                                            <td align="left" class="txt id-41081 last"
                                                style="font-size:0px;padding:0px 55px 0px 0px;padding-bottom:0px;word-break:break-word;">
                                                <div style="font-family:Helvetica nue, sans-serif;font-size:10px;line-height:25px;text-align:left;color:#000000;">
                                                    <span style="mso-line-height-rule:exactly;font-size:10px;font-family:Helvetica nue, sans-serif;text-transform:none;font-style:none;font-weight:400;color:#000000;line-height:25px;letter-spacing:0px;text-decoration:initial;">Размер  <?= str_ireplace('-', '.', $item->get_meta('pa_size')) ?><?= $quantity > 1 ? "x{$quantity}" : "" ?></span></div>
                                            </td>
                                        </tr>
                                        <?php } ?>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <!--[if mso | IE]></td><td class="last-outlook cl-outlook id-41059-outlook -outlook" style="vertical-align:top;width:55px;"><![endif]-->
                    <div class="mj-column-px-55 mj-outlook-group-fix last cl id-41059 "
                         style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;max-width:60px;float:right;">
                        <table border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%">
                            <tbody>
                            <tr>
                                <td style="background-color:transparent;border:none;border-radius:0px 0px 0px 0px;vertical-align:top;padding:0px 0px 0px 0px;">
                                    <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="" width="100%">
                                        <tbody>
                                        <tr>

                                            <td align="right" class="txt id-41060 middle"
                                                style="font-size:0px;padding:0px 0px 0px 0px;padding-bottom:0px;word-break:break-word;">
                                                <div style="font-family:Helvetica nue, sans-serif;font-size:10px;line-height:12px;text-align:right;color:#000000;">
                                                    <span style="mso-line-height-rule:exactly;font-size:10px;font-family:Helvetica nue, sans-serif;text-transform:none;font-style:none;font-weight:400;color:<?= empty($product->get_sale_price()) ? "#ffffff;" : "#999999;" ?>line-height:12px;letter-spacing:0px;text-decoration:line-through;">
                                                        <?= !empty($product->get_sale_price()) ? number_format($product->get_regular_price(), 0, ',', ' ' )." ₽" : "&nbsp;" ?></span></div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="txt id-41061 last"
                                                style="font-size:0px;padding:0px 0px 0px 0px;padding-bottom:0px;word-break:break-word;">
                                                <div style="font-family:Helvetica nue, sans-serif;font-size:12px;line-height:18px;text-align:right;color:#000000;">
                                                    <span style="mso-line-height-rule:exactly;font-size:12px;font-family:Helvetica nue, sans-serif;text-transform:none;font-style:none;font-weight:400;color:#000000;line-height:18px;letter-spacing:0px;text-decoration:initial;"><?= !empty($product->get_sale_price()) ? number_format($product->get_sale_price(),0,',',' ')." ₽" : number_format($product->get_price(),0,',',' ')." ₽" ?></span></div>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <!--[if mso | IE]></td></tr></table><![endif]-->
                </td>
            </tr>
            </tbody>
        </table>
    </div>
<?php
} ?>

