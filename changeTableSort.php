<?php
require_once __DIR__."/wp-load.php";
include_once(ABSPATH.'wp-admin/includes/admin.php');
require_once(ABSPATH.'wp-admin/includes/user.php');

global $wpdb;


$lookups = $wpdb->get_results("SELECT * FROM ms_wc_product_meta_lookup WHERE min_price = 0 OR min_price is null");
foreach ($lookups as $lookup) {

    $product = wc_get_product($lookup->product_id);
    if (empty($product)) {
        $wpdb->delete('ms_wc_product_meta_lookup', ['product_id' => $lookup->product_id]);
        continue;
    }
    if ($product->get_parent_id()) {
        $product = wc_get_product($product->get_parent_id());
    }
    $childrenIds = $product->get_children();
    if (empty($childrenIds)) {
        continue;
    }
    $children = false;
    $i = 0;
    while($children === false) {
        $children = wc_get_product($childrenIds[$i]);
        $i++;
        echo "Children: {$childrenIds[$i]} \n";
    }

    if(is_bool($children)) {
        var_dump([$childrenIds, $product->get_id()]); die();
    }
    $price = $children->get_price();
    $wpdb->update('ms_wc_product_meta_lookup', ['max_price' => $price, 'min_price' => $price], ['product_id' => $lookup->product_id]);
    echo $lookup->product_id."\n";
}

