<?php
/**
 * Single Product title
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/title.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see        https://docs.woocommerce.com/document/template-structure/
 * @package    WooCommerce\Templates
 * @version    1.6.4
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}
global $product;

$fields = get_fields($product->get_id());
$brand = wp_get_post_terms( $product->get_id(), 'product_brand', array('orderby'=>'name', 'fields' => 'names'));
$brand = count($brand) > 0 ? $brand[0] : "";
?>
<div class="cart-item-title-wrap">

    <h1>
        <?php
        if (!empty($fields['type'])) { ?>
            <span class="cart-item-category"><?= $fields['type'] ?></span>
        <?php
        } ?>
        <span class="cart-item-title"><?= !empty($fields['title_on_page']) ? $brand.' '.$fields['title_on_page'] : the_title() ?></span>
    </h1>