jQuery(function ($) {
    $(document).ready(function () {

        $(document).on('click', 'a.add-to-cart-btn', function (e) {
            e.preventDefault();
            let data = {
                product_id: $(this).data('product-id'),
                quantity: 1,
                variation_id: $(this).data('variant-id'),
                isSimple: $(this).data('is-simple')
            };

            if(data.variation_id === undefined && data.isSimple === undefined) {
                if($(".add-product-notify").length > 0){
                    $(".add-product-notify").show();
                }

                return false;
            }

            if ($(e.target).hasClass('add-to-cart-btn--active')) {
                $(e.target).removeClass('add-to-cart-btn--active');

                $.post(ajax_url, {action: 'remove_item_cart', data: data}, after_add_to_cart, 'JSON');

                if ($(this).parent().children('.add-to-cart-btn--active').length == 0) {
                    $(this).parents('.product-size').children('a.go-checkout--catalog').css("display", "none");
                }

                return false;
            }

            $.post(ajax_url, {action: 'add_to_cart', data: data}, after_add_to_cart, 'JSON');

            $(this).addClass('add-to-cart-btn--active');
            if ($('.add-to-cart-btn').is(":visible") && $(this).hasClass('product-size-item') === false) {
                $(this).hide();
                $('.go-checkout').show();
            }
            $(".size-select-item").each(function(index){
                if($(this).hasClass('_active')) {
                    $(this).removeClass('_active');
                }
            })

            if($(this).parents('.product-size-list').length > 0) {
                $(this).parents('.product-size').children('a.go-checkout--catalog').css("display", "block");
            }
        });
        $(document).on('click', 'a.add-to-cart-checkout-btn', function(e) {
            e.preventDefault();
            let data = {
                product_id: $(this).data('product-id'),
                quantity: 1,
                variation_id: $(this).data('variant-id'),
                isSimple: $(this).data('is-simple')
            };
            console.log(data);
            $.post(ajax_url, {action: 'add_to_cart', data: data}, function(response) {
                if(response.error === false) {
                    window.location = '/checkout'
                } else {
                    console.log(response);
                }
            }, 'JSON');

        })
        function after_add_to_cart(response) {
            if (response.error === false) {
                if (response.cartCountItem == 0) {
                    $('a#icon-basket span.basket-count').remove();
                    $('div#basket-hover').remove();
                } else {
                    if($('a#icon-basket').has('span').length) {
                        $('a#icon-basket span.basket-count').text(response.cartCountItem)
                    } else {
                        $('a#icon-basket').html('<span class="basket-count">'+response.cartCountItem+'</span>');
                    }

                    if($('div.header-icons').has('div#basket-hover').length) {
                        $('div#basket-hover').remove();
                    }
                    $('div.header-icons').append(response.cartItems);
                }
            }
        }

        $(document).on('click', '.icon-basket', function () {
            let parent = $(this).parents('div.accordion-item');
            if (parent.hasClass('accordion-item--closed')) {
                parent.removeClass('accordion-item--closed').addClass('accordion-item--opened');
            } else {
                parent.removeClass('accordion-item--opened').addClass('accordion-item--closed');
            }
        })
        $(document).on('click', '.m-filter', function () {
            let filter = $('.cart-col-l');
            $(this).toggleClass('m-filter--active');
            if (filter.is(':hidden')) {
                filter.show();
            } else {
                filter.hide();
            }
        })
        $(".size-select-item").on('click', function (e){
            e.preventDefault();
            let variantId = $(this).data('variant-id');

            if($('.add-product-notify').length > 0 && $('.add-product-notify').is(':visible')){
                $('.add-product-notify').hide();
            }
            $('.add-to-cart-btn').attr('data-variant-id', variantId);
            $('.add-to-cart-checkout-btn').attr('data-variant-id', variantId);
            $('.add-to-cart-checkout-btn').attr('is-simple', 'false');
            if ($('.add-to-cart-btn').is(":hidden")) {
                $('.go-checkout').hide();
                $('.add-to-cart-btn').show();
            }
        })

        $('input[name="billing_postcode"]').on('blur', function(){
            $('body').trigger('update_checkout');
            location.reload();
        });
        function modifyBody(input) {
            let city = $('input[name="billing_city"]'),
                address = $('input[name="billing_address_1"]'),
                message = $('div.local_pickup_message');
            if(input.val().indexOf('local_pickup') !== -1) {
                if(city.is(":visible")){
                    city.hide();
                }
                if(address.is(":visible")){
                    address.hide();
                }
                if(message.is(':hidden')){
                    message.show();
                }
            } else {
                if(message.is(':visible')){
                    message.hide();
                }
                if(city.is(":hidden")){
                    city.show();
                }
                if(address.is(":hidden")){
                    address.show();
                }
            }
        }
        $('input.shipping_method').each(function(i){
            if($(this).is(':checked')) {
                modifyBody($(this))
            }
        });
        $('input.shipping_method').on('change', function() {
            modifyBody($(this))
        })

        $(document).on('click', '.basket__cost_delete', function(e){
                e.preventDefault();
                let product_id = $(this).data('product-id');
                $.ajax({
                    type: "POST",
                    async: false,
                    url: ajax_url,
                    data:  {action: 'remove_item_cart', data: {product_id: product_id}},
                    success: after_add_to_cart,
                });
                if($('.add-to-cart-btn').length > 0) {
                    if ($('a.add-to-cart-btn[data-variant-id=' + variant_id + ']').hasClass('add-to-cart-btn--active')) {
                        $('a.add-to-cart-btn[data-variant-id=' + variant_id + ']').removeClass('add-to-cart-btn--active');
                    }
                }
                if ($('div.size-select').length > 0) {
                    if ($('div.size-select-item[data-variant-id=' + variant_id + ']').hasClass('_active')) {
                        $('div.size-select-item[data-variant-id=' + variant_id + ']').removeClass('_active');
                    }

                    let addButtons = "<a href=\"\" class=\"btn btn-primary animated-btn add-to-cart-btn\" data-product-id=\"" + product_id + "\">Добавить в корзину</a>\n" +
                        "        <a href=\"\" class=\"btn btn-secondary add-to-cart-checkout-btn\">Купить</a>";
                    $('.cart-item-btns').html(addButtons);
                }
            if ($(this).hasClass('reload')) {
                location.reload();
            }
        });
    });

});