<?php
ini_set('memory_limit', '-1');
require_once __DIR__."/wp-load.php";
include_once(ABSPATH.'wp-admin/includes/admin.php');
require_once(ABSPATH.'wp-admin/includes/user.php');
require_once "parser/AmountParser.php";
//require_once "parser/ProductDataParser.php";


//use parser\ProductDataParser;

$parser = new AmountParser();
//$parser = new ProductDataParser();
try {
    $parser->parse();
}catch (Exception $e) {
    var_dump($e->getMessage());
    die();
}