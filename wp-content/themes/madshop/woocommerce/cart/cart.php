<?php
/**
 * Cart Page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/cart/cart.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.8.0
 */

defined( 'ABSPATH' ) || exit;

do_action( 'woocommerce_before_cart' );
$checkout = WC()->checkout();
$cartData = WC()->cart->get_cart_contents();
$checkoutField = $checkout->get_checkout_fields();
?>

<div class="basket">
    <div class="wrap">
        <div class="bread-crumbs-wrap">
            <nav class="bread-crumbs">
                <div><a href="/">MADshop</a></div>
                <div><span>Корзина</span></div>
            </nav>


        </div>
        <div class="basket__content" style="display: block;">
            <div class="basket__first-col">
                <?php
                foreach ($cartData as $cartItem) {
                    $fields = get_fields($cartItem['product_id']);
                    $brand = get_the_terms($cartItem['product_id'], 'product_brand');
                    // var_dump($fields);die();
                    ?>
                    <div class="basket__item" style="margin: 0 auto;">
                        <a href="<?= get_permalink($cartItem['product_id']) ?>" class="basket-item">
                            <img width="150px" height="150px" class="item-preview"
                                 src="<?= wp_get_attachment_url($cartItem['data']->get_image_id()) ?>" alt=""
                                 class="basket-img">
                        </a>
                        <div class="basket__about">
                            <div class="basket__about_category"><?php
                                if (!empty($fields['type'])) {
                                    echo $fields['type'];
                                } ?></div>
                            <div class="basket__about_name">
                                <a href="<?= get_permalink($cartItem['product_id']) ?>">
                                    <?php
                                    if (!empty($fields['title_on_page'])) {
                                        echo $fields['title_on_page'];
                                    } else {
                                        echo $product->get_title();
                                    } ?>
                                </a>
                            </div>
                            <div class="basket__about_feature">
                                <div class="basket__about_size">
                                    <div class="basket__about_size-header">
                                        Размер
                                    </div>
                                    <div class="basket__about_size-number">
                                        <?= str_replace('-', '.', $cartItem['variation']['attribute_pa_size']) ?>
                                    </div>
                                </div>
                                <div class="basket__about_amount">
                                    <div class="basket__about_amount-header">
                                        Количество
                                    </div>
                                    <div class="basket__about_amount-change">
                                            <span class="decrement" data-quantity="<?= $cartItem['quantity'] - 1 ?>"
                                                  data-key="<?= $cartItem['key'] ?>">-</span>
                                        <span class="basket__about_amount-number"
                                              data-key="<?= $cartItem['key'] ?>"><?= $cartItem['quantity'] ?></span>
                                        <span class="increment" data-quantity="<?= $cartItem['quantity'] + 1 ?>"
                                              data-key="<?= $cartItem['key'] ?>">+</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="basket__cost">
                            <?php
                            $price = explode(' ', strip_tags($cartItem['data']->get_price_html()));
                            if (count($price) > 1) {
                                ?>
                                <div class="basket__cost_stock">
                                    <?= $price[0] ?>
                                </div>
                                <div class="basket__cost_number">
                                    <?= $price[1] ?>
                                </div>
                                <?php
                            } else { ?>
                                <div class="basket__cost_number">
                                    <?= $price[0] ?>
                                </div>
                                <?php
                            } ?>
                            <a href="<?= wc_get_cart_remove_url($cartItem['key']) ?>" class="basket__cost_delete no-ajax">
                                удалить
                            </a>
                        </div>
                        <button class="basket__item_media-btn">
                            <img src="<?= get_template_directory_uri() ?>/img/cross.svg" alt="">
                        </button>
                    </div>
                    <div class="basket-dr"></div>
                    <?php
                } ?>
                <div class="basket__price" style="margin: 0 auto;">
                    <div class="basket__price_total" style="border-top: none; padding: 0; margin: 0;">
                        <div>Итого</div>
                        <div><?= WC()->cart->get_cart_total() ?></div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php do_action( 'woocommerce_after_cart' ); ?>
