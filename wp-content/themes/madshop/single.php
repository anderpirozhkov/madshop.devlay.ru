<?php
/**
 * The template for displaying all single posts and attachments 
 */
  
get_header(); ?>
    <div class="rootWrap">
    <div class="content">
        <?php load('header') ?>
        <div id="maincontent"></div>
        <div class="wrap">
            <div class="bread-crumbs-wrap">
                <nav class="bread-crumbs">
                    <div><a href="">MADshop</a></div>
                    <div><a href="">Мужские кроссовки</a></div>
                    <div><span>Reebok</span></div>
                </nav>

                <div class="cart-sort">
                    <label>
                        <select class="js-choice">
                            <option value="">Дешевле выше</option>
                            <option value="">Дороже выше</option>
                            <option value="">Новинки выше</option>
                        </select>
                    </label>
                </div>
            </div>
        </div>
        <div class="wrap">
            <div class="mobile-filter-wrap">
                <div class="m-filter">
                    <div class="m-filter-btn js-mobile-filter-open">
                        <span>1</span>Фильтр
                    </div>
                    <div class="m-filter-wrap">
                        <div class="m-filter-top">
                            <div>
                                <div class="m-filter-btn js-mobile-filter-close">
                                    <span>1</span>Фильтр
                                </div>
                            </div>
                            <div><a href="">Сбросить</a></div>
                            <div><a href="">Отменить</a></div>
                        </div>
                        <div class="m-filter-container">
                            <div class="m-filter-container-scroll">
                                <div class="cart-filter">
                                    <div class="accordion type2 js-cart-acc-m">
                                        <div class="accordion-item _open">
                                            <div class="accordion-title">
                                                <div>
                                                    Категории <span class="cart-filter-count">1</span>
                                                </div>
                                                <div>

                                                </div>
                                            </div>
                                            <div class="accordion-list">
                                                <div class="scroll">
                                                    <label class="label">
                                             <span class="checkbox">
                                                <input name="c1" type="checkbox"><i></i>
                                             </span>
                                                        <span class="label-title">Кроссовки</span>
                                                    </label>
                                                    <label class="label">
                                             <span class="checkbox">
                                                <input name="c1" type="checkbox"><i></i>
                                             </span>
                                                        <span class="label-title">Поло</span>
                                                    </label>
                                                    <label class="label">
                                             <span class="checkbox">
                                                <input name="c1" type="checkbox"><i></i>
                                             </span>
                                                        <span class="label-title">Свитера</span>
                                                    </label>
                                                    <label class="label">
                                             <span class="checkbox">
                                                <input name="c1" type="checkbox"><i></i>
                                             </span>
                                                        <span class="label-title">Толстовки</span>
                                                    </label>
                                                    <label class="label">
                                             <span class="checkbox">
                                                <input name="c1" type="checkbox"><i></i>
                                             </span>
                                                        <span class="label-title">Олимпийки</span>
                                                    </label>
                                                    <label class="label">
                                             <span class="checkbox">
                                                <input name="c1" type="checkbox"><i></i>
                                             </span>
                                                        <span class="label-title">Брюки</span>
                                                    </label>
                                                    <label class="label">
                                             <span class="checkbox">
                                                <input name="c1" type="checkbox"><i></i>
                                             </span>
                                                        <span class="label-title">Шорты</span>
                                                    </label>
                                                    <label class="label">
                                             <span class="checkbox">
                                                <input name="c1" type="checkbox"><i></i>
                                             </span>
                                                        <span class="label-title">Джинсы</span>
                                                    </label>
                                                    <label class="label">
                                             <span class="checkbox">
                                                <input name="c1" type="checkbox"><i></i>
                                             </span>
                                                        <span class="label-title">Кроссовки</span>
                                                    </label>
                                                    <label class="label">
                                             <span class="checkbox">
                                                <input name="c1" type="checkbox"><i></i>
                                             </span>
                                                        <span class="label-title">Поло</span>
                                                    </label>
                                                    <label class="label">
                                             <span class="checkbox">
                                                <input name="c1" type="checkbox"><i></i>
                                             </span>
                                                        <span class="label-title">Свитера</span>
                                                    </label>
                                                    <label class="label">
                                             <span class="checkbox">
                                                <input name="c1" type="checkbox"><i></i>
                                             </span>
                                                        <span class="label-title">Толстовки</span>
                                                    </label>
                                                    <label class="label">
                                             <span class="checkbox">
                                                <input name="c1" type="checkbox"><i></i>
                                             </span>
                                                        <span class="label-title">Олимпийки</span>
                                                    </label>
                                                    <label class="label">
                                             <span class="checkbox">
                                                <input name="c1" type="checkbox"><i></i>
                                             </span>
                                                        <span class="label-title">Брюки</span>
                                                    </label>
                                                    <label class="label">
                                             <span class="checkbox">
                                                <input name="c1" type="checkbox"><i></i>
                                             </span>
                                                        <span class="label-title">Шорты</span>
                                                    </label>
                                                    <label class="label">
                                             <span class="checkbox">
                                                <input name="c1" type="checkbox"><i></i>
                                             </span>
                                                        <span class="label-title">Джинсы</span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="accordion-item">
                                            <div class="accordion-title">
                                                <div>
                                                    Бренд
                                                </div>
                                                <div>
                                                    <div class="selected">Reebok</div>
                                                </div>
                                            </div>
                                            <div class="accordion-list">
                                                <div class="scroll">
                                                    <label class="label">
                                             <span class="checkbox">
                                                <input name="c1" type="checkbox"><i></i>
                                             </span>
                                                        <span class="label-title">Кроссовки</span>
                                                    </label>
                                                    <label class="label">
                                             <span class="checkbox">
                                                <input name="c1" type="checkbox"><i></i>
                                             </span>
                                                        <span class="label-title">Поло</span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="accordion-item">
                                            <div class="accordion-title">
                                                <div>
                                                    Размер
                                                </div>
                                                <div>
                                                    <div class="selected">
                                                        42 EU, 42.5 EU
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="accordion-list">
                                                <div class="scroll">
                                                    <label class="label">
                                             <span class="checkbox">
                                                <input name="c1" type="checkbox"><i></i>
                                             </span>
                                                        <span class="label-title">Кроссовки</span>
                                                    </label>
                                                    <label class="label">
                                             <span class="checkbox">
                                                <input name="c1" type="checkbox"><i></i>
                                             </span>
                                                        <span class="label-title">Поло</span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="accordion-item">
                                            <div class="accordion-title">
                                                <div>Пол</div>
                                                <div>
                                                    <div class="selected">

                                                    </div>
                                                </div>
                                            </div>
                                            <div class="accordion-list">
                                                <div class="scroll">
                                                    <label class="label">
                                             <span class="checkbox">
                                                <input name="c1" type="checkbox"><i></i>
                                             </span>
                                                        <span class="label-title">Кроссовки</span>
                                                    </label>
                                                    <label class="label">
                                             <span class="checkbox">
                                                <input name="c1" type="checkbox"><i></i>
                                             </span>
                                                        <span class="label-title">Поло</span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="accordion-item">
                                            <div class="accordion-title">
                                                <div>Цвет</div>
                                                <div>
                                                    <div class="selected">

                                                    </div>
                                                </div>
                                            </div>
                                            <div class="accordion-list">
                                                <div class="scroll">
                                                    <label class="label">
                                             <span class="checkbox">
                                                <input name="c1" type="checkbox"><i></i>
                                             </span>
                                                        <span class="label-title">Кроссовки</span>
                                                    </label>
                                                    <label class="label">
                                             <span class="checkbox">
                                                <input name="c1" type="checkbox"><i></i>
                                             </span>
                                                        <span class="label-title">Поло</span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="m-filter-bottom">
                            <button class="btn btn-primary">Показать</button>
                            <div class="m-filter-info">
                                64 товаров по выбранным критериям
                            </div>
                        </div>
                    </div>
                </div>

                <div class="m-sort">
                    <label>
                        <select class="js-choice-m">
                            <option value="">Дешевле выше</option>
                            <option value="">Дороже выше</option>
                            <option value="">Новинки выше</option>
                        </select>
                    </label>
                </div>
            </div>
            <div class="filtred-params-wrap">
                <div class="filtred-params-container">
                    <div class="filtred-params-item">
                        <div>Reebok</div>
                        Бренд
                    </div>
                    <div class="filtred-params-item">
                        <div>42, 42.5, 43, 44, 45</div>
                        Размер EU
                    </div>
                    <div class="filtred-params-item">
                        <div>Черный</div>
                        Цвет
                    </div>
                    <div class="filtred-params-item">
                        <div>Кожа</div>
                        Материал
                    </div>
                    <div class="filtred-params-item">
                        <div>Мужской</div>
                        Пол
                    </div>
                </div>
            </div>
            <div class="cart">
                <div class="cart-col">
                    <div class="cart-col-l">
                        <div class="cart-filter">
                            <div class="accordion type2 js-cart-acc">
                                <div class="accordion-item _open">
                                    <div class="accordion-title">
                                        Категории <span class="cart-filter-count">1</span>
                                    </div>
                                    <div class="accordion-list">
                                        <div class="scroll">
                                            <label class="label">
                                       <span class="checkbox">
                                          <input name="c1" type="checkbox"><i></i>
                                       </span>
                                                <span class="label-title">Кроссовки</span>
                                            </label>
                                            <label class="label">
                                       <span class="checkbox">
                                          <input name="c1" type="checkbox"><i></i>
                                       </span>
                                                <span class="label-title">Поло</span>
                                            </label>
                                            <label class="label">
                                       <span class="checkbox">
                                          <input name="c1" type="checkbox"><i></i>
                                       </span>
                                                <span class="label-title">Свитера</span>
                                            </label>
                                            <label class="label">
                                       <span class="checkbox">
                                          <input name="c1" type="checkbox"><i></i>
                                       </span>
                                                <span class="label-title">Толстовки</span>
                                            </label>
                                            <label class="label">
                                       <span class="checkbox">
                                          <input name="c1" type="checkbox"><i></i>
                                       </span>
                                                <span class="label-title">Олимпийки</span>
                                            </label>
                                            <label class="label">
                                       <span class="checkbox">
                                          <input name="c1" type="checkbox"><i></i>
                                       </span>
                                                <span class="label-title">Брюки</span>
                                            </label>
                                            <label class="label">
                                       <span class="checkbox">
                                          <input name="c1" type="checkbox"><i></i>
                                       </span>
                                                <span class="label-title">Шорты</span>
                                            </label>
                                            <label class="label">
                                       <span class="checkbox">
                                          <input name="c1" type="checkbox"><i></i>
                                       </span>
                                                <span class="label-title">Джинсы</span>
                                            </label>
                                            <label class="label">
                                       <span class="checkbox">
                                          <input name="c1" type="checkbox"><i></i>
                                       </span>
                                                <span class="label-title">Кроссовки</span>
                                            </label>
                                            <label class="label">
                                       <span class="checkbox">
                                          <input name="c1" type="checkbox"><i></i>
                                       </span>
                                                <span class="label-title">Поло</span>
                                            </label>
                                            <label class="label">
                                       <span class="checkbox">
                                          <input name="c1" type="checkbox"><i></i>
                                       </span>
                                                <span class="label-title">Свитера</span>
                                            </label>
                                            <label class="label">
                                       <span class="checkbox">
                                          <input name="c1" type="checkbox"><i></i>
                                       </span>
                                                <span class="label-title">Толстовки</span>
                                            </label>
                                            <label class="label">
                                       <span class="checkbox">
                                          <input name="c1" type="checkbox"><i></i>
                                       </span>
                                                <span class="label-title">Олимпийки</span>
                                            </label>
                                            <label class="label">
                                       <span class="checkbox">
                                          <input name="c1" type="checkbox"><i></i>
                                       </span>
                                                <span class="label-title">Брюки</span>
                                            </label>
                                            <label class="label">
                                       <span class="checkbox">
                                          <input name="c1" type="checkbox"><i></i>
                                       </span>
                                                <span class="label-title">Шорты</span>
                                            </label>
                                            <label class="label">
                                       <span class="checkbox">
                                          <input name="c1" type="checkbox"><i></i>
                                       </span>
                                                <span class="label-title">Джинсы</span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="accordion-item">
                                    <div class="accordion-title">
                                        Бренд
                                    </div>
                                    <div class="accordion-list">
                                        <div class="scroll">
                                            <label class="label">
                                       <span class="checkbox">
                                          <input name="c1" type="checkbox"><i></i>
                                       </span>
                                                <span class="label-title">Кроссовки</span>
                                            </label>
                                            <label class="label">
                                       <span class="checkbox">
                                          <input name="c1" type="checkbox"><i></i>
                                       </span>
                                                <span class="label-title">Поло</span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="accordion-item">
                                    <div class="accordion-title">
                                        Размер
                                    </div>
                                    <div class="accordion-list">
                                        <div class="scroll">
                                            <label class="label">
                                       <span class="checkbox">
                                          <input name="c1" type="checkbox"><i></i>
                                       </span>
                                                <span class="label-title">Кроссовки</span>
                                            </label>
                                            <label class="label">
                                       <span class="checkbox">
                                          <input name="c1" type="checkbox"><i></i>
                                       </span>
                                                <span class="label-title">Поло</span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="accordion-item">
                                    <div class="accordion-title">
                                        Пол
                                    </div>
                                    <div class="accordion-list">
                                        <div class="scroll">
                                            <label class="label">
                                       <span class="checkbox">
                                          <input name="c1" type="checkbox"><i></i>
                                       </span>
                                                <span class="label-title">Кроссовки</span>
                                            </label>
                                            <label class="label">
                                       <span class="checkbox">
                                          <input name="c1" type="checkbox"><i></i>
                                       </span>
                                                <span class="label-title">Поло</span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="accordion-item">
                                    <div class="accordion-title">
                                        Цвет
                                    </div>
                                    <div class="accordion-list">
                                        <div class="scroll">
                                            <label class="label">
                                       <span class="checkbox">
                                          <input name="c1" type="checkbox"><i></i>
                                       </span>
                                                <span class="label-title">Кроссовки</span>
                                            </label>
                                            <label class="label">
                                       <span class="checkbox">
                                          <input name="c1" type="checkbox"><i></i>
                                       </span>
                                                <span class="label-title">Поло</span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="cart-col-r">
                        <div class="cart-list">
                            <?php
                                the_content( );
                            ?>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <?php /*
    <div id="primary" class="posts-display-page">
        <main id="main" class="site-main" role="main">
            <div class="container">
                <div class="text-page__inner">
                    <?php
                    if(have_posts()) : while(have_posts()) : the_post();
                        ?>
                        <div class="display-post">
                            <div class="single-article-image">
                                <a href="<?php echo get_permalink($post->ID);?>">
                                    <?php
                                    if ( has_post_thumbnail() ) {
                                        the_post_thumbnail( 'full' );
                                    }
                                    ?>
                                </a>
                            </div>
                            <?php  $categories = get_categories(); ?>
                            <div class="category-list">
                                <ul>
                                    <?php
                                    foreach($categories as $category) {
                                        echo '<li class="category-item"><a href="' . get_category_link($category->term_id) . '">'. $category->name .'</a><span>/</span></li>';
                                    }
                                    ?>
                                </ul>
                            </div>
                            <h2 class="title-post">
                                <a href="<?php echo get_permalink($post->ID);?>">
                                    <?php the_title(); ?>
                                </a>
                            </h2>
                            <a href="<?php echo get_permalink($post->ID);?>">
                                <?php
                                echo '<div class="entry-content">';
                                the_excerpt();
                                echo '</div>';
                                ?>
                            </a>
                        </div>
                    <?php  endwhile;
                    endif;   ?>
                </div>
            </div>
        </main>
    </div>
*/?>

<?php get_footer('main'); ?>