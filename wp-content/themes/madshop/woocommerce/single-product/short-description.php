<?php
/**
 * Single product short description
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/short-description.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}
/*
global $post;

$short_description = apply_filters( 'woocommerce_short_description', $post->post_excerpt );

if ( ! $short_description ) {
	return;
}

?>
<div class="cart-item-params accordion js-cart-item-acc">
    <div class="cart-item-param accordion-item _open-alweys">
        <div class="accordion-title">
            <div>Описание</div>
        </div>
        <div class="accordion-list">
            Мужские кроссовки из коричневой кожи с белой подошвой. Шнурки, подпятник и логотипы
            Reebok в черном цвете. Дополнительный черный резиновый слой под подошвой
            обеспечивает практичность белого цвета.
        </div>
    </div>
    <div class="cart-item-param row">
        <div class="cart-item-param-title">
            <div>Артикул</div>
        </div>
        <div class="cart-item-param-content">
            EV3100ZN
        </div>
    </div>
    <div class="cart-item-param row">
        <div class="cart-item-param-title">
            <div>Материал верха</div>
        </div>
        <div class="cart-item-param-content">
            Натуральная кожа, резина
        </div>
    </div>
    <div class="cart-item-param row">
        <div class="cart-item-param-title">
            <div>Страна производства</div>
        </div>
        <div class="cart-item-param-content">
            Вьетнам
        </div>
    </div>
*/?>