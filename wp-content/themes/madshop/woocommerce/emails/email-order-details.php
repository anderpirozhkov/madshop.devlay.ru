<?php
/**
 * Order details table shown in emails.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/email-order-details.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates\Emails
 * @version 3.7.0
 */

defined('ABSPATH') || exit;
?>


<div style="background-color:#f2f2f2;padding:10px;">
    <!--[if mso | IE]>
    <table align="center" border="0" cellpadding="0" cellspacing="0" class="sct-outlook id-41070-outlook sk-outlook pr-16-outlook pl-16-outlook -outlook" style="width:440px;" width="600"><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->
                <div class="sct id-41070 sk pr-16 pl-16 " style="background:#ffffff;background-color:#ffffff;margin:0px auto;border-radius:2px 2px 0px 0px;max-width:440px;">
                    <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:#ffffff;background-color:#ffffff;width:100%;border-radius:2px 2px 0px 0px;">
                        <tbody>
                            <tr>
                                <td style="border:none;direction:ltr;font-size:0px;padding:20px 15px 0px 15px;text-align:left;">
                                    <!--[if mso | IE]><table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td class="last-outlook cl-outlook id-41071-outlook -outlook" style="vertical-align:middle;width:570px;"><![endif]-->
                                                <div class="mj-column-px-570 mj-outlook-group-fix last cl id-41071 " style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:middle;width:100%;">
                                                    <table border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%">
                                                        <tbody>
                                                            <tr>
                                                                <td style="background-color:transparent;border:none;border-radius:0px 0px 0px 0px;vertical-align:middle;padding:0px 0px 0px 0px;">
                                                                    <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="" width="100%">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td align="center" class="img id-41073 last" style="font-size:0px;padding:0px;padding-bottom:0px;word-break:break-word;">
                                                                                    <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="border-collapse:collapse;border-spacing:0px;">
                                                                                        <tbody>
                                                                                            <tr>
                                                                                                <td style="width:50px;">
                                                                                                    <img alt="" height="auto" src="<?= get_template_directory_uri() ?>/img/41073.png" style="border:0;border-radius:0px 0px 0px 0px;display:block;outline:none;text-decoration:none;height:auto;width:100%;font-size:13px;" width="50"/>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </tbody>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <!--[if mso | IE]></td></tr></table><![endif]-->
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <!--[if mso | IE]></td></tr></table><table align="center" border="0" cellpadding="0" cellspacing="0" class="sct-outlook id-41082-outlook sk-outlook pr-16-outlook pl-16-outlook -outlook" style="width:440px;" width="600"><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]--><div class="sct id-41082 sk pr-16 pl-16 " style="background:#ffffff;background-color:#ffffff;margin:0px auto;border-radius:0px 0px 0px 0px;max-width:440px;">
        <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:#ffffff;background-color:#ffffff;width:100%;border-radius:0px 0px 0px 0px;"><tbody><tr><td style="border:none;direction:ltr;font-size:0px;padding:15px 0px 10px 0px;text-align:center;">
                    <!--[if mso | IE]><table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td class="last-outlook cl-outlook id-41083-outlook -outlook" style="vertical-align:middle;width:440px;"><![endif]--><div class="mj-column-px-600 mj-outlook-group-fix last cl id-41083 " style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:middle;width:100%;">
                        <table border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%"><tbody><tr><td style="background-color:transparent;border:none;border-radius:0px 0px 0px 0px;vertical-align:middle;padding:0px 0px 0px 0px;">
                                    <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="" width="100%"><tbody><tr><td align="center" class="txt id-41084 middle" style="font-size:0px;padding:0px 20px 0px 20px;padding-bottom:8px;word-break:break-word;">
                                                <div style="font-family:Helvetica nue, sans-serif;font-size:21px;line-height:23px;text-align:center;color:#000000;width:220px;">
                                                    <span style="mso-line-height-rule:exactly;font-size:21px;font-family:Helvetica nue, sans-serif;text-transform:none;font-style:none;font-weight:400;color:#000000;line-height:23px;letter-spacing:0px;text-decoration:initial;">Заказ №<?= $order->get_order_number() ?> оформлен успешно</span></div>
                                            </td></tr><tr><td align="center" class="txt id-41085 last" style="font-size:0px;padding:0px 0px 0px 0px;padding-bottom:0px;word-break:break-word;"><div style="font-family:Helvetica nue, sans-serif;font-size:14px;line-height:21px;text-align:center;color:#000000;">
                                                    <span style="mso-line-height-rule:exactly;font-size:14px;font-family:Helvetica nue, sans-serif;text-transform:none;font-style:none;font-weight:400;color:#333333;line-height:21px;letter-spacing:0px;text-decoration:initial;">Мы свяжемся с Вами в ближайшее время.</span></div>
                                            </td></tr></tbody></table>
                                </td></tr></tbody></table></div>
                    <!--[if mso | IE]></td></tr></table><![endif]-->
                </td></tr></tbody></table></div>
    <!--[if mso | IE]></td></tr></table><table align="center" border="0" cellpadding="0" cellspacing="0" class="sct-outlook id-41052-outlook sk-outlook pr-16-outlook pl-16-outlook -outlook" style="width:440px;" width="600"><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->
        <?= wc_get_email_order_items(
                $order,
                array(
                    'show_sku' => $sent_to_admin,
                    'show_image' => false,
                    'image_size' => array(32, 32),
                    'plain_text' => $plain_text,
                    'sent_to_admin' => $sent_to_admin,
                )
            );
            ?>
    <!--[if mso | IE]></td></tr></table><table align="center" border="0" cellpadding="0" cellspacing="0" class="sct-outlook id-410156-outlook pr-16-outlook pl-16-outlook -outlook" style="width:440px;" width="600"><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]--><div class="sct id-410156 pr-16 pl-16 " style="background:#ffffff;background-color:#ffffff;margin:0px auto;border-radius:0px 0px 0px 0px;max-width:440px;margin-top:-1px;">
        <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:#ffffff;background-color:#ffffff;width:100%;border-radius:0px 0px 0px 0px;"><tbody><tr><td style="border:none;direction:ltr;font-size:0px;padding:15px 10px 5px 15px;text-align:left;">
                    <!--[if mso | IE]>
                    <table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td class="" style="width:570px;">
                    <![endif]--><div class="mj-column-per-100 mj-outlook-group-fix" style="font-size:0;line-height:0;text-align:left;display:inline-block;width:100%;direction:ltr;">
                        <!--[if mso | IE]>
                        <table border="0" cellpadding="0" cellspacing="0" role="presentation"><tr><td style="vertical-align:middle;width:279px;">
                        <![endif]--><div class="mj-column-per-49 mj-outlook-group-fix middle cl id-410157 " style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:middle;width:49%;">
                            <table border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%"><tbody><tr><td style="background-color:transparent;border:none;border-radius:0px 0px 0px 0px;vertical-align:middle;padding:0px 0px 0px 0px;">
                                        <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="" width="100%"><tbody><tr><td align="left" class="txt id-410158 last" style="font-size:0px;padding:0px 0px 0px 0px;padding-bottom:0px;word-break:break-word;"><div style="font-family:Helvetica nue, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#000000;"><span style="mso-line-height-rule:exactly;font-size:14px;font-family:Helvetica nue, sans-serif;text-transform:none;font-style:none;font-weight:400;color:#999999;line-height:20.019999265670776px;letter-spacing:0px;text-decoration:initial;">Сумма</span></div>
                                                </td></tr></tbody></table>
                                    </td></tr></tbody></table></div>
                        <!--[if mso | IE]>
                        </td><td style="vertical-align:top;width:5px;">
                        <![endif]--><div class="mj-column-per-1 mj-outlook-group-fix gtr" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:1%;">
                            <table border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%"><tbody><tr><td style="vertical-align:top;padding:0px;">
                                        <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="" width="100%"><tbody></tbody></table>
                                    </td></tr></tbody></table></div>
                        <!--[if mso | IE]>
                        </td><td style="vertical-align:middle;width:279px;">
                        <![endif]--><div class="mj-column-per-49 mj-outlook-group-fix last cl id-410159 " style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:middle;width:49%;">
                            <table border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%"><tbody><tr><td style="background-color:transparent;border:none;border-radius:0px 0px 0px 0px;vertical-align:middle;padding:0px 0px 0px 0px;">
                                        <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="" width="100%"><tbody><tr><td align="right" class="txt id-410160 last" style="font-size:0px;padding:0px 0px 0px 0px;padding-bottom:0px;word-break:break-word;"><div style="font-family:Helvetica nue, sans-serif;font-size:14px;line-height:16px;text-align:right;color:#000000;"><span style="mso-line-height-rule:exactly;font-size:14px;font-family:Helvetica nue, sans-serif;text-transform:none;font-style:none;font-weight:400;color:#000000;line-height:16.37999939918518px;letter-spacing:0px;text-decoration:initial;"><?= number_format($order->get_total(), 0, ',', ' ') ?> ₽</span></div>
                                                </td></tr></tbody></table>
                                    </td></tr></tbody></table></div>
                        <!--[if mso | IE]>
                        </td></tr></table>
                        <![endif]--></div>
                    <!--[if mso | IE]>
                    </td></tr></table>
                    <![endif]-->
                </td></tr></tbody></table></div>
    <!--[if mso | IE]>
    </td></tr></table>
    <table align="center" border="0" cellpadding="0" cellspacing="0" class="sct-outlook id-410174-outlook pr-16-outlook pl-16-outlook -outlook" style="width:440px;" width="600"><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
    <![endif]-->
    <?php $discont = $order->get_discount_total();
    if($discont > 0) {
    ?>
    <div class="sct id-410174 pr-16 pl-16 " style="background:#ffffff;background-color:#ffffff;margin:0px auto;border-radius:0px 0px 0px 0px;max-width:440px;">
        <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:#ffffff;background-color:#ffffff;width:100%;border-radius:0px 0px 0px 0px;"><tbody><tr><td style="border:none;direction:ltr;font-size:0px;padding:0px 15px 8px 15px;text-align:left;">
                    <!--[if mso | IE]>
                    <table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td class="" style="width:570px;">
                    <![endif]--><div class="mj-column-per-100 mj-outlook-group-fix" style="font-size:0;line-height:0;text-align:left;display:inline-block;width:100%;direction:ltr;">
                        <!--[if mso | IE]>
                        <table border="0" cellpadding="0" cellspacing="0" role="presentation"><tr><td style="vertical-align:middle;width:279px;">
                        <![endif]--><div class="mj-column-per-49 mj-outlook-group-fix middle cl id-410175 " style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:middle;width:49%;">
                            <table border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%"><tbody><tr><td style="background-color:transparent;border:none;border-radius:0px 0px 0px 0px;vertical-align:middle;padding:0px 0px 0px 0px;">
                                        <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="" width="100%"><tbody><tr><td align="left" class="txt id-410176 last" style="font-size:0px;padding:0px 0px 0px 0px;padding-bottom:0px;word-break:break-word;"><div style="font-family:Helvetica nue, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#000000;">
                                                        <span style="mso-line-height-rule:exactly;font-size:14px;font-family:Helvetica nue, sans-serif;text-transform:none;font-style:none;font-weight:400;color:#999999;line-height:20.019999265670776px;letter-spacing:0px;text-decoration:initial;">Скидка</span></div>
                                                </td></tr></tbody></table>
                                    </td></tr></tbody></table></div>
                        <!--[if mso | IE]>
                        </td><td style="vertical-align:top;width:5px;">
                        <![endif]--><div class="mj-column-per-1 mj-outlook-group-fix gtr" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:1%;">
                            <table border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%"><tbody><tr><td style="vertical-align:top;padding:0px;">
                                        <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="" width="100%"><tbody></tbody></table>
                                    </td></tr></tbody></table></div>
                        <!--[if mso | IE]>
                        </td><td style="vertical-align:middle;width:279px;">
                        <![endif]--><div class="mj-column-per-49 mj-outlook-group-fix last cl id-410177 " style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:middle;width:49%;">
                            <table border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%"><tbody><tr><td style="background-color:transparent;border:none;border-radius:0px 0px 0px 0px;vertical-align:middle;padding:0px 0px 0px 0px;">
                                        <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="" width="100%"><tbody><tr><td align="right" class="txt id-410178 last" style="font-size:0px;padding:0px 0px 0px 0px;padding-bottom:0px;word-break:break-word;"><div style="font-family:Helvetica nue, sans-serif;font-size:14px;line-height:16px;text-align:right;color:#000000;">
                                                       <?php $discont = $order->get_discount_total(); ?>
                                                        <span style="mso-line-height-rule:exactly;font-size:14px;font-family:Helvetica nue, sans-serif;text-transform:none;font-style:none;font-weight:400;color:#000000;line-height:16.37999939918518px;letter-spacing:0px;text-decoration:initial;"><?= $discont > 0 ? "- ".$discont." ₽" : ""?></span></div>
                                                </td></tr></tbody></table>
                                    </td></tr></tbody></table></div>
                        <!--[if mso | IE]>
                        </td></tr></table>
                        <![endif]--></div>
                    <!--[if mso | IE]>
                    </td></tr></table>
                    <![endif]-->
                </td></tr></tbody></table>
    </div>
    <?php } ?>
    <!--[if mso | IE]>
    </td></tr></table>
    <table align="center" border="0" cellpadding="0" cellspacing="0" class="sct-outlook id-410179-outlook pr-16-outlook pl-16-outlook -outlook" style="width:440px;" width="600"><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
    <![endif]--><div class="sct id-410179 pr-16 pl-16 " style="background:#ffffff;background-color:#ffffff;margin:0px auto;border-radius:0px 0px 2px 2px;max-width:440px;">
        <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:#ffffff;background-color:#ffffff;width:100%;border-radius:0px 0px 2px 2px;"><tbody><tr><td style="border:none;direction:ltr;font-size:0px;padding:5px 15px 20px 15px;text-align:center;">
                    <!--[if mso | IE]>
                    <table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td class="" style="width:570px;">
                    <![endif]--><div class="mj-column-per-100 mj-outlook-group-fix" style="font-size:0;line-height:0;text-align:left;display:inline-block;width:100%;direction:ltr;border-top: 1px solid #ddd;padding-top:10px;">
                        <!--[if mso | IE]>
                        <table border="0" cellpadding="0" cellspacing="0" role="presentation"><tr><td style="vertical-align:middle;width:279px;">
                        <![endif]--><div class="mj-column-per-49 mj-outlook-group-fix middle cl id-410180 " style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:middle;width:49%;">
                            <table border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%"><tbody><tr><td style="background-color:transparent;border:none;border-radius:0px 0px 0px 0px;vertical-align:middle;padding:0px 0px 0px 0px;">
                                        <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="" width="100%"><tbody><tr><td align="left" class="txt id-410181 last" style="font-size:0px;padding:0px 0px 0px 0px;padding-bottom:0px;word-break:break-word;"><div style="font-family:Helvetica nue, sans-serif;font-size:18px;line-height:26px;text-align:left;color:#000000;">
                                                        <span style="mso-line-height-rule:exactly;font-size:18px;font-family:Helvetica nue, sans-serif;text-transform:none;font-style:none;font-weight:400;color:#000000;line-height:25.739999055862427px;letter-spacing:0px;text-decoration:initial;">Итого</span></div>
                                                </td></tr></tbody></table>
                                    </td></tr></tbody></table></div>
                        <!--[if mso | IE]>
                        </td><td style="vertical-align:top;width:5px;">
                        <![endif]--><div class="mj-column-per-1 mj-outlook-group-fix gtr" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:1%;">
                            <table border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%"><tbody><tr><td style="vertical-align:top;padding:0px;">
                                        <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="" width="100%"><tbody></tbody></table>
                                    </td></tr></tbody></table></div>
                        <!--[if mso | IE]>
                        </td><td style="vertical-align:middle;width:279px;">
                        <![endif]--><div class="mj-column-per-49 mj-outlook-group-fix last cl id-410182 " style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:middle;width:49%;">
                            <table border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%"><tbody><tr><td style="background-color:transparent;border:none;border-radius:0px 0px 0px 0px;vertical-align:middle;padding:0px 0px 0px 0px;">
                                        <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="" width="100%"><tbody><tr><td align="right" class="txt id-410183 last" style="font-size:0px;padding:0px 0px 0px 0px;padding-bottom:0px;word-break:break-word;"><div style="font-family:Helvetica nue, sans-serif;font-size:18px;line-height:21px;text-align:right;color:#000000;">
                                                        <span style="mso-line-height-rule:exactly;font-size:18px;font-family:Helvetica nue, sans-serif;text-transform:none;font-style:none;font-weight:400;color:#000000;line-height:21.059999227523804px;letter-spacing:0px;text-decoration:initial;"><?= number_format($order->get_total(), 0, ',', ' ') ?> ₽</span></div>
                                                </td></tr></tbody></table>
                                    </td></tr></tbody></table></div>
                        <!--[if mso | IE]>
                        </td></tr></table>
                        <![endif]--></div>
                    <!--[if mso | IE]>
                    </td></tr></table>
                    <![endif]-->
                </td></tr></tbody></table></div>
    <!--[if mso | IE]>
    </td></tr></table>
    <table align="center" border="0" cellpadding="0" cellspacing="0" class="sct-outlook id-410219-outlook sk-outlook pr-16-outlook pl-16-outlook -outlook" style="width:440px;" width="600"><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
    <![endif]--><div class="sct id-410219 sk pr-16 pl-16 " style="background:#f2f2f2;background-color:#f2f2f2;margin:0px auto;border-radius:0px 0px 0px 0px;max-width:440px;">
        <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:#f2f2f2;background-color:#f2f2f2;width:100%;border-radius:0px 0px 0px 0px;"><tbody><tr><td style="border:none;direction:ltr;font-size:0px;padding:10px 0px 10px 0px;text-align:left;">
                    <!--[if mso | IE]>
                    <table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td class="last-outlook cl-outlook id-410220-outlook -outlook" style="vertical-align:middle;width:568px;">
                    <![endif]--><div class="mj-column-px-568 mj-outlook-group-fix last cl id-410220 " style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:middle;width:100%;">
                        <table border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%"><tbody><tr><td style="background-color:#ffffff;border:none;border-radius:2px 2px 2px 2px;vertical-align:middle;padding:15px 10px 20px 10px">
                                    <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="" width="100%"><tbody><tr><td align="left" class="txt id-410221 middle" style="font-size:0px;padding:0px 0px 0px 0px;word-break:break-word;"><div style="font-family:Helvetica nue, sans-serif;font-size:10px;line-height:12px;text-align:left;color:#000000;">
                                                    <span style="mso-line-height-rule:exactly;font-size:10px;font-family:Helvetica nue, sans-serif;text-transform:none;font-style:none;font-weight:400;color:#999999;line-height:12px;letter-spacing:0px;text-decoration:initial;">Способ доставки</span></div>
                                            </td></tr><tr><td align="left" class="txt id-410222 middle" style="font-size:0px;padding:0px 0px 0px 0px;word-break:break-word;"><div style="font-family:Helvetica nue, sans-serif;font-size:12px;line-height:16px;text-align:left;color:#000000;border-bottom:0.5px solid #eee;padding-bottom:10px;margin-bottom: 10px;">
                                                    <span style="mso-line-height-rule:exactly;font-size:12px;font-family:Helvetica nue, sans-serif;text-transform:none;font-style:none;font-weight:400;color:#000000;line-height:16px;letter-spacing:0px;text-decoration:initial;"><?= $order->get_shipping_method() ?></span></div>
                                            </td></tr><tr><td align="left" class="txt id-410227 middle" style="font-size:0px;padding:0px 0px 0px 0px;word-break:break-word;"><div style="font-family:Helvetica nue, sans-serif;font-size:10px;line-height:12px;text-align:left;color:#000000;">
                                                    <span style="mso-line-height-rule:exactly;font-size:10px;font-family:Helvetica nue, sans-serif;text-transform:none;font-style:none;font-weight:400;color:#999999;line-height:12px;letter-spacing:0px;text-decoration:initial;">Получатель и контакты</span></div>
                                            </td></tr>
                                        <tr>
                                            <td align="left" class="txt id-410233 middle" style="font-size:0px;padding:0px 0px 0px 0px;word-break:break-word;">
                                                <div style="font-family:Helvetica nue, sans-serif;font-size:12px;line-height:16px;text-align:left;color:#000000;">
                                                    <span style="mso-line-height-rule:exactly;font-size:12px;font-family:Helvetica nue, sans-serif;text-transform:none;font-style:none;font-weight:400;color:#000000;line-height:16px;letter-spacing:0px;text-decoration:initial;">
                                                        Адрес: <?= $order->get_billing_city() .' '.$order->get_billing_address_1() ?>
                                                    </span>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" class="txt id-410233 middle" style="font-size:0px;padding:0px 0px 0px 0px;word-break:break-word;">
                                                <div style="font-family:Helvetica nue, sans-serif;font-size:12px;line-height:16px;text-align:left;color:#000000;">
                                                    <span style="mso-line-height-rule:exactly;font-size:12px;font-family:Helvetica nue, sans-serif;text-transform:none;font-style:none;font-weight:400;color:#000000;line-height:16px;letter-spacing:0px;text-decoration:initial;">
                                                        <?= $order->get_billing_last_name().' '.$order->get_billing_first_name() ?>
                                                    </span>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" class="txt id-410235 middle" style="font-size:0px;padding:0px 0px 0px 0px;word-break:break-word;">
                                                <div style="font-family:Helvetica nue, sans-serif;font-size:12px;line-height:16px;text-align:left;color:#000000;">
                                                    <span style="mso-line-height-rule:exactly;font-size:12px;font-family:Helvetica nue, sans-serif;text-transform:none;font-style:none;font-weight:400;color:#000000;line-height:16px;letter-spacing:0px;text-decoration:initial;">
                                                        <?= $order->get_billing_email() ?>
                                                    </span>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" class="txt id-410234 middle" style="font-size:0px;padding:0px 0px 0px 0px;word-break:break-word;">
                                                <div style="font-family:Helvetica nue, sans-serif;font-size:12px;line-height:16px;text-align:left;color:#000000;border-bottom:0.5px solid #eee;padding-bottom:10px;margin-bottom: 10px;">
                                                    <span style="mso-line-height-rule:exactly;font-size:12px;font-family:Helvetica nue, sans-serif;text-transform:none;font-style:none;font-weight:400;color:#000000;line-height:16px;letter-spacing:0px;text-decoration:initial;">
                                                        <?= $order->get_billing_phone() ?>
                                                    </span>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr><td align="left" class="txt id-410232 middle" style="font-size:0px;padding:0px 0px 0px 0px;word-break:break-word;"><div style="font-family:Helvetica nue, sans-serif;font-size:10px;line-height:12px;text-align:left;color:#000000;">
                                                    <span style="mso-line-height-rule:exactly;font-size:10px;font-family:Helvetica nue, sans-serif;text-transform:none;font-style:none;font-weight:400;color:#999999;line-height:12px;letter-spacing:0px;text-decoration:initial;">Способ оплаты</span></div>
                                            </td></tr><tr><td align="left" class="txt id-410228 last" style="font-size:0px;padding:0px 0px 0px 0px;padding-bottom:0px;word-break:break-word;"><div style="font-family:Helvetica nue, sans-serif;font-size:12px;line-height:16px;text-align:left;color:#000000;">
                                                    <span style="mso-line-height-rule:exactly;font-size:12px;font-family:Helvetica nue, sans-serif;text-transform:none;font-style:none;font-weight:400;color:#000000;line-height:16px;letter-spacing:0px;text-decoration:initial;"><?= $order->get_payment_method_title() ?></span></div>
                                            </td></tr></tbody></table>
                                </td></tr></tbody></table></div>
                    <!--[if mso | IE]>
                    </td></tr></table>
                    <![endif]-->
                </td></tr></tbody></table></div>
    <!--[if mso | IE]>
    </td></tr></table>
    <table align="center" border="0" cellpadding="0" cellspacing="0" class="sct-outlook id-410236-outlook sk-outlook pr-16-outlook pl-16-outlook -outlook" style="width:440px;" width="600"><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
    <![endif]--><div class="sct id-410236 sk pr-16 pl-16 " style="background:#f2f2f2;background-color:#f2f2f2;margin:0px auto;border-radius:0px 0px 0px 0px;max-width:440px;">
        <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:#f2f2f2;background-color:#f2f2f2;width:100%;border-radius:0px 0px 0px 0px;"><tbody><tr><td style="border:none;direction:ltr;font-size:0px;padding:0px 32px 5px 32px;text-align:center;">
                    <!--[if mso | IE]>
                    <table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td class="last-outlook cl-outlook id-410237-outlook -outlook" style="vertical-align:middle;width:536px;">
                    <![endif]--><div class="mj-column-px-536 mj-outlook-group-fix last cl id-410237 " style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:middle;width:100%;">
                        <table border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%"><tbody><tr><td style="background-color:transparent;border:none;border-radius:0px 0px 0px 0px;vertical-align:middle;padding:0px 0px 0px 0px;">
                                    <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="" width="100%"><tbody><tr><td align="center" class="txt id-410238 last" style="font-size:0px;padding:0px 0px 0px 0px;padding-bottom:0px;word-break:break-word;">
                                                <div style="font-family:Helvetica nue, sans-serif;font-size:10px;line-height:13px;text-align:center;color:#000000;">
                                                    <span style="mso-line-height-rule:exactly;font-size:10px;font-family:Helvetica nue, sans-serif;text-transform:none;font-style:none;font-weight:400;color:#999999;line-height:13px;letter-spacing:0px;text-decoration:initial;">Вы получили это письмо, так как оформили заказ на сайте MADshop.ru Если вы не оформляли заказ и не соврешали покупку, нажмите на ссылку ниже</span></div>
                                            </td></tr></tbody></table>
                                </td></tr></tbody></table></div>
                    <!--[if mso | IE]>
                    </td></tr></table>
                    <![endif]-->
                </td></tr></tbody></table></div>
    <!--[if mso | IE]>
    </td></tr></table>
    <table align="center" border="0" cellpadding="0" cellspacing="0" class="sct-outlook id-410239-outlook sk-outlook pr-16-outlook pl-16-outlook -outlook" style="width:440px;" width="600"><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
    <![endif]--><div class="sct id-410239 sk pr-16 pl-16 " style="background:#f2f2f2;background-color:#f2f2f2;margin:0px auto;border-radius:0px 0px 0px 0px;max-width:440px;">
        <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:#f2f2f2;background-color:#f2f2f2;width:100%;border-radius:0px 0px 0px 0px;"><tbody><tr><td style="border:none;direction:ltr;font-size:0px;padding:0px 16px 15px 16px;text-align:left;">
                    <!--[if mso | IE]>
                    <table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td class="last-outlook cl-outlook id-410240-outlook -outlook" style="vertical-align:middle;width:568px;">
                    <![endif]--><div class="mj-column-px-568 mj-outlook-group-fix last cl id-410240 " style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:middle;width:100%;">
                        <table border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%"><tbody><tr><td style="background-color:transparent;border:none;border-radius:0px 0px 0px 0px;vertical-align:middle;padding:0px 0px 0px 0px;">
                                    <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="" width="100%"><tbody><tr><td align="center" vertical-align="middle" class="btn id-410241 last" style="font-size:0px;padding:0px;padding-bottom:0px;word-break:break-word;">
                                                <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="border-collapse:separate;line-height:100%;"><tr><td align="center" bgcolor="transparent" role="presentation" style="border:none;border-radius:0px 0px 0px 0px;cursor:auto;mso-padding-alt:4px 4px 4px 4px;background:transparent;" valign="middle"> <a
                                                                    href="#insertUrlLink" style="display:inline-block;background:transparent;color:#ffffff;font-family:Helvetica nue, sans-serif;font-size:13px;font-weight:normal;line-height:100%;margin:0;text-decoration:none;text-transform:none;padding:4px 4px 4px 4px;mso-padding-alt:0px;border-radius:0px 0px 0px 0px;" target="_blank">
                                                                <span style="mso-line-height-rule:exactly;font-size:12px;font-family:Helvetica nue, sans-serif;text-transform:none;font-style:none;font-weight:400;color:#999999;line-height:14px;letter-spacing:0px;text-decoration:underline;">Отказаться от заказа</span></a>
                                                        </td></tr></table>
                                            </td></tr></tbody></table>
                                </td></tr></tbody></table></div>
                    <!--[if mso | IE]>
                    </td></tr></table>
                    <![endif]-->
                </td></tr></tbody></table></div>
    <!--[if mso | IE]>
    </td></tr></table>
    <![endif]--></div>
