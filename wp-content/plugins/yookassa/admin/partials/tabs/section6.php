<?php

?>
<form id="yoomoney-form-6" class="yoomoney-form">
    <div class="col-md-12">

        <div class="row">
            <div class="col-md-12 text-center">
                <div class="label-container icon_name_checkmark-green"></div>
                <h4 class="success"><?= __('Всё готово, чтобы принимать платежи', 'yookassa') ?></h4>
                <p><?= __('Вы можете вернуться и изменить настройки в любой момент', 'yookassa') ?></p>
            </div>
        </div>
        <div class="row form-footer">
            <div class="col-md-12 text-center">
                <button class="btn btn-light btn-back" data-tab="section1" style="margin:0;"><?= __('К настройкам', 'yookassa') ?></button>
            </div>
        </div>
    </div>

</form>
