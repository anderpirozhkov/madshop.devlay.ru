<?php
//include __DIR__.'/ProductDataParser.php';
include __DIR__.'/../vendor/autoload.php';
use Laravie\Parser\Xml\Document;
use Laravie\Parser\Xml\Reader;
//use parser\ProductDataParser;

class AmountParser
{
    const IMPORT_FILE = ABSPATH."xml_import/data/xml/import.xml";
    private $data;
    private $date;
    private $shopCategories;

    public function __construct()
    {
        $xml = (new Reader(new Document()))->load(self::IMPORT_FILE);
        $this->date = date('Ymd-H:i');
        $this->data = $xml->getContent();
        $this->shopCategories = get_terms(array(
                                        'taxonomy' => 'product_cat',
                                        'get' => 'all'
                                    ));
    }

    private function writeLog($message)
    {

        $data = is_string($message) ? $message : json_encode($message);
        file_put_contents(__DIR__.'/logs/parserAmountProducts'.$this->date.'.log', print_r($data, true).PHP_EOL, FILE_APPEND);
    }

    public function parse()
    {
        $this->writeLog('Start');
        $data = json_decode(json_encode($this->data), true);
        if(json_last_error()) {
            $this->writeLog(json_last_error_msg());
            throw new Exception('Во время чтения JSON произошла ошибка - '.json_last_error_msg());
        }
        $data = $this->prepareData($data);
        $data = array_reverse($data);
        $this->writeLog('Данные подготовлены');
        $this->writeLog('Перемещаю в корзину товары отсутствующие в выгрузке:');
        $this->moveToTrash($data);
        $this->writeLog('Обновление данных:');
        $this->updateContent($data);
        $this->writeLog('Данные обновлены');
//        exec("tar -cvf ".ABSPATH.'xml_import/data/importHistory/import-'.date("Y-m-d-H-i").".tar.gz ". self::IMPORT_FILE);
//        exec ("rm -rf ".self::IMPORT_FILE);

//        (new ProductDataParser)->parse();
    }

    private function prepareData(array $data): array
    {
        $returned = [];
        foreach ($data['cat'] as $category) {
            if(empty($category['positions'])) {
                continue;
            }
            foreach($category['positions']['pos'] as $order => $position) {
                if ((int)$position['amount_left'] < 1) {
                    continue;
                }
                $re = '/^[a-zA-Z0-9]*/m';
                preg_match($re, $position['group_uid'], $matches);
                $groupId = $matches[0];
                $position['def_articul'] = trim($position['articul']);
                $position['articul'] = trim($position['articul']).'-'.$groupId;
                $position['glob_cat'] = $category['title'];
                $returned[trim($position['articul']) ?: $order][] = array_filter($position);
            }
        }
        file_put_contents('parser/logs/parserAmountProductsPrepareData'.date('Ymd_H:i').'.log', print_r($returned, true).PHP_EOL, FILE_APPEND);
        return $returned;
    }

    private function moveToTrash(array $data) {

        global $wpdb;
        file_put_contents('data.log', print_r($data, true).PHP_EOL);
        $products = $wpdb->get_results("select ID, post_title, guid, post_type, post_status, sku.meta_value as sku, isRelease.meta_value as isRelease
                                    from ms_posts
                                        left join ms_postmeta sku on ms_posts.ID = sku.post_id and sku.meta_key = '_sku'
                                        left join ms_postmeta isRelease on ms_posts.ID = isRelease.post_id and isRelease.meta_key = 'view_on_releases'
                                    where post_type = 'product' and post_status != 'trash'
                                          and (isRelease.meta_value = 0 or isRelease.meta_value is null)", ARRAY_A);
        $this->writeLog("Запрос вернул ".count($products)." результатов." );
        if (empty($products)) {

            return;
        }

        $i = 0;
        foreach ($products as $product) {
            if (key_exists($product['sku'],$data) === false) {
                $i++;
                $this->writeLog("Продукт {$product['ID']}|{$product['sku']}|{$product['post_title']}|{$product['guid']} - перемещен в корзину." );
                $wpdb->update('ms_posts', ['post_date' => date('Y-m-d H:i:s', strtotime(date('Y-m-d H:i:s').'-1 year'))], ['ID' => $product['ID']]);
                update_field('isSold', 1, $product['ID']);
                $this->removeHasVariants((new WC_Product_Factory())->get_product($product['ID']));
            }
        }
        $this->writeLog("Обработано ".count($products)." продуктов, в корзину перемещено {$i}");
    }

    private function updateContent(array $data)
    {
        $this->writeLog('Начинаю обновлять товары:');
        $_pf = new WC_Product_Factory();

        foreach ($data as $articul => $productItems) {
            $this->writeLog('SKU:'.$articul);
            $productId = $this->getProduct($articul);

            if ($productId === null) {
                $this->writeLog('Product not found, creatable new :');
                $productId = $this->addProduct($articul, $productItems);
            }

            if ($productId === 0) {
                continue;
            }

            $product = $_pf->get_product($productId);

            $product->set_stock_status('instock');

            //Remove all variants
            $this->writeLog('Remove all variants in product '.$product->get_id());
            $this->removeHasVariants($product);

            //Add variations with import.xml
            $this->writeLog('Add variations with import.xml in product '.$product->get_id());
            foreach ($productItems as $productItem) {
                $productItem['size'] = $this->validateSize($productItem['size']);
                $this->newVariant($productItem, $product);
            }

            $product = $_pf->get_product($product->get_id());
            $this->writeLog('Check has variants product:');
            $availableVariations = $product->get_available_variations();
            if (empty($availableVariations)) {
                $this->writeLog('Product variants not found - set status is sold: '.$productId);
                update_field('isSold', 1, $productId);
            } else {
                $this->writeLog('Product has variants - remove status is sold: '.$productId);
                update_field('isSold', 0, $productId);
            }
        }
    }

    private function getProduct(string $articul): ?int
    {
        global $wpdb;

        $wcProductPost = $wpdb->get_results("SELECT ms_posts.ID, ms_posts.post_parent FROM ms_posts WHERE ms_posts.ID = (SELECT ms_postmeta.post_id FROM ms_postmeta WHERE ms_postmeta.meta_key = \"_sku\" AND  ms_postmeta.meta_value = \"{$articul}\" LIMIT 1)");
        if (empty($wcProductPost)) {

            return null;
        }

        if ((int)$wcProductPost[0]->post_parent > 0) {

            return (int)$wcProductPost[0]->post_parent;
        }

        return (int)$wcProductPost[0]->ID;
    }

    private function addProduct(string $articul, array $productVariants): int
    {
        $productVariant = $productVariants[0];
        $customTitle = str_ireplace('(', '', $productVariant['title']);
        $customTitle = str_ireplace(')', '', $customTitle);
        $customTitle = str_ireplace($productVariant['producer'].' '.$productVariant['def_articul'], '-', $customTitle);
        $typeAndTitle = explode('-', $customTitle);
        $productVariant['type'] = trim($typeAndTitle[0]);
        $productVariant['titleOnPage'] = empty($typeAndTitle[1]) ? trim($productVariant['title']) : trim($typeAndTitle[1]);
        $brand = $this->checkOrCreateBrand($productVariant['producer']);

        $categories = [15];
        if (mb_stristr($productVariant['type'], 'муж')) {
            array_push($categories, 397);
        } elseif (mb_stristr($productVariant['type'], 'жен')) {
            array_push($categories, 408);
        }
        $globCatTerm = get_term_by('name', $productVariant['glob_cat'],'product_cat');
        if (empty($globCatTerm) === false) {
            array_push($categories, $globCatTerm->term_id);
        }
        foreach ($this->shopCategories as $shopCategory) {
            if(mb_stristr($productVariant['title'], $shopCategory->name) !== false) {
                array_push($categories, $shopCategory->term_id);
            }
            if (mb_stristr($productVariant['titleOnPage'], $shopCategory->name) !== false) {
                array_push($categories, $shopCategory->term_id);
            }
            if (mb_stristr($productVariant['type'], $shopCategory->name) !== false) {
                array_push($categories, $shopCategory->term_id);
            }
        }
        $post = array(
            'post_status' => "draft",
            'post_title' => $productVariant['title'],
            'post_parent' => '',
            'post_type' => "product",
        );
        $postId = wp_insert_post($post, 1);
        if (is_wp_error($postId)) {
            $this->writeLog("Adding variant failed: {$postId->get_error_code()} | {$postId->get_error_message()} | {$postId->get_error_data()} ");
            return 0;
        }
        update_field('type', trim($productVariant['type']), $postId);
        update_field('title_on_page', trim($productVariant['titleOnPage']), $postId);
        update_post_meta($postId, '_sku', trim($articul));
        wp_set_object_terms($postId, $brand, 'product_brand');
        wp_set_object_terms($postId, $categories, 'product_cat');
        wp_set_object_terms($postId, 'variable', 'product_type', false);
        $sizeTax = wc_attribute_taxonomy_name('size');
        $attributes = array(
            $sizeTax => array(
                'name' => $sizeTax,
                'value' => 1,
                'is_visible' => '1',
                'is_variation' => '1',
                'is_taxonomy' => '1'
            ),
        );
        update_post_meta($postId, '_product_attributes', $attributes);

        return $postId;
    }

    private function newVariant($productItem, $product)
    {
        if ((int)$productItem['amount_left'] === 0) {
            $this->writeLog('Size '. $productItem['size']  .' amount 0 - skip');
            return;
        }

        $fields = get_fields($product->get_id());
        $variationPost = array(
            'post_title'  => $fields['title_on_page']?:$product->get_title(),
            'post_content' => '',
            'post_status' => 'publish',
            'post_parent' => $product->get_id(),
            'post_type'   => 'product_variation',
        );
        $variationId = wp_insert_post($variationPost, 1);
        if (is_wp_error($variationId)) {
            $this->writeLog("Adding variant failed: {$variationId->get_error_code()} | {$variationId->get_error_message()} | {$variationId->get_error_data()} ");
            return;
        }
        $variation = new WC_Product_Variation( $variationId );
        $taxonomy = 'pa_size';
        $size = str_ireplace(',', '.', $productItem['size']);
        if( ! term_exists( $size, $taxonomy ) ) {
            wp_insert_term( $size, $taxonomy );
        }
        wp_set_post_terms($product->get_id(), $size, $taxonomy, true );

        $term = get_term_by( 'name', $size, $taxonomy, ARRAY_A);
        if(empty($term)) {
            $term = get_term_by( 'slug', $size, $taxonomy, ARRAY_A);
        }
        update_post_meta($variationId, 'attribute_'.$taxonomy, $term['slug']);
        if (isset($productItem['oldprice'])) {
            if (stripos($productItem['price'], ',')) {
                $productItem['price'] = substr($productItem['price'], 0, stripos($productItem['price'], ','));
            }
            update_post_meta( $variationId, '_sale_price', $productItem['price']);
            $variation->set_regular_price($productItem['oldprice']);
        } else {
            $variation->set_regular_price($productItem['price']);
        }

        $variation->save();
        $this->writeLog('Size '.$productItem['size'].' - created');
    }

    private function checkOrCreateBrand($producer)
    {
        if (empty($producer)) {
            return 0;
        }
        if ($producer === "ADIDAS") {
            $producer = "adidas Originals";
        }
        if ($producer === "CARHARTT") {
            $producer = "Carhartt WIP";
        }
        $brandTax = get_term_by('name', $producer, 'product_brand', ARRAY_A);
        if (!$brandTax) {
            $brandTax = wp_insert_term($producer, 'product_brand');
        }

        return $brandTax['term_taxonomy_id'];
    }

    private function validateSize(string $size): string
    {
        if (stristr($size, "ONE SIZE") ||
            stristr($size, "1 size") ||
            stristr($size, "1size") ||
            stristr($size, "mxs") ||
            stristr($size, "mxl") ||
            stristr($size, "unisex") ||
            stristr($size, "os") ||
            stristr($size, "sxm")) {

            return "O/S";
        }

        return str_ireplace('.','-', strtolower($size));
    }

    private function removeHasVariants(WC_Product $product): void
    {
        $variantsIds = $product->get_children();
        $attributes = $product->get_attributes();
        if (empty($variantsIds) === false) {
            foreach ($variantsIds as $variantId){
                $variantAttribute = wc_get_product_variation_attributes($variantId);
                wp_delete_post($variantId, true);
                if(empty($variantAttribute['attribute_pa_size']) === false) {
                    wp_remove_object_terms( $product->get_id(), $variantAttribute['attribute_pa_size'], 'pa_size' );
                }
            }
        }

        if (empty($attributes) === false && empty($attributes['pa_size'] === false)) {
            foreach ($attributes['pa_size']->get_options() as $option) {
                wp_remove_object_terms( $product->get_id(), $option, 'pa_size' );
            }
        }
    }
}
