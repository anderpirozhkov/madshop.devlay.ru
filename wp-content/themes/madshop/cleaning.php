<?php
/**
 * Template Name: Cleaning
 */
get_header();

load('header');
$breadcrumbs = array(
    'delimiter' => '',
    'wrap_before' => '<nav class="bread-crumbs">',
    'wrap_after' => '</nav>',
    'before' => '<div>',
    'after' => '</div>',
    'home' => _x('MADshop', 'breadcrumb', 'woocommerce'),
);

?>
<style>
	table{
		width:70%;
	}
	.about-page {
    background: #fff;
	}
	.about-page h1 {
	font-size:18px;
    text-align:center;
	padding-top:30px;
	padding-bottom:10px;
	width:100%;
	}

	.wrap.about-content {
    max-width: 740px;
	font-size: 13px;
    line-height: 21px;
	}

	.leftside {
	display: none;
    justify-content: center;
	}
	@media(max-width:768px){
		.leftside{
			display:flex;
			padding-bottom:15px;
		}
		table{
		width:100%;
		}
	}
</style>

    <div class="l_content about-page" id="content">
        <div class="wrap about-content">
        <div class="lc_main">
            <div class="lcm_content">
                <div class="content_wrap">
                    <!-- <div class="offer-block">
                    <img src="/img/originals-offer.svg">
                    <span class="originals-offer">Подлинный товар</span>
                    </div> -->
                    <div class="section section_text">
                        <div class="uss_section_content">
							<h1>Химчистка</h1>
							<p style="text-align:center;margin-top:0">Записаться можно по телефону <a style="text-decoration:underline" href="tel:+73432887881">+7 (343) 288-78-81</a><br>или приехав к нам в магазин</p>
							<p style="margin: 0 auto 15px;text-align: center;border: 1px solid #ddd;padding: 5px 20px;display: block;width: fit-content;">г. Екатеринбург, ул. Радищева, 25</p>
                            <div class="clian-content">
								<div class="leftside">
									<img style="width:110%;height:100%" src="https://solefresh.ru/wp-content/uploads/2019/12/DSC08940-3.jpg" />
								</div>
								<div class="right:side">
							<table style="margin: 0 auto;border-collapse: collapse;border: 1px solid #ddd;font-size:11px;">
								<tr style="background:#f9f9f9">
									<td style="padding:10px 0 10px 15px;background:#f9f9f9;font-size:11px;border-bottom: 1px solid #ddd;"><strong>Очистка</strong></td>
									<td style="text-align:center;padding:10px 10px 10px 5px;background:#f9f9f9;font-size:11px;border-bottom: 1px solid #ddd;">Базовая</td>
									<td style="text-align:center;padding:10px 10px 10px 5px;background:#f9f9f9;font-size:11px;border-bottom: 1px solid #ddd;">Комплекс</td>
									<td style="text-align:center;padding:10px 15px 10px 5px;background:#f9f9f9;font-size:11px;border-bottom: 1px solid #ddd;">Глубокая</td>
								</tr>
									<tr>
										<td style="padding:10px 10px 10px 15px;border-bottom: 1px solid #eee;">Удаление катышков</td>
										<td style="text-align:center;color:#ddd;border-bottom: 1px solid #eee;">—</td>
										<td style="text-align:center;font-size:36px;border-bottom: 1px solid #eee;">•</td>
										<td style="text-align:center;font-size:36px;border-bottom: 1px solid #eee;">•</td>
									</tr>
									<tr>
										<td style="padding:10px 10px 10px 15px;border-bottom: 1px solid #eee;">Ручная чистка</td>
										<td style="text-align:center;font-size:36px;border-bottom: 1px solid #eee;">•</td>
										<td style="text-align:center;font-size:36px;border-bottom: 1px solid #eee;">•</td>
										<td style="text-align:center;font-size:36px;border-bottom: 1px solid #eee;">•</td>
									</tr>
									<tr>
										<td style="padding:10px 10px 10px 15px;border-bottom: 1px solid #eee;">Чистка шнурков</td>
										<td style="text-align:center;font-size:36px;border-bottom: 1px solid #eee;">•</td>
										<td style="text-align:center;font-size:36px;border-bottom: 1px solid #eee;">•</td>
										<td style="text-align:center;font-size:36px;border-bottom: 1px solid #eee;">•</td>
									</tr>
									<tr>
										<td style="padding:10px 10px 10px 15px;border-bottom: 1px solid #eee;">Чистка стельки</td>
										<td style="text-align:center;color:#ddd;border-bottom: 1px solid #eee;">—</td>
										<td style="text-align:center;font-size:36px;border-bottom: 1px solid #eee;">•</td>
										<td style="text-align:center;font-size:36px;border-bottom: 1px solid #eee;">•</td>
									</tr>
									<tr>
										<td style="padding:10px 10px 10px 15px;border-bottom: 1px solid #eee;">Глубокая очистка</td>
										<td style="text-align:center;font-size:36px;border-bottom: 1px solid #eee;">•</td>
										<td style="text-align:center;font-size:36px;border-bottom: 1px solid #eee;">•</td>
										<td style="text-align:center;color:#ddd;border-bottom: 1px solid #eee;">—</td>
									</tr>
									<tr>
										<td style="padding:10px 10px 10px 15px;border-bottom: 1px solid #eee;">Восстановление цвета замши</td>
										<td style="text-align:center;color:#ddd;border-bottom: 1px solid #eee;">—</td>
										<td style="text-align:center;color:#ddd;border-bottom: 1px solid #eee;">—</td>
										<td style="text-align:center;font-size:36px;border-bottom: 1px solid #eee;">•</td>
									</tr>
									<tr>
										<td style="padding:10px 10px 10px 15px;border-bottom: 1px solid #eee;">Восстановление белой подошвы</td>
										<td style="text-align:center;color:#ddd;border-bottom: 1px solid #eee;">—</td>
										<td style="text-align:center;color:#ddd;border-bottom: 1px solid #eee;">—</td>
										<td style="text-align:center;font-size:36px;border-bottom: 1px solid #eee;">•</td>
									</tr>
									<tr>
										<td style="padding:10px 10px 10px 15px;border-bottom: 1px solid #eee;">Ароматизация</td>
										<td style="text-align:center;color:#ddd;border-bottom: 1px solid #eee;">—</td>
										<td style="text-align:center;color:#ddd;border-bottom: 1px solid #eee;">—</td>
										<td style="text-align:center;font-size:36px;border-bottom: 1px solid #eee;">•</td>
									</tr>
									<tr>
										<td style="padding:10px 10px 10px 15px;border-bottom: 1px solid #eee;">Водоотталкивающий лак</td>
										<td style="text-align:center;color:#ddd;border-bottom: 1px solid #eee;">—</td>
										<td style="text-align:center;color:#ddd;border-bottom: 1px solid #eee;">—</td>
										<td style="text-align:center;font-size:36px;border-bottom: 1px solid #eee;">•</td>
									</tr>
									<tr>
										<td style="padding:10px 10px 15px 15px;">Жирование</td>
										<td style="text-align:center;color:#ddd">—</td>
										<td style="text-align:center;color:#ddd">—</td>
										<td style="text-align:center;font-size:36px">•</td>
									</tr>
																	<tr style="background:#f9f9f9">
									<td style="padding:10px 0 10px 15px;font-size:11px;border-bottom: 1px solid #ddd;border-top: 1px solid #ddd;line-height:14px;"><strong>Кроссовки, кеды</strong> <br><i>Ткань</i></td>
									<td style="text-align:center;padding:10px 10px 10px 5px;font-size:11px;border-bottom: 1px solid #ddd;border-top: 1px solid #ddd;">500</td>
									<td style="text-align:center;padding:10px 10px 10px 5px;font-size:11px;border-bottom: 1px solid #ddd;border-top: 1px solid #ddd;">1000</td>
									<td style="text-align:center;padding:10px 15px 10px 5px;font-size:11px;border-bottom: 1px solid #ddd;border-top: 1px solid #ddd;">2000</td>
								</tr>
								<tr style="background:#f9f9f9">
									<td style="padding:10px 0 10px 15px;font-size:11px;border-bottom: 1px solid #ddd;line-height:14px;"><strong>Кроссовки, кеды</strong> <br><i>Кожа</i></td>
									<td style="text-align:center;padding:10px 10px 10px 5px;font-size:11px;border-bottom: 1px solid #ddd;">700</td>
									<td style="text-align:center;padding:10px 10px 10px 5px;font-size:11px;border-bottom: 1px solid #ddd;">1200</td>
									<td style="text-align:center;padding:10px 15px 10px 5px;font-size:11px;border-bottom: 1px solid #ddd;">2200</td>
								</tr>
								<tr style="background:#f9f9f9">
									<td style="padding:10px 0 10px 15px;font-size:11px;border-bottom: 1px solid #ddd;line-height:14px;"><strong>Кроссовки, кеды</strong>  <br><i>Замша, нубук</i></td>
									<td style="text-align:center;padding:10px 10px 10px 5px;font-size:11px;border-bottom: 1px solid #ddd;">800</td>
									<td style="text-align:center;padding:10px 10px 10px 5px;font-size:11px;border-bottom: 1px solid #ddd;">1200</td>
									<td style="text-align:center;padding:10px 15px 10px 5px;font-size:11px;border-bottom: 1px solid #ddd;">2200</td>
								</tr>
								<tr style="background:#f9f9f9">
									<td style="padding:10px 0 10px 15px;font-size:11px;border-bottom: 1px solid #ddd;line-height:14px;"><strong>Кроссовки высокие</strong> <br><i>Ткань</i></td>
									<td style="text-align:center;padding:10px 10px 10px 5px;font-size:11px;border-bottom: 1px solid #ddd;">1000</td>
									<td style="text-align:center;padding:10px 10px 10px 5px;font-size:11px;border-bottom: 1px solid #ddd;">1500</td>
									<td style="text-align:center;padding:10px 15px 10px 5px;font-size:11px;border-bottom: 1px solid #ddd;">2500</td>
								</tr>
								<tr style="background:#f9f9f9">
									<td style="padding:10px 0 10px 15px;font-size:11px;line-height:14px;"><strong>Кроссовки высокие</strong> <br><i>Кожа, замша, нубук</i></td>
									<td style="text-align:center;padding:10px 10px 10px 5px;font-size:11px;">1200</td>
									<td style="text-align:center;padding:10px 10px 10px 5px;font-size:11px;">1800</td>
									<td style="text-align:center;padding:10px 15px 10px 5px;font-size:11px;">2700</td>
								</tr>
							</table>
							</div>
                        </div>
                    </div>
                    <div class="cleaner"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php get_footer('main'); ?>
