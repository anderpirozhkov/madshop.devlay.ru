<?php

defined('ABSPATH') || exit;
global $wp_query;

get_header();
$breadcrumbs = array(
    'delimiter' => '',
    'wrap_before' => '<nav class="bread-crumbs">',
    'wrap_after' => '</nav>',
    'before' => '<div>',
    'after' => '</div>',
    'home' => _x('MADshop', 'breadcrumb', 'woocommerce'),
);
do_action('woocommerce_catalog_ordering');
$catalog_orderby_options = apply_filters(
    'woocommerce_catalog_orderby',
    array(
        'menu_order' => __('Default sorting', 'woocommerce'),
        'popularity' => __('Sort by popularity', 'woocommerce'),
        'rating' => __('Sort by average rating', 'woocommerce'),
        'date' => __('Сначала новые', 'woocommerce'),
        'price' => __('Дешевле выше', 'woocommerce'),
        'price-desc' => __('Дороже выше', 'woocommerce'),
    )
);

$terms = get_terms();
$category = get_queried_object();
?>

    <div class="content">
        <?php
        load('header'); ?>
        <div id="maincontent"></div>
        <div class="wrap">
            <div class="bread-crumbs-wrap">
                <?php
                woocommerce_breadcrumb($breadcrumbs);
                do_action( 'woocommerce_before_shop_loop' );
                ?>
            </div>
        </div>
        <div class="wrap">
            <div class="mobile-filter-wrap">
                <div class="m-filter">
                    <div class="m-filter-btn js-mobile-filter-open">
                        Фильтр
                    </div>
                    <div class="m-filter-wrap">
                        <div class="m-filter-top">
                            <div>
                                <div class="m-filter-btn js-mobile-filter-close">
                                    Фильтр
                                </div>
                            </div>
                        </div>
                        <div class="m-filter-container">
                            <div class="m-filter-container-scroll">
                                <div class="cart-filter">
                                    <div class="accordion type2 js-cart-acc-m">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php do_action( 'woocommerce_before_shop_loop', true ); ?>
            </div>
            <div class="cart">
                <div class="cart-col">
                    <div class="cart-col-l">

                        <?php /* do_shortcode('[premmerce_filter]') */ ?>

                        <form action="#" method="POST" class="cart-filter">
                            <div class="accordion type2 js-cart-acc">
                                <?php
                                if ($category->term_id !== 408 && $category->term_id !== 397 && $category->term_id !== 434) { ?>
                                    <div class="accordion-item accordion-item--opened">
                                        <?= do_shortcode('[br_filter_single filter_id=42284]') ?>
                                    </div>
                                    <div class="accordion-item accordion-item--closed">
                                        <?= do_shortcode('[br_filter_single filter_id=9503]') ?>
                                    </div>
                                    <?php
                                } else { ?>
                                    <div class="accordion-item accordion-item--opened">
                                        <?= do_shortcode('[br_filter_single filter_id=9503]') ?>
                                    </div>
                                    <?php
                                } ?>
                                <div class="accordion-item accordion-item--closed">
                                    <?= do_shortcode('[br_filter_single filter_id=9504]') ?>
                                </div>
                                <div class="accordion-item accordion-item--closed">
                                    <?= do_shortcode('[br_filter_single filter_id=9507]') ?>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="cart-col-r">
                        <div class="cart-list">
                            <?php
                            if (woocommerce_product_loop()) {
                                if (wc_get_loop_prop('total')) {
                                    while (have_posts()) {
                                        the_post();
                                        $isSold = get_field('isSold', get_the_ID());
                                        /**
                                         * Hook: woocommerce_shop_loop.
                                         */
                                        do_action('woocommerce_shop_loop');

                                        wc_get_template_part('content', 'product');
                                    }
                                }

                                woocommerce_product_loop_end();
                            } else {
                                /**
                                 * Hook: woocommerce_no_products_found.
                                 *
                                 * @hooked wc_no_products_found - 10
                                 */
                                do_action('woocommerce_no_products_found');
                            } ?>
                        </div>
                        <?php
                        /**
                         * Hook: woocommerce_after_shop_loop.
                         *
                         * @hooked woocommerce_pagination - 10
                         */
                        do_action('woocommerce_after_shop_loop');
                        ?>
                    </div>
                </div>
            </div>

        </div>
    </div>
<?php
get_footer('main'); ?>