<?php

/**
 * Template Name: Popular products
 */
defined('ABSPATH') || exit;
global $wp_query;
get_header();
$breadcrumbs = array(
    'delimiter' => '',
    'wrap_before' => '<nav class="bread-crumbs">',
    'wrap_after' => '</nav>',
    'before' => '<div>',
    'after' => '</div>',
    'home' => _x('MADshop', 'breadcrumb', 'woocommerce'),
);

$requestUri = str_ireplace('?'.$_SERVER['QUERY_STRING'], '', $_SERVER['REQUEST_URI']);
if (stripos($requestUri, '/page/')) {
    $pageUri = substr($requestUri, stripos($requestUri, '/page/'), strlen($requestUri));
    $pageSegments = array_filter(explode('/', $pageUri));
    $paged = array_pop($pageSegments);
} else {
    $paged = 1;
}

$taxQuery = [];
if (isset($_GET['filters'])) {
    $filterData = explode('|', $_GET['filters']);
    foreach ($filterData as $filter) {
        $ids = explode(
            '-',
            substr($filter, stripos($filter, '[') + 1, (stripos($filter, ']') - 1) - stripos($filter, '['))
        );
        $taxonomy = substr($filter, 0, stripos($filter, '['));
        $taxQuery[] = [
            'taxonomy' => $taxonomy === 'size' ? 'pa_'.$taxonomy : $taxonomy,
            'field' => 'id',
            'terms' => $ids,
            'operator' => 'IN'
        ];
    }
}

$postProducts = array(
    'limit' => 30,
    'posts_per_page' => 30,
    'paged' => $paged,
    'order' => 'desc',
    'post_type' => 'product',
    'orderby' => 'post_views',
    'paginate' => true,
);

if (empty($taxQuery) === false) {
    $postProducts['tax_query'] = [
        'relation' => 'OR',
        $taxQuery
    ];
}

$wpQuery = new WP_Query($postProducts);
$total = isset($total) ? $total : $wpQuery->max_num_pages;
$base = isset($base) ? $base : esc_url_raw(
    str_replace(999999999, '%#%', remove_query_arg('add-to-cart', get_pagenum_link(999999999, false)))
);
$format = isset($format) ? $format : '';

?>
    <script>let hideChoiceM = true; </script>
    <div class="content">
        <?php
        load('header'); ?>
        <div id="maincontent"></div>
        <div class="wrap">
            <div class="bread-crumbs-wrap">
                <?php
                woocommerce_breadcrumb($breadcrumbs); ?>
            </div>
        </div>
        <div class="wrap">
            <div class="cart">
                <div class="cart-col">
                    <div class="cart-col-l">
                        <?php if($wpQuery->have_posts()) {?>
                            <form action="#" method="POST" class="cart-filter">
                                <div class="accordion type2 js-cart-acc">
                                    <div class="accordion-item accordion-item--opened">
                                        <?= do_shortcode('[br_filter_single filter_id=9504]') ?>
                                    </div>
                                    <div class="accordion-item accordion-item--closed">
                                        <?= do_shortcode('[br_filter_single filter_id=9503]') ?>
                                    </div>
                                    <div class="accordion-item accordion-item--closed">
                                        <?= do_shortcode('[br_filter_single filter_id=42284]') ?>
                                    </div>
                                    <div class="accordion-item accordion-item--closed">
                                        <?= do_shortcode('[br_filter_single filter_id=9507]') ?>
                                    </div>
                                </div>
                            </form>
                        <?php } ?>
                    </div>
                    <div class="cart-col-r">
                        <div class="cart-list">
                            <?php
                            if ($wpQuery->have_posts()) {
                                foreach($wpQuery->get_posts() as $post) {
                                    $product = wc_get_product($post->ID);
                                    setup_postdata($GLOBALS['post'] =& $post);
                                    wc_get_template_part('content', 'product');
                                }
                                wp_reset_postdata();
                            } else {
                                do_action('woocommerce_no_products_found');
                            }
                            ?>
                        </div>

                        <nav class="pagination">
                            <?php
                            $links = paginate_links(
                                apply_filters(
                                    'woocommerce_pagination_args',
                                    array( // WPCS: XSS ok.
                                        'base' => $base,
                                        'format' => '',
                                        'add_args' => false,
                                        'prev_next' => false,
                                        'current' => max(1, $paged),
                                        'total' => $total,
                                        'prev_text' => '&larr;',
                                        'next_text' => '&rarr;',
                                        'type' => 'array',
                                        'mid_size' => 3,
                                    )
                                )
                            );
                            if (!empty($links)) {
                                foreach ($links as $link) {
                                    echo $link;
                                }
                            }
                            ?>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php
get_footer('main'); ?>