<?php
/**
 * Variable product add to cart
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/add-to-cart/variable.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.5.5
 */

defined('ABSPATH') || exit;

global $product;
$variations = [];
$taxonomy = 'pa_size';

$variations = getVariationsSort($available_variations);
$brand = wp_get_post_terms( $product->get_id(), 'product_brand');

$cartItems = WC()->cart->get_cart();
$productInCart = false;
foreach ($cartItems as $cartItem) {
    $productInCart = $cartItem['product_id'] == $product->get_id() ?: false;
}

do_action('woocommerce_before_add_to_cart_form'); ?>
<div class="cart-item-btns">
    <a href="/checkout" class="btn go-checkout" <?php if($productInCart) { ?> style="display: block;" <?php } else { ?> style="display: none;" <?php } ?>>Перейти в корзину</a>
    <a href="" class="btn btn-primary animated-btn add-to-cart-btn" data-is-simple="1" data-product-id="<?= $product->get_id() ?>" <?php if($productInCart) { ?> style="display: none;" <?php } else { ?> style="display: block;" <?php } ?> >
        Добавить в корзину
    </a>
    <a href="/checkout" data-product-id="<?= $product->get_id() ?>" data-is-simple="true" data-variant-id="0" class="btn btn-secondary add-to-cart-checkout-btn">Купить</a>
</div>

<?php
$onlyOnline = get_field('only_online_payment', $product->get_id());
if ($onlyOnline) {
    ?>
    <div class="only-online-payment">
        Только онлайн оплата
    </div>

<?php } ?>
<div class="cart-item-params accordion js-cart-item-acc">
    <?php
    if (empty($product->post->post_content) === false) { ?>
        <div class="cart-item-param accordion-item _open-alweys">
            <div class="accordion-title">
                <div>Описание</div>
            </div>
            <div class="accordion-list">
                <?= $product->post->post_content ?>
            </div>
        </div>
        <?php
    }  if($product->get_sku()) {?>
    <div class="cart-item-param row">
        <div class="cart-item-param-title">
            <div>Артикул</div>
        </div>
        <div class="cart-item-param-content">
            <?= $product->get_sku() ?>
        </div>
    </div>
    <?php } ?>

    <div class="cart-item-param accordion-item _open">
        <div class="accordion-title sub">
            <div>Оплата и доставка</div>
        </div>
        <div class="accordion-list">
            <p class="title_tab"><b>Доставка по России</b></p>
            <p>- Курьерская служба СДЭК (Срок доставки 2-10 дней)</p>
            <p>- Почта России (Срок доставки 2-10 дней)</p>
            <p>-&nbsp;При заказе от 10 000 рублей доставка по России <strong>бесплатно.</strong></p>
            <p class="title_tab"><b>Доставка по Екатеринбургу</b></p>
            <p>- Курьером по Екатеринбургу (бесплатно, срок доставки до 2-х дней).</p>
            <p class="title_tab"><b>Оплата</b></p>
            <p>- Оплатить заказ можно наличными/картой при получении, или электронными средствами на сайте.</p>
            <p>- К оплате на сайте принимаются Qiwi, Яндекс.Деньги, карты любых банков РФ и многое другое.</p>
            <p>- Вы можете произвести оплату банковскими картами платежных систем: Visa, Mastercard, МИР.</p></div>
    </div>
    <div class="cart-item-param accordion-item">
        <div class="accordion-title sub">
            <div>Отказ и возврат</div>
        </div>
        <div class="accordion-list">
            <div class="title">Возврат:</div>
            <p>Возврат или обмен товара возможен в течение 14 календарных дней,
                не считая дня совершения покупки в нашем магазине на ул. Радищева.</p>
            <p>Не подошедший товар необходимо отправить нам обратно через Почту России, либо курьерской службой по
                адресу:</p>
            <p>— Куда: 620014, г. Екатеринбург ул. Радищева 25.</p>
            <p>— Кому: ИП Мазурков А.Д.</p>
            <p>К возврату принимаются изделия в товарном виде: без следов эксплуатации, с заводскими бирками и в
                оригинальной упаковке.</p>
        </div>
    </div>
</div>
<div class="still-for-brand">
<!--    <a href="--><?//= get_term_link($brand[0]->term_id)?><!--">Еще от --><?//= $brand[0]->name?><!--</a>-->
</div>