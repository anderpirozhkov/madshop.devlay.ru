<?php

global $wpdb;

if (empty($_SERVER['REQUEST_URI'])) {

    return;
}
$actualLink = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
$queryStr = $_SERVER['QUERY_STRING'];
$requestUri = str_ireplace('?'.$queryStr, '', $_SERVER['REQUEST_URI']);

if(preg_match_all('/(\.-)/m', $requestUri, $matches, PREG_SET_ORDER, 0)) {
    $requestUri = preg_replace('/(\.-)/m', '-', $requestUri);
} elseif (preg_match_all('/(-\.)/m', $requestUri, $matches, PREG_SET_ORDER, 0)) {
    $requestUri = preg_replace('/(-\.)/m', '-', $requestUri);
} else {
    $requestUri = preg_replace('/(\.)/m', '-', $requestUri);
}
$requestUri = str_ireplace('--', '-', $requestUri);

$segmentsPath = explode('/',$requestUri);

if (is_array($segmentsPath)) {
    $segmentsPath = array_filter($segmentsPath);
}

if (is_array($segmentsPath) === false || empty($segmentsPath)) {

    return;
}

$endpointPath = trim(array_pop($segmentsPath));
$postId = $wpdb->get_var("select ID from ms_posts where post_name = \"{$endpointPath}\" and post_type != 'attachment'");

if (empty($postId)) {

    return;
}
$queryStr = empty($queryStr) ? "" : "?{$queryStr}";
$validLink = get_permalink($postId).$queryStr;

if ($validLink !== $actualLink) {
    wp_redirect(get_permalink($postId).$queryStr, 301);
    exit();
}

