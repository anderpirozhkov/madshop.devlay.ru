<?php namespace Premmerce\Filter\Widget;

use WP_Widget;
use Premmerce\Filter\FilterPlugin;
use Premmerce\Filter\Filter\Container;

class FilterWidget extends WP_Widget
{


    /**
     * FilterWidget constructor.
     */
    public function __construct()
    {
        parent::__construct(
            'premmerce_filter_filter_widget',
            __('Premmerce filter', 'premmerce-filter'),
            [
                'description' => __('Product attributes filter', 'premmerce-filter'),
            ]
        );
    }

    /**
     * Render widget
     *
     * @param array $args
     * @param array $instance
     */
    public function widget($args, $instance)
    {
        if (apply_filters('premmerce_product_filter_active', false)) {
            $data = self::getFilterWidgetContent($args, $instance);
            do_action('premmerce_product_filter_render', $data);
        }
    }

    /**
     * Get Filter Widget data
     *
     * @param array $args
     * @param array $instance
     */
    public static function getFilterWidgetContent($args = [], $instance = [])
    {
        $items = Container::getInstance()->getItemsManager()->getFilters();
        $items = apply_filters('premmerce_product_filter_items', $items);
        $settings = get_option(FilterPlugin::OPTION_SETTINGS, []);
        $style = isset($settings['style']) ? $settings['style'] : 'premmerce';
        $showFilterButton = !empty($settings['show_filter_button']);

        $data = [
            'args' => $args,
            'style' => $style,
            'showFilterButton' => $showFilterButton,
            'attributes' => $items,
            'formAction' => apply_filters('premmerce_product_filter_form_action', ''),
            'instance' => $instance
        ];

        return $data;
    }

    /**
     * @param array $new_instance
     * @param array $old_instance
     *
     * @return array
     */
    public function update($new_instance, $old_instance)
    {
        $instance = [];
        $instance['title'] = strip_tags($new_instance['title']);

        return $instance;
    }

    /**
     * @param array $instance
     *
     * @return string|void
     */
    public function form($instance)
    {
        do_action('premmerce_product_filter_widget_form_render', [
            'title' => isset($instance['title']) ? $instance['title'] : '',
            'widget' => $this,
        ]);
    }
}
