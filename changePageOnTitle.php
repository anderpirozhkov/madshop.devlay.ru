<?php

require_once __DIR__."/wp-load.php";
include_once(ABSPATH.'wp-admin/includes/admin.php');
require_once(ABSPATH.'wp-admin/includes/user.php');

global $wpdb;

$products = get_posts(
    [
        'post_type' => 'product',
        'numberposts' => -1,
        'post_status' => 'published',
    ]
);
foreach ($products as $product) {
    $brand = get_the_terms($product->ID, 'product_brand');
//    $type = get_field('type', $product->ID);
//    $sku = $wpdb->get_var("SELECT meta_value FROM ms_postmeta WHERE post_id = {$product->ID} AND meta_key = \"_sku\"");
//    $titleOnPage = $product->post_title;
//    $titleOnPage = str_ireplace($brand[0]->name, '', $titleOnPage);
//    $titleOnPage = str_ireplace($type, '', $titleOnPage);
//    $titleOnPage = str_ireplace('Кроссовки', '', $titleOnPage);
//    if ($sku) {
//        $titleOnPage = str_ireplace($sku, '', $titleOnPage);
//    }
    $titleOnPage = get_field('title_on_page', $product->ID);
    $titleOnPage = str_ireplace('Originals', '', $titleOnPage);
    $titleOnPage = str_ireplace('adidas', '', $titleOnPage);
    $titleOnPage = ucwords(strtolower($titleOnPage));
    update_field('title_on_page', trim($titleOnPage), $product->ID);
}