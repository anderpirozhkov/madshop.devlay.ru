<?php

define('DONOTCACHEPAGE', true);
global $woocommerce;
$cartData = WC()->cart->get_cart_contents();

$cartItems = [];
foreach ($cartData as $cartItem) {
    if (empty($cartItems[$cartItem['product_id']])) {
        $fields = get_fields($cartItem['product_id']);
        $brand = get_the_terms($cartItem['product_id'], 'product_brand');
        $cartItems[$cartItem['product_id']] = [
            'type' => $fields['type'] ?: '',
            'link' => get_permalink($cartItem['product_id']),
            'image' => wp_get_attachment_url($cartItem['data']->get_image_id()),
            'titleOnPage' => $fields['title_on_page'] ?: '',
            'quantity' => 1,
            'price' => explode(' ', strip_tags($cartItem['data']->get_price_html())),
        ];
        $cartItems[$cartItem['product_id']]['sizes'][] = str_replace(
            '-',
            '.',
            $cartItem['variation']['attribute_pa_size']
        );
    } else {
        $cartItems[$cartItem['product_id']]['sizes'][] = str_replace(
            '-',
            '.',
            $cartItem['variation']['attribute_pa_size']
        );
        $cartItems[$cartItem['product_id']]['quantity']++;
    }
}
?>
<header>
    <div class="header">
        <div class="wrap">
            <div class="header-container">
                <div class="header-lg">
                    <div class="menu-btn">
                        <div class="icon icon-menu"></div>
                    </div>
                    <div class="logo">
                        <a title="mad shop" href="/">
                            <img src="<?= get_template_directory_uri() ?>/img/logo.svg" alt="">
                        </a>
                    </div>
                    <?php load('Menu'); ?>
                </div>
                <div class="header-icons">
                    <div id="search-box">
                        <?= do_shortcode('[yith_woocommerce_ajax_search]') ?>
                    </div>
                    <div class="icon icon-search" id="icon-search"></div>
                    <a class="icon icon-basket" id="icon-basket">
                        <?php
                        if (WC()->cart->get_cart_contents_count() > 0) { ?>
                            <span class="basket-count"><?= WC()->cart->get_cart_contents_count(); ?></span>
                            <?php
                        } ?>
                    </a>
                    <?php
                    if (WC()->cart->get_cart_contents_count() > 0) { ?>
                        <?= getBasketHover() ?>
                        <?php
                    } ?>
                </div>
            </div>
        </div>
    </div>
    <div class="header-box"></div>
</header>

<div class="m-menu-wrap">
    <div class="m-menu-container">
        <div class="m-menu-header">
            <div class="m-menu-logo">
                <img src="<?= get_template_directory_uri() ?>/img/logo.svg" alt="">
            </div>
            <div class="m-menu-close"></div>
        </div>
        <div class="m-menu-main">
            <nav class="m-menu-primary">
                <?php wp_nav_menu(['menu' => 'header-menu']); ?>
            </nav>
            <nav class="m-menu-secondary">
                <div></div>
            </nav>
        </div>
        <div class="m-menu-footer">
            <nav class="m-menu-info">
                <div><a href="/o-nas/">О нас</a></div>
                <div><a href="/dostavka-i-oplata/">Доставка и оплата</a></div>
                <div><a href="/obmen-i-vozvrat/">Обмен и возврат</a></div>
                <div><a class="fmi fmi2" href="tel:+73432887881">+7 (343) 288-78-81</a></div>
                <div><a class="fmi fmi3" href="mailto:hi@madshop.ru">hi@madshop.ru</a></div>
                <div><span class="fmi fmi4">Екатеринбург, Радищева 25</span></div>
            </nav>

            <div class="m-menu-social">
                <div>
                    <a title="Instagram" href="https://www.instagram.com/the__madshop">
                        <img src="<?= get_template_directory_uri() ?>/img/in.svg" alt="">
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>