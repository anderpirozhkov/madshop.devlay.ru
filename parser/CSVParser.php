<?php

require_once __DIR__."/../wp-load.php";
include_once( ABSPATH . 'wp-admin/includes/admin.php' );
define('ALLOW_UNFILTERED_UPLOADS', true);

class CSVParser
{
    public function parseFile($filename = "", $delimiter = ",")
    {
        if (!file_exists($filename) || !is_readable($filename)) {
            return false;
        }

        $header = null;
        $data = array();
        if (($handle = fopen($filename, 'r')) !== false) {
            while (($row = fgetcsv($handle, 1000, $delimiter)) !== false) {
                if (!$header) {
                    $header = $row;
                } else {
                    if (count($header) === count($row)){
                        $data[] = array_combine($header, $row);
                    }
                }
            }
            fclose($handle);
        }
        return $data;
    }

    public function prepare(array $data)
    {
        $returned = [];
        foreach ($data as $item) {
            if (empty($returned[$item['articul']])) {
                foreach ($item as $key => $value) {
                    switch ($key){
                        case 'images':
                            if (unserialize($value) === false) {
                                $value = preg_replace_callback ( '!s:(\d+):"(.*?)";!', function($match) {
                                    return ($match[1] == strlen($match[2])) ? $match[0] : 's:' . strlen($match[2]) . ':"' . $match[2] . '";';
                                },$value );
                            }
                            $images = unserialize($value);
                            if (empty($images)) {
                                $returned[$item['articul']]['images'] = [];
                            }
                            else {
                                foreach ($images as $orderImage => $path) {
                                    $returned[$item['articul']]['images'][] = $this->storeImage($path);
                                }
                            }
                            break;
                        case 'image':
                            $returned[$item['articul']][$key] = $this->storeImage($path);
                            break;
                        case 'tabs':
                            break;
                        case 'size':
                            $returned[$item['articul']][$key][] = $value;
                            break;
                        default:
                            $returned[$item['articul']][$key] = $value;
                            break;
                    }
                }
            } else {
                $returned[$item['articul']]['size'][] .= $item['size'];
            }
        }
        file_put_contents('log.log', print_r($returned, true).PHP_EOL);
        return $returned;
    }

    private function storeImage($path)
    {
        $url = "https://madshop.ru/uploadedFiles/eshopimages/orig/{$path}";

        return media_sideload_image($url, 0, null, 'src');
    }

}